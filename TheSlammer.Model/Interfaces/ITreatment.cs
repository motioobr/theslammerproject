﻿using System;

namespace TheSlammer.Model.Interfaces
{
    public interface ITreatment
    {
        int TreatmentID { get; set; }
        DateTime StartDate { get; set; }
        DateTime? EndDate { get; set; }
        bool InTreatment { get; set; }
        Enums.TreatmentType Type { get; set; }
        int PatientID { get; set; }
        Inmate.Inmate Patient { get; set; }
        int DoctorID { get; set; }
        Medical.Medical Doctor { get; set; }
    }
}