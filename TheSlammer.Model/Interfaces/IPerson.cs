﻿using System;

namespace TheSlammer.Model.Interfaces
{
    public interface IPerson
    {
        int PersonID { get; set; }
        int IdNumber { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime? DateOfBirth { get; set; }
        Enums.Gender Gender { get; set; }
    }
}