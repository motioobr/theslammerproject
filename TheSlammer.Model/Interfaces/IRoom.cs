﻿using TheSlammer.Model.Locations;

namespace TheSlammer.Model.Interfaces
{
    public interface IRoom : ILocation
    {
        int RoomID { get; set; }
        byte BuildingID { get; set; }
        Building Building { get; set; }
    }
}