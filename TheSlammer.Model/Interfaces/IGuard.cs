﻿using System.Collections.Generic;
using TheSlammer.Model.Inmate;

namespace TheSlammer.Model.Interfaces
{
    public interface IGuard : ISecurity
    {
        ICollection<Cell> Cells { get; set; }
    }
}