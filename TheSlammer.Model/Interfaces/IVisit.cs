﻿using System;
using TheSlammer.Model.Visit;

namespace TheSlammer.Model.Interfaces
{
    public interface IVisit
    {
        int VisitID { get; set; }
        Enums.Relationship Relation { get; set; }
        bool WasAttended { get; set; }
        bool WasIntturupted { get; set; }
        DateTime Date { get; set; }
        TimeSpan Durtion { get; set; }
        int InmateID { get; set; }
        Inmate.Inmate Inmate { get; set; }
        int VisitorID { get; set; }
        Visitor Visitor { get; set; }
    }
}