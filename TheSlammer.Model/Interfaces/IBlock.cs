﻿using System.Collections.Generic;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Interfaces
{
    public interface IBlock : IRoom
    {
        int OfficerID { get; set; }
        Officer Officer { get; set; }
        ICollection<Cell> Cells { get; set; }
    }
}