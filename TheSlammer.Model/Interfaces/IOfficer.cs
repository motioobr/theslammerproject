﻿using System.Collections.Generic;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Interfaces
{
    public interface IOfficer : ISecurity
    {
        ICollection<Block> Blocks { get; set; }
    }
}