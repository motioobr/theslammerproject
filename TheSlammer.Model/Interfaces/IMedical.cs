﻿using System.Collections.Generic;
using TheSlammer.Model.Medical;

namespace TheSlammer.Model.Interfaces
{
    public interface IMedical : IUser
    {
        Enums.MedicalExperties Expertises { get; set; }
        ICollection<Treatment> Treatments { get; set; }
    }
}