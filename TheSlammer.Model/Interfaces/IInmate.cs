﻿using System;
using System.Collections.Generic;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Medical;

namespace TheSlammer.Model.Interfaces
{
    public interface IInmate : IPerson
    {
        Enums.EyeColor Eye { get; set; }
        Enums.HairColor Hair { get; set; }
        Enums.Health Health { get; set; }
        decimal Height { get; set; }
        DateTime IncarcerationDate { get; set; }
        bool InCustody { get; set; }
        Enums.Religion Religion { get; set; }
        DateTime? ReleaseDate { get; set; }
        decimal Weight { get; set; }
        byte MonthlyVisits { get; set; }
        int CellID { get; set; }
        Cell Cell { get; set; }
        ICollection<Treatment> Treatments { get; set; }
        ICollection<Visit.Visit> Visits { get; set; }
    }
}