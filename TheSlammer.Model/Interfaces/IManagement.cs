﻿using TheSlammer.Model.Management;

namespace TheSlammer.Model.Interfaces
{
    public interface IManagement : IUser
    {
        int OfficeID { get; set; }
        Office Office { get; set; }
    }
}