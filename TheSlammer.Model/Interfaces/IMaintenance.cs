﻿using System.Collections.Generic;
using TheSlammer.Model.Maintenance;

namespace TheSlammer.Model.Interfaces
{
    public interface IMaintenance : IUser
    {
        Enums.MaintenanceType Job { get; set; }
        ICollection<Work> Works { get; set; }
    }
}