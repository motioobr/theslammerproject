﻿using System.Collections.Generic;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Interfaces
{
    public interface ICell : IRoom
    {
        byte Capacity { get; set; }
        int BlockID { get; set; }
        int GuardID { get; set; }
        Block Block { get; set; }
        Guard Guard { get; set; }
        ICollection<Inmate.Inmate> Inmates { get; set; }
    }
}