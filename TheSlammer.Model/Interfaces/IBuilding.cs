﻿using System.Collections.Generic;
using TheSlammer.Model.Locations;

namespace TheSlammer.Model.Interfaces
{
    public interface IBuilding : ILocation
    {
        byte BuildingID { get; set; }
        string Name { get; set; }
        ICollection<Room> Rooms { get; set; }
    }
}