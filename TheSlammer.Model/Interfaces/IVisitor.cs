﻿using System.Collections.Generic;

namespace TheSlammer.Model.Interfaces
{
    public interface IVisitor : IPerson
    {
        ICollection<Visit.Visit> Visits { get; set; }
    }
}