﻿using System;

namespace TheSlammer.Model.Interfaces
{
    public interface IFactory<in TEnum, out TReturn, in IType>
        where TEnum : struct, IComparable, IConvertible, IFormattable 
        where TReturn : class, new() 
        where IType : class
    {
        TReturn Generate( TEnum enumChoice, IType interfaceType = null );
    }
}