﻿namespace TheSlammer.Model.Interfaces
{
    public interface IUser : IPerson
    {
        string Username { get; set; }
        string Password { get; set; }
    }
}