﻿using System;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Interfaces
{
    public interface IWork
    {
        int WorkID { get; set; }
        Enums.MaintenanceType Job { get; set; }
        DateTime? WorkDate { get; set; }
        bool IsCompleted { get; set; }
        int? WorkerID { get; set; }
        Maintenance.Maintenance Worker { get; set; }
        Enums.WorkType WorkType { get; set; }
    }
}