﻿using System;

namespace TheSlammer.Model.Inmate
{
    public class SHU : Cell
    {
        public enum ViolationType : byte
        {
            CupOffence,
            Fight,
            Disobey,
            PropertyDestruction,
            Other
        };

        public bool IsFull { get; set; }
        public Inmate Inmate { get; set; }
        public string ViolationDescription { get; set; }
        public ViolationType Violation { get; set; }
        public DateTime DayIn { get; set; }
        public DateTime? DayOut { get; set; }
    }
}