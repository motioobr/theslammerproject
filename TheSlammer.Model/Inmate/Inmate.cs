﻿using System;
using System.Collections.Generic;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Model.Medical;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Inmate
{
    [Authorization(
        Level.Management | Level.Security,
        Level.Management | Level.Security | Level.Medical,
        Level.Management | Level.Security | Level.Medical,
        Level.Management | Level.Security )]
    public class Inmate : Person, IInmate
    {
        public Inmate() : base( Enums.Role.Inmate )
        {
            Treatments = new List<Treatment>();
            Visits = new List<Visit.Visit>();
        }

        


        #region Field/Properties

        public Enums.EyeColor Eye { get; set; }
        public Enums.HairColor Hair { get; set; }
        public Enums.Health Health { get; set; }
        public decimal Height { get; set; }
        public DateTime IncarcerationDate { get; set; }
        public bool InCustody { get; set; }
        public Enums.Religion Religion { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public decimal Weight { get; set; }
        public byte MonthlyVisits { get; set; }
        public int CellID { get; set; }


        #region Entity Framework Navigation Properties

        public virtual Cell Cell { get; set; }
        public virtual ICollection<Treatment> Treatments { get; set; }
        public virtual ICollection<Visit.Visit> Visits { get; set; }

        #endregion


        #endregion
    }
}