﻿using System.Collections.Generic;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Inmate
{
    public class Cell : Room, ICell
    {
        public Cell()
        {
            Inmates = new List<Inmate>();
        }


        #region Fields/Properties

        public byte Capacity { get; set; }
        public int BlockID { get; set; }
        public int GuardID { get; set; }


        #region Entity Framework Navigation Properties

        public virtual ICollection<Inmate> Inmates { get; set; }
        public virtual Block Block { get; set; }
        public virtual Guard Guard { get; set; }

        #endregion


        #endregion
    }
}