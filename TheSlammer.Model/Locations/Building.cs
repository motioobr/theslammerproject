﻿using System.Collections.Generic;
using TheSlammer.Model.Interfaces;

namespace TheSlammer.Model.Locations
{
    public class Building : IBuilding
    {
        public Building()
        {
            Rooms = new List<Room>();
        }

        public byte BuildingID { get; set; }
        public string Name { get; set; }


        #region Entity Framework Navigation Properties

        public virtual ICollection<Room> Rooms { get; set; }

        #endregion
    }
}