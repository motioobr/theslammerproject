﻿using TheSlammer.Model.Interfaces;

namespace TheSlammer.Model.Locations
{
    public abstract class Room : IRoom
    {
        #region Fields/Properties

        public int RoomID { get; set; }
        public byte BuildingID { get; set; }


        #region Entity Framework Navigation Properties

        public virtual Building Building { get; set; }

        #endregion


        #endregion
    }
}