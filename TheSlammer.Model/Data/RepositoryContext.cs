﻿using System.Data.Entity;
using System.Diagnostics;
using TheSlammer.Model.Properties;

namespace TheSlammer.Model.Data
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext() : base()
        {

            Database.SetInitializer( new ContextSeeder() );
        }
    }
}