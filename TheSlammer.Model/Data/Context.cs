using System.Data.Entity;
using TheSlammer.Model.Data.Configuration;
using TheSlammer.Model.Data.Configuration.Locations;
using TheSlammer.Model.Data.Configuration.People;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Management;
using TheSlammer.Model.People;
using TheSlammer.Model.Security;
using TheSlammer.Model.Visit;

namespace TheSlammer.Model.Data
{
    public class Context : RepositoryContext
    {
        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            #region People

            modelBuilder.Configurations.Add( new PersonEntityConfig() );
            modelBuilder.Configurations.Add( new UserEntityConfig() );
            modelBuilder.Configurations.Add( new MedicalEntityConfig() );
            modelBuilder.Configurations.Add( new MaintenanceEntityConfig() );

            modelBuilder.Configurations.Add( new InmateEntityConfig() );
            modelBuilder.Configurations.Add( new VisitorEntityConfig() );

            modelBuilder.Configurations.Add( new ManagementEntityConfig() );
            modelBuilder.Configurations.Add( new GuardEntityConfig() );

            modelBuilder.Entity<Warden>().ToTable( "Wardens" ).HasKey( w => w.PersonID );
            modelBuilder.Entity<Officer>().ToTable( "Officers" ).HasKey( w => w.PersonID );
            modelBuilder.Entity<Security.Security>().ToTable( "SecurityStaff" ).HasKey( w => w.PersonID );
            modelBuilder.Entity<Secretary>().ToTable( "Secretaries" ).HasKey( w => w.PersonID );
            modelBuilder.Entity<Administrator>().ToTable( "Administrator" ).HasKey( a => a.PersonID );
                                                                            
            #endregion

            modelBuilder.Configurations.Add(new WorkEntityConfig());
               


            #region Locations

            modelBuilder.Configurations.Add( new BuildingEntityConfig() );

            modelBuilder.Configurations.Add( new RoomEntityConfig() );

            modelBuilder.Configurations.Add( new OfficeEntityConfig() );
            modelBuilder.Configurations.Add( new WarehouseEntityConfig() );

            modelBuilder.Configurations.Add( new BlockEntityConfig() );
            modelBuilder.Configurations.Add( new CellEntityConfig() );

            modelBuilder.Entity<SHU>().ToTable( "SHUs" ).HasKey( b => b.RoomID );

            #endregion


            base.OnModelCreating( modelBuilder );
        }


        #region DbSets


        #region People

        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Warden> Wardens { get; set; }
        public virtual DbSet<Officer> Officers { get; set; }
        public virtual DbSet<Guard> Guards { get; set; }
        public virtual DbSet<Secretary> Secretaries { get; set; }
        public virtual DbSet<Medical.Medical> Medical { get; set; }
        public virtual DbSet<Maintenance.Maintenance> Maintenance { get; set; }
        public virtual DbSet<Inmate.Inmate> Inmates { get; set; }
        public virtual DbSet<Visitor> Visitors { get; set; }
        public virtual DbSet<Administrator> Administrators { get; set; } 

        #endregion

        public virtual DbSet<Work> Works { get; set; } 

        #region Locations

        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Building> Buildings { get; set; }
        public virtual DbSet<Block> Blocks { get; set; }
        public virtual DbSet<Cell> Cells { get; set; }
        public virtual DbSet<SHU> SHUs { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }
        public virtual DbSet<Office> Offices { get; set; }

        #endregion


        #endregion
    }
}