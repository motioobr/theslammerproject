﻿using System.Data.Entity.ModelConfiguration;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class MaintenanceEntityConfig : EntityTypeConfiguration<Maintenance.Maintenance>
    {
        internal MaintenanceEntityConfig()
        {
            ToTable("MaintenanceStaff");

            HasKey( m => m.PersonID );

            HasMany( m => m.Works )
                .WithOptional(w => w.Worker)
                .HasForeignKey( w => w.WorkerID );


        }
    }
}