﻿using System.Data.Entity.ModelConfiguration;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class InmateEntityConfig : EntityTypeConfiguration<Inmate.Inmate>
    {
        internal InmateEntityConfig()
        {
            ToTable( "Inmates" );

            HasKey( i => i.PersonID );

            HasMany( i => i.Treatments )
                .WithRequired( t => t.Patient )
                .HasForeignKey( t => t.PatientID );

            HasMany( i => i.Visits )
                .WithRequired( v => v.Inmate )
                .HasForeignKey( v => v.InmateID );

            // Property( i => i.DangerLevel )
            //    .HasColumnName( "Danger" );

            Property( i => i.Religion )
                .HasColumnName( "Religion" );
        }
    }
}