﻿using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class GuardEntityConfig : EntityTypeConfiguration<Guard>
    {
        internal GuardEntityConfig()
        {
            ToTable( "Guards" );

            HasKey( g => g.PersonID );

            HasMany( g => g.Cells )
                .WithRequired( c => c.Guard )
                .HasForeignKey( c => c.GuardID );
        }
    }
}