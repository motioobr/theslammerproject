﻿using System.Data.Entity.ModelConfiguration;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class MedicalEntityConfig : EntityTypeConfiguration<Medical.Medical>
    {
        internal MedicalEntityConfig()
        {
            ToTable( "MedicalStaff" );

            HasKey( m => m.PersonID );

            HasMany( m => m.Treatments )
                .WithRequired( t => t.Doctor )
                .HasForeignKey( t => t.DoctorID );
        }
    }
}