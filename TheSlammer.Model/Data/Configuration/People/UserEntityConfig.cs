﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class UserEntityConfig : EntityTypeConfiguration<User>
    {
        internal UserEntityConfig()
        {
            ToTable( "Users" );

            HasKey( u => u.PersonID );

            Property( u => u.Username )
                .HasMaxLength( 30 )
                .IsRequired()
                .HasColumnAnnotation(
                                     "Index",
                                     new IndexAnnotation( new IndexAttribute( "Unique_Username" ) {IsUnique = true} ) );

            Property( u => u.Password )
                .HasMaxLength( 30 )
                .IsRequired();
        }
    }
}