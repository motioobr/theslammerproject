﻿using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class OfficerEntityConfig : EntityTypeConfiguration<Officer>
    {
        internal OfficerEntityConfig()
        {
            ToTable( "Officers" );

            HasKey( o => o.PersonID );

            HasMany( o => o.Blocks )
                .WithRequired( b => b.Officer )
                .HasForeignKey( b => b.OfficerID )
                .WillCascadeOnDelete();
        }
    }
}