﻿using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Visit;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class VisitorEntityConfig : EntityTypeConfiguration<Visitor>
    {
        internal VisitorEntityConfig()
        {
            ToTable( "Visitors" );

            HasKey( v => v.PersonID );

            HasMany( v => v.Visits )
                .WithRequired( v => v.Visitor )
                .HasForeignKey( v => v.VisitorID );
        }
    }
}