﻿using System.Data.Entity.ModelConfiguration;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class ManagementEntityConfig : EntityTypeConfiguration<Management.Management>
    {
        internal ManagementEntityConfig()
        {
            ToTable( "ManagementStaff" );

            HasKey( m => m.PersonID );

            HasRequired( m => m.Office )
                .WithMany()
                .HasForeignKey( m => m.OfficeID )
                .WillCascadeOnDelete();
        }
    }
}