﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Data.Configuration.People
{
    internal class PersonEntityConfig : EntityTypeConfiguration<Person>
    {
        internal PersonEntityConfig()
        {
            ToTable( "People" );

            HasKey( p => p.PersonID );

            Property( p => p.PersonID )
                .HasDatabaseGeneratedOption( DatabaseGeneratedOption.Identity );

            Property( x => x.Gender )
                .HasColumnName( "Gender" );

            Property( x => x.FirstName )
                .HasMaxLength( 30 )
                .IsRequired();

            Property( x => x.LastName )
                .HasMaxLength( 30 )
                .IsRequired();

            Property( x => x.DateOfBirth )
                .HasColumnName( "DOB" )
                .HasColumnType( "date" );
        }
    }
}