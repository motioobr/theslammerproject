﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Locations;

namespace TheSlammer.Model.Data.Configuration.Locations
{
    internal class RoomEntityConfig : EntityTypeConfiguration<Room>
    {
        internal RoomEntityConfig()
        {
            ToTable( "Rooms" );

            HasKey( r => r.RoomID );

            Property( r => r.RoomID )
                .HasDatabaseGeneratedOption( DatabaseGeneratedOption.Identity );
        }
    }
}