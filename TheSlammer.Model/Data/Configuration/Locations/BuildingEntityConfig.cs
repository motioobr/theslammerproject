﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Locations;

namespace TheSlammer.Model.Data.Configuration.Locations
{
    internal class BuildingEntityConfig : EntityTypeConfiguration<Building>
    {
        internal BuildingEntityConfig()
        {
            ToTable( "Buildings" );

            HasKey( b => b.BuildingID );

            Property( b => b.Name )
                .IsRequired()
                .HasMaxLength( 30 );

            Property( b => b.BuildingID )
                .HasDatabaseGeneratedOption( DatabaseGeneratedOption.Identity );

            HasMany( b => b.Rooms )
                .WithRequired( r => r.Building )
                .HasForeignKey( r => r.BuildingID )
                .WillCascadeOnDelete();
        }
    }
}