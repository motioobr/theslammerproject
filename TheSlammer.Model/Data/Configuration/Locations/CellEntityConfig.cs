﻿using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Inmate;

namespace TheSlammer.Model.Data.Configuration.Locations
{
    internal class CellEntityConfig : EntityTypeConfiguration<Cell>
    {
        internal CellEntityConfig()
        {
            ToTable( "Cells" );

            HasKey( c => c.RoomID );

            HasMany( c => c.Inmates )
                .WithRequired( i => i.Cell )
                .HasForeignKey( i => i.CellID )
                .WillCascadeOnDelete();
        }
    }
}