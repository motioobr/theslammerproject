﻿using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Data.Configuration.Locations
{
    internal class BlockEntityConfig : EntityTypeConfiguration<Block>
    {
        internal BlockEntityConfig()
        {
            ToTable( "Blocks" );

            HasKey( b => b.RoomID );

            HasMany( b => b.Cells )
                .WithRequired( c => c.Block )
                .HasForeignKey( c => c.BlockID )
                .WillCascadeOnDelete();

            HasRequired( b => b.Officer )
                .WithMany( o => o.Blocks )
                .HasForeignKey( b => b.OfficerID )
                .WillCascadeOnDelete();
        }
    }
}