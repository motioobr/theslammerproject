﻿using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Management;

namespace TheSlammer.Model.Data.Configuration.Locations
{
    internal class OfficeEntityConfig : EntityTypeConfiguration<Office>
    {
        internal OfficeEntityConfig()
        {
            ToTable( "Offices" );

            HasKey( o => o.RoomID );
        }
    }
}