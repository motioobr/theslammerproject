﻿using System.Data.Entity.ModelConfiguration;
using TheSlammer.Model.Maintenance;

namespace TheSlammer.Model.Data.Configuration.Locations
{
    internal class WarehouseEntityConfig : EntityTypeConfiguration<Warehouse>
    {
        internal WarehouseEntityConfig()
        {
            ToTable( "Warehouses" );

            HasKey( w => w.RoomID );
            /*
            HasRequired( w => w.Manager )
                .WithMany( m => m.Warehouses )
                .HasForeignKey( w => w.ManagerID )
                .WillCascadeOnDelete();*/
        }
    }
}