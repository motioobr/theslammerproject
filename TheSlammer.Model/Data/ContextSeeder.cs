﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Management;
using TheSlammer.Model.Security;
using TheSlammer.Model.Visit;

namespace TheSlammer.Model.Data
{
    internal class ContextSeeder : DropCreateDatabaseAlways<Context>// DropCreateDatabaseIfModelChanges<Context> 
    {
        protected override void Seed( Context context )
        {
            CustomSeed( context );
            base.Seed( context );

            // TODO add all unique constraints after db build
        }

        private static void CustomSeed( Context context )
        {
            #region Fill lists

            FillBuildings();
            FillOffices();

            FillWardens();
            FillSecretaries();

            FillMaintenance();
            FillWarehouses();
            FillAdministrator();
            FillOfficers();
            FillGuard();
            FillBlocks();
            FillCells();
            FillInmates();
            FillWorks();
            FillMedical();
            FillVisitors();

            OfficeToManagment();

            #endregion


            AddToContext( context );

            context.SaveChanges();
        }
      
        private static void AddToContext( Context context )
        {
            context.Wardens.AddRange( _wardens );
            context.Secretaries.AddRange( _secretaries );
            context.Guards.AddRange( _guards );
            context.Officers.AddRange( _officers );
            context.Medical.AddRange( _medicalStaff );
            context.Maintenance.AddRange( _maintenanceStaff );
            context.Visitors.AddRange( _visitors );
            context.Inmates.AddRange( _inmates );
            context.Administrators.AddRange( _administrators );

            context.Works.AddRange( _works );
            context.Buildings.AddRange( _buildings );
            context.Offices.AddRange( _offices );
            context.Warehouses.AddRange( _warehouses );
            context.Blocks.AddRange( _blocks );
            context.Cells.AddRange( _cells );
        }

        private static void FillBlocks()
        {
            _blocks = new List<Block>();

            Random random = new Random();
            const int maxBlocks = 20;

            for ( int i = 0; i < maxBlocks; i++ )
            {
                int rand = random.Next( 0, 5 );

                for ( int j = 0; j < rand; j++ )
                {
                    _blocks.Add( new Block
                    {
                        Building = _buildings[ random.Next( _buildings.Count ) ],
                        Officer  = _officers [ random.Next( _officers.Count ) ]
                    } );
                }
            }
        }

        private static void FillCells()
        {
            _cells = new List<Cell>();
            Random random = new Random();

            foreach ( var block in _blocks )
            {
                int maxCellsInBlock = random.Next( 10, 30 );

                for ( int i = 0; i < maxCellsInBlock; i++ )
                {
                    _cells.Add( new Cell
                    {
                        Building = block.Building,
                        Capacity = (byte)random.Next( 2, 5 ),
                        Block    = block,
                        Guard    = _guards[ random.Next( _guards.Count ) ]
                    } );
                }
            }
        }

        private static void OfficeToManagment()
        {
            var officeCount = 0;

            foreach ( var secretary in _secretaries ) secretary.Office = _offices[ officeCount++ ];

            foreach ( var warden in _wardens ) warden.Office = _offices[ officeCount++ ];
        }

        private static void FillVisitors()
        {
            _visitors = new List<Visitor>
            {
                new Visitor
                {
                    FirstName = "Brandon",
                    LastName = "Hall",
                    Gender = Enums.Gender.Male,
                    IdNumber = 365123695,
                    DateOfBirth = new DateTime( 1972, 10, 6 )
                },
                new Visitor
                {
                    FirstName = "Amanda",
                    LastName = "Payne",
                    Gender = Enums.Gender.Female,
                    IdNumber = 853059476,
                    DateOfBirth = new DateTime( 1968, 9, 15 )
                },
                new Visitor
                {
                    FirstName = "Fred",
                    LastName = "Payne",
                    Gender = Enums.Gender.Male,
                    IdNumber = 235910238,
                    DateOfBirth = new DateTime( 1988, 9, 18 )
                },
                new Visitor
                {
                    FirstName = "Nicholas",
                    LastName = "Johnson",
                    Gender = Enums.Gender.Male,
                    IdNumber = 423069862,
                    DateOfBirth = new DateTime( 1966, 4, 19 )
                },
                new Visitor
                {
                    FirstName = "Louis",
                    LastName = "Evans",
                    Gender = Enums.Gender.Male,
                    IdNumber = 203594621,
                    DateOfBirth = new DateTime( 1992, 6, 24 )
                },
                new Visitor
                {
                    FirstName = "Philip",
                    LastName = "Dean",
                    Gender = Enums.Gender.Male,
                    IdNumber = 423068953,
                    DateOfBirth = new DateTime( 1973, 4, 15 )
                },
                new Visitor
                {
                    FirstName = "Timothy",
                    LastName = "Mills",
                    Gender = Enums.Gender.Male,
                    IdNumber = 865320159,
                    DateOfBirth = new DateTime( 1986, 2, 8 )
                },
                new Visitor
                {
                    FirstName = "Roger",
                    LastName = "Matthews",
                    Gender = Enums.Gender.Male,
                    IdNumber = 423065986,
                    DateOfBirth = new DateTime( 1958, 6, 21 )
                },
                new Visitor
                {
                    FirstName = "Pamela",
                    LastName = "Spencer",
                    Gender = Enums.Gender.Female,
                    IdNumber = 230659863,
                    DateOfBirth = new DateTime( 1966, 10, 3 )
                },
                new Visitor
                {
                    FirstName = "Andrew",
                    LastName = "Olson",
                    Gender = Enums.Gender.Male,
                    IdNumber = 301596387,
                    DateOfBirth = new DateTime( 1979, 8, 17 )
                }
            };
        }

        private static void FillInmates()
        {
            #region Inmates as text

            string inmatesAsText = @"Adam,Roberts,633276224,09/15/1991,0,5,1,2.07,03/23/2000,3,,136,11
Douglas,Wright,722217794,02/18/1957,5,4,2,1.82,09/28/1997,0,04/07/2053,151,3
Bruce,Payne,732881429,03/25/1959,4,4,0,1.57,03/16/1967,0,07/01/2054,172,39
Gerald,Miller,611451453,03/28/1966,3,3,2,2.12,01/17/1984,6,09/27/2074,78,28
Thomas,Welch,564700457,09/29/1969,1,4,0,2.18,02/20/1958,4,02/01/2037,166,40
Phillip,James,499885928,07/04/1955,0,6,1,2.03,09/17/2003,2,09/25/2028,51,10
Martin,Morris,338164079,05/21/1967,6,5,0,1.62,03/06/1978,5,,197,7
Bobby,Hawkins,854832969,12/21/1981,1,8,2,2.13,05/06/1985,5,,129,15
Joshua,Henderson,694094189,12/04/1968,3,2,2,1.26,10/17/1957,1,11/09/2036,143,33
Johnny,James,611437092,06/17/1985,0,8,1,2.02,12/26/1999,2,12/29/2019,146,37
Thomas,Rice,652038744,01/16/1960,1,6,1,1.97,12/13/1972,2,12/06/2049,179,7
Earl,Coleman,609731112,04/06/1954,2,4,2,1.43,05/24/1955,4,08/06/2084,157,19
Gregory,Miller,651187326,10/01/1983,0,6,1,1.39,03/04/1980,1,12/28/2047,44,19
Brian,Washington,199046925,11/03/1973,2,6,1,1.84,08/02/1964,4,06/12/2017,165,37
Chris,Lane,321432557,08/05/1991,2,1,2,1.98,01/22/1976,5,06/03/2037,48,19
Ryan,Gonzales,824705614,04/22/1952,6,8,0,1.58,08/24/1969,2,08/05/2062,185,21
Gregory,Johnson,119837133,09/18/1983,5,2,2,2.06,11/03/2011,0,11/04/2093,82,2
David,Phillips,158303657,04/30/1955,4,8,1,1.27,11/06/1971,4,08/10/2016,58,17
Michael,Romero,257586182,01/03/1959,2,8,1,1.81,07/27/2005,4,06/18/2056,166,25
Joe,Rice,523919112,02/28/1983,3,2,1,2.04,03/27/2013,3,10/12/2021,192,35
Albert,Nichols,207001504,11/21/1951,2,2,1,2.1,09/21/1974,5,01/21/2036,82,3
Ryan,Wallace,367856741,01/28/1953,4,9,0,1.55,12/25/2003,4,09/19/2075,140,31
Phillip,West,415028381,04/12/1969,1,9,1,2.19,03/31/2011,5,05/16/2015,143,12
Adam,Jacobs,350001372,07/10/1955,4,7,2,2.02,03/16/1987,6,01/13/2041,190,8
Michael,Patterson,191273570,06/18/1969,3,3,1,1.41,09/26/1977,2,,38,30
Joseph,Ellis,209490564,02/06/1993,1,6,1,1.98,03/08/1985,5,11/14/2033,39,22
George,Cunningham,422808816,05/21/1957,4,9,0,1.84,04/09/2011,6,11/28/2085,165,27
Shawn,Carpenter,527950634,12/13/1980,6,0,1,1.46,03/14/1994,6,08/02/2024,98,9
Bruce,Mccoy,275841714,02/24/1956,6,6,0,1.45,06/09/2009,6,01/03/2086,156,38
Earl,Rogers,641269954,02/26/1968,6,9,1,1.24,11/26/1965,1,05/12/2033,39,25
Alan,Graham,499734951,10/22/1970,6,6,1,1.43,04/14/1977,3,09/22/2074,122,20
Dennis,Day,353341114,07/17/1964,0,3,0,2.14,07/01/1982,3,01/23/2045,60,5
Sean,Gordon,341755615,08/26/1971,0,3,0,1.88,02/05/1962,5,05/24/2017,178,2
Timothy,Ryan,881009015,12/22/1987,2,3,1,2.11,02/01/1986,0,07/07/2054,165,12
Arthur,Turner,723300062,04/17/1993,3,1,2,2.02,06/08/1991,6,,186,5
Dennis,Hamilton,822653462,03/11/1968,3,9,1,1.48,01/02/1995,4,08/14/2046,175,6
Joshua,Sanders,792647315,09/11/1971,3,9,1,1.85,02/09/1967,4,04/26/2075,88,26
Mark,Brooks,816034750,11/12/1982,0,7,0,1.96,11/25/1987,1,08/11/2078,158,37
Steve,Hernandez,543833072,02/11/1965,3,6,0,1.32,07/22/1984,3,,88,18
Alan,Robinson,801922618,05/31/1961,0,0,0,2.17,03/13/1996,4,06/04/2068,133,17
Justin,Washington,749257710,07/29/1950,0,2,1,1.55,01/25/1961,4,10/03/2088,35,26
Andrew,Ryan,207914719,06/09/1967,3,3,0,2.06,11/21/1968,0,01/28/2049,176,12
Joseph,Cook,866931030,02/20/1964,3,4,2,1.75,05/14/2012,6,11/01/2016,42,17
Mark,Hunter,449126226,08/04/1984,0,7,2,1.39,05/07/2015,4,06/30/2031,146,13
Thomas,Rodriguez,530597862,12/08/1955,3,9,0,1.42,12/21/1994,0,05/17/2074,199,26
Joseph,Dean,541575673,08/13/1978,2,8,2,1.65,09/01/2002,4,12/15/2064,190,20
Michael,Harper,210527136,05/22/1976,6,4,1,1.59,09/17/1987,3,08/30/2026,46,24
Raymond,Dixon,211826742,07/27/1969,3,2,1,2.19,02/20/2013,3,09/03/2026,164,28
Gary,Andrews,332200004,07/22/1964,5,1,2,1.76,12/16/1998,4,11/11/2029,115,3
Steve,Andrews,545491628,06/22/1971,4,9,1,1.31,03/23/1958,3,10/22/2018,171,18
Jose,Jones,881487326,01/11/1967,5,2,2,1.5,05/26/1965,0,12/04/2090,115,20
Patrick,Flores,411713441,12/29/1989,2,1,2,1.31,09/08/1989,3,11/17/2081,89,12
Jimmy,Richards,277340303,05/10/1956,2,4,1,1.89,07/02/2005,5,03/14/2095,178,27
Jeffrey,Ryan,348909240,12/30/1971,1,4,0,1.56,05/08/1960,1,06/08/2077,174,39
Brian,Ruiz,607191650,12/29/1967,1,6,0,1.24,07/30/1996,1,11/28/2043,195,25
Joshua,Knight,884061790,04/03/1983,2,5,0,1.9,01/18/1995,2,05/18/2086,121,25
Patrick,Olson,447017820,03/28/1960,2,3,0,1.84,12/27/2008,5,11/22/2061,142,35
Raymond,Rogers,686553165,03/11/1967,0,7,1,1.51,08/15/2002,1,04/24/2074,41,1
Frank,Bennett,549748954,04/17/1965,0,1,0,2.18,12/23/1980,4,11/01/2063,73,18
Jeremy,Meyer,604019890,02/16/1974,4,1,0,1.84,01/13/1976,3,,159,40
Roger,Mills,837194099,04/17/1974,6,9,0,1.39,03/19/1990,0,,182,29
Gregory,Jones,350999972,09/23/1953,3,5,0,1.88,06/19/1955,1,08/17/2039,46,24
Frank,Alvarez,348252669,11/13/1991,1,8,0,1.71,12/18/2005,1,04/26/2067,36,24
Randy,Schmidt,875014296,02/14/1965,0,3,2,1.54,02/21/1994,3,,160,2
Charles,Wheeler,185114265,07/20/1950,4,3,2,1.31,11/14/2006,1,06/24/2079,199,20
Gerald,Lee,314000720,04/07/1972,0,9,1,2.13,01/05/1987,6,10/04/2021,46,12
Jose,Lewis,604388105,03/21/1981,0,5,1,1.99,08/01/1973,3,08/04/2080,93,24
Ernest,Roberts,321372676,07/09/1981,0,3,0,1.51,03/17/1968,5,,37,37
Carlos,Peters,592825299,10/01/1977,5,9,1,1.76,01/02/1987,2,03/22/2047,198,8
Patrick,Fox,342237699,08/06/1979,6,5,0,1.2,02/13/1966,6,04/07/2029,51,31
Larry,Mitchell,791863363,09/30/1987,0,3,2,1.56,12/24/1958,2,10/24/2061,57,31
Jose,Morales,429828131,07/08/1968,1,3,1,1.91,12/13/2003,6,07/17/2059,51,27
Benjamin,Cox,166009826,10/07/1976,2,5,1,2.15,09/23/2012,1,02/13/2078,104,9
Nicholas,Reed,184472116,04/03/1963,4,9,2,1.72,07/25/1987,1,11/23/2055,153,33
Joseph,Olson,176173238,01/10/1986,3,6,2,1.93,03/27/2001,0,03/14/2016,76,32
Craig,Garrett,152216716,11/29/1971,3,0,2,2.12,09/29/1955,4,12/20/2058,43,34
Todd,Stewart,453689414,12/17/1971,4,9,1,1.61,11/16/1968,1,,57,25
Russell,Day,849249889,07/29/1983,0,9,2,1.59,05/07/2006,5,,130,5
Eugene,Richards,127515363,06/05/1965,2,6,2,1.66,06/26/1965,3,02/24/2098,73,10
Jesse,Stanley,459695401,05/14/1974,3,4,2,1.6,08/05/1980,2,12/15/2083,179,11
Gregory,Gutierrez,486477716,06/19/1950,0,7,2,1.64,01/27/2005,2,08/07/2029,135,2
David,Brooks,194072336,02/06/1977,1,0,1,1.44,01/25/2002,6,02/20/2019,93,31
Henry,Peters,645328740,08/23/1987,1,4,1,1.41,10/21/2010,6,08/31/2040,52,8
Daniel,Lawson,522196710,07/25/1957,3,6,0,1.8,07/12/1981,3,11/23/2062,129,21
Philip,Greene,640173500,01/23/1966,3,5,2,1.7,10/24/2006,3,12/20/2057,133,4
Ernest,Howell,719448856,03/11/1985,3,7,0,1.59,03/06/1982,6,08/31/2017,92,4
Willie,West,377684379,12/18/1989,2,5,1,1.64,11/13/2005,5,05/20/2046,139,25
Robert,Weaver,172230669,08/12/1981,4,5,1,1.96,12/31/1966,1,03/29/2026,36,29
Richard,Rogers,740473190,04/17/1964,3,5,2,1.33,02/17/1984,0,07/11/2020,82,26
Albert,Welch,546722972,11/28/1982,4,3,0,1.66,11/18/1968,5,10/21/2088,121,9
Richard,Ryan,533297400,12/19/1988,2,4,0,1.45,08/21/1964,1,09/14/2086,135,19
Roy,Elliott,676416892,03/23/1992,1,6,0,2.14,06/13/2009,5,11/07/2082,199,33
Bobby,Grant,717741828,02/09/1956,6,9,2,2.2,04/12/1970,4,04/22/2080,68,27
Gerald,Williams,569256077,06/03/1951,6,2,0,1.58,04/22/1966,4,,148,28
Fred,Mcdonald,728169004,01/01/1953,6,8,1,1.5,03/12/1974,2,09/06/2018,189,0
Larry,Hawkins,426909964,07/01/1983,0,7,2,1.44,03/11/2004,2,,149,13
Michael,Evans,489964876,09/05/1983,4,6,0,1.73,07/01/2009,5,12/16/2076,44,2
Jack,Wilson,265915467,02/12/1966,3,5,0,1.58,12/25/2013,5,04/04/2035,121,25
Jonathan,Owens,382291809,08/02/1978,4,2,0,1.97,05/15/1979,6,10/07/2028,87,7
Harry,Robertson,267599857,02/12/1966,0,0,2,1.52,12/16/1969,2,06/09/2097,58,20
Patrick,Mason,441361345,12/17/1969,4,1,0,1.23,06/29/1978,6,10/11/2074,64,33
Timothy,Ferguson,128174388,01/04/1981,4,6,0,1.85,04/07/2000,5,06/04/2054,54,14
Raymond,Nelson,127254775,03/04/1983,4,2,1,1.62,01/06/2015,4,03/07/2021,194,30
Eric,Reid,367807869,03/09/1962,6,6,0,1.93,05/05/1997,1,10/19/2049,94,36
Scott,Torres,419024178,12/05/1958,2,0,0,1.57,01/26/1967,0,01/16/2056,101,25
George,Parker,806367458,12/22/1962,2,3,2,2.06,10/26/1988,3,09/11/2042,163,16
Jonathan,Ellis,605296576,01/20/1978,5,6,0,1.43,07/07/2010,4,,195,16
Raymond,Hernandez,490201080,03/10/1972,4,7,1,1.94,02/06/1972,0,07/04/2098,176,14
Victor,Bennett,423350975,05/10/1980,6,8,2,1.97,08/28/1965,2,02/25/2039,128,35
Aaron,Cook,730029699,05/19/1980,6,1,0,2.03,09/10/1980,3,11/03/2068,168,35
Juan,Sullivan,388254846,05/28/1965,2,7,1,1.68,09/04/2004,1,05/29/2078,46,22
Keith,Elliott,850795467,12/23/1983,0,7,0,1.77,06/08/1967,5,07/11/2033,137,26
Harold,Hansen,848316949,11/05/1966,6,6,1,1.67,04/19/1989,3,03/08/2061,88,25
Ernest,Cooper,856063063,10/28/1950,6,7,1,2.15,10/10/1956,5,09/11/2046,196,31
Eugene,Lynch,227604991,05/05/1959,0,7,1,1.97,09/27/1975,4,06/22/2024,91,20
Keith,Martin,165521752,09/01/1982,2,2,2,1.23,04/01/1961,3,02/05/2098,67,40
Dennis,Crawford,421827612,02/20/1973,4,7,1,2.1,03/09/2009,0,10/08/2033,101,33
David,George,184698568,02/17/1976,1,6,2,1.42,03/18/1974,2,05/12/2064,91,3
Jerry,Morris,747251090,04/13/1990,0,1,2,1.52,01/30/1991,6,12/28/2034,63,12
Victor,Garza,418142929,06/13/1979,0,2,1,2.14,12/07/1973,2,11/12/2071,75,17
Joseph,Perkins,124868344,10/03/1956,1,2,0,2.2,03/28/1995,3,,86,4
Kevin,Ruiz,701146978,08/07/1950,4,4,0,1.52,08/25/1969,0,01/23/2017,96,14
Matthew,Chavez,253850169,07/02/1977,4,0,2,2.0,03/22/1972,1,08/26/2076,137,29
Robert,Watkins,123545403,01/13/1961,2,8,2,1.38,04/11/1990,1,08/24/2074,80,30
Howard,Warren,798741860,01/16/1963,2,6,2,1.82,07/04/1981,6,10/21/2088,124,13
Albert,Thomas,754654886,04/27/1992,2,6,0,1.26,07/18/1970,2,03/11/2021,102,5
Andrew,Miller,589449396,10/18/1955,4,6,1,1.83,05/26/1968,0,09/15/2022,149,5
Frank,Mendoza,690270743,09/19/1987,5,6,0,1.54,07/17/1964,4,02/18/2076,67,9
Daniel,Morales,121911772,03/06/1982,4,5,1,1.68,05/01/1959,3,07/08/2089,170,21
Eric,Taylor,546247390,09/03/1982,2,8,2,1.72,07/30/1992,6,07/18/2042,115,1
Justin,Kennedy,112834646,02/25/1966,4,4,0,2.15,04/07/1972,2,01/31/2094,133,33
Craig,Garza,488639248,12/31/1959,4,9,1,1.91,06/17/1956,5,03/10/2031,184,37
Fred,Sullivan,168153796,05/24/1976,6,8,1,2.07,06/20/1970,3,10/16/2066,166,16
Bobby,Burton,364515894,08/06/1991,4,5,2,1.96,09/10/1977,2,05/07/2090,169,0
Jonathan,Cox,857067059,09/15/1962,2,6,2,1.89,11/20/2003,2,02/26/2050,95,22
Peter,Gomez,473990504,06/01/1963,6,7,2,1.55,01/01/2010,2,,116,24
James,Jones,239703901,10/13/1963,6,0,1,1.21,11/08/2002,0,02/06/2061,112,3
Eugene,Meyer,204618388,10/21/1950,3,1,0,1.71,10/09/1968,4,03/26/2082,179,37
Chris,Riley,626527169,01/29/1971,3,0,1,1.62,05/13/1964,4,,193,28
Mark,Campbell,429191446,01/15/1952,3,0,0,1.7,02/23/2003,6,01/04/2058,122,1
Phillip,Castillo,172230539,04/14/1977,3,2,1,1.21,11/03/2014,2,10/14/2056,186,38
Ronald,Simpson,339039002,10/20/1959,4,7,0,1.23,01/04/1971,0,,191,25
Steven,Ramos,379699814,05/21/1972,6,2,1,1.21,01/30/1956,5,10/08/2018,150,5
Alan,Grant,141000970,02/26/1988,5,8,1,1.4,06/07/1998,0,06/04/2072,97,17
Michael,Williamson,471245110,07/31/1990,5,2,1,1.75,06/15/1967,1,04/11/2020,132,7
Eric,Price,619659499,03/04/1978,6,5,2,1.41,06/08/1962,1,11/21/2056,197,26
Gary,Chavez,388773600,11/28/1978,4,7,0,1.7,09/28/2000,0,08/07/2053,149,19
Mark,Cunningham,672385086,12/02/1962,2,5,2,1.76,06/30/2000,6,04/28/2056,85,23
Jonathan,Peters,131051788,10/27/1971,1,1,2,2.17,01/31/1962,0,09/06/2043,37,13
Victor,Palmer,672874814,05/14/1982,3,5,0,1.64,09/22/1999,0,12/06/2044,169,16
Jesse,Cox,789160164,11/05/1968,6,0,1,1.96,10/10/2002,4,01/09/2093,173,28
Scott,Ferguson,406142649,07/18/1991,3,4,0,1.37,04/04/2003,4,09/23/2056,194,38
Dennis,Jacobs,725169935,04/29/1982,5,3,0,1.67,12/03/1992,4,02/16/2027,41,40
Richard,Banks,168027924,11/03/1985,6,3,2,1.78,03/13/1965,2,08/02/2032,178,35
Ronald,Murphy,245344267,07/07/1987,1,1,1,2.15,01/04/1983,0,,115,6
Walter,Stephens,507295413,04/04/1968,6,7,1,1.5,03/23/1959,2,12/01/2039,38,5
Peter,Wood,328843144,06/12/1986,5,9,2,1.99,12/14/2009,5,08/31/2044,68,21
Sean,Harper,155047916,06/02/1960,6,4,1,1.55,04/23/1956,3,05/15/2016,105,0
Clarence,Morales,341945532,01/09/1958,1,1,0,2.05,07/15/2006,0,11/15/2024,176,23
Earl,Webb,859739488,01/03/1955,4,1,1,1.75,12/22/1965,4,07/12/2077,192,19
Donald,Kennedy,249137952,04/03/1951,5,8,0,2.12,08/26/1966,0,05/30/2015,49,33
Craig,Dixon,346010145,09/22/1960,1,8,1,1.76,06/05/1992,5,09/16/2017,161,1
Shawn,Kim,640777680,04/26/1969,0,5,2,1.42,07/29/2006,3,09/23/2066,137,19
Ralph,Hunt,746476895,11/03/1962,5,6,2,2.19,06/14/1988,1,03/21/2033,143,23
Bobby,Moore,379514191,03/17/1970,0,9,0,1.42,01/13/2012,2,02/07/2048,60,4
Phillip,Weaver,292477676,06/16/1990,4,2,0,1.65,04/27/2005,5,04/21/2087,136,26
Jack,Elliott,318534704,10/06/1973,0,7,2,1.94,06/07/1975,5,06/05/2087,65,23
Martin,Rogers,251603820,10/17/1959,0,9,2,1.4,01/17/1994,4,07/14/2052,186,33
Johnny,Fuller,581011895,02/11/1951,5,5,2,1.48,08/15/1986,1,03/30/2079,196,11
Jonathan,Schmidt,213703540,12/11/1983,0,6,0,1.96,02/07/2012,6,01/02/2072,117,1
Larry,Stone,637788468,01/28/1960,1,5,2,2.09,07/24/1989,4,12/31/2029,200,38
Howard,Fernandez,226404544,02/25/1984,6,9,0,1.63,03/21/1983,6,08/11/2046,141,21
Juan,Davis,730433535,07/29/1973,5,6,2,2.08,12/20/1982,1,01/30/2051,121,12
Ernest,Henry,143444445,06/14/1985,5,5,2,2.04,08/11/1962,4,12/13/2095,166,19
Mark,Young,390187513,07/17/1971,2,9,1,1.53,05/07/1995,1,11/05/2051,80,31
Jason,Chapman,860047046,03/30/1962,3,9,2,1.84,08/26/2007,6,10/18/2096,174,2
Charles,Crawford,677599235,07/06/1980,6,6,2,1.47,05/09/1965,5,04/05/2061,168,2
Lawrence,Parker,767870691,10/02/1961,6,9,2,1.83,08/19/2013,2,11/01/2039,87,9
Chris,Watkins,610896447,05/11/1981,4,0,2,1.4,05/30/1959,2,11/17/2028,56,26
Ronald,Bishop,626147974,11/17/1952,5,2,0,2.1,02/08/2005,0,06/19/2015,94,35
Bruce,Reed,830817558,11/08/1976,1,7,2,2.04,07/10/1984,2,01/10/2094,125,17
Richard,Bradley,251657271,05/11/1983,1,1,0,1.4,10/18/1961,5,07/22/2066,35,12
James,Sanders,771835460,02/05/1982,3,0,0,2.02,05/24/2014,4,01/01/2051,40,27
David,Thomas,382737016,12/01/1983,5,3,1,1.84,09/21/1994,0,12/12/2016,87,8
Jeffrey,Woods,218468761,12/26/1976,1,2,0,1.89,02/14/1967,2,02/27/2061,144,13
Kevin,Bell,663300901,06/21/1982,5,7,1,1.24,02/22/1999,3,12/31/2095,120,3
Thomas,Stephens,884208760,11/15/1987,5,3,1,1.22,05/17/2014,0,12/17/2034,45,21
Victor,Grant,803748439,12/27/1951,5,7,1,1.55,09/08/2003,5,06/08/2092,96,33
Joe,Larson,605431851,01/28/1953,1,7,1,2.17,01/25/1984,0,02/23/2045,172,26
Craig,Olson,362727183,08/21/1980,6,3,0,1.85,09/08/1996,4,07/25/2031,136,35
Ernest,Lawson,206990116,12/08/1963,0,4,1,1.98,07/30/1995,6,10/02/2090,91,6
Wayne,Burke,792879840,03/12/1951,4,4,0,1.69,07/10/1972,2,07/27/2062,89,27
Martin,Arnold,491152821,06/07/1973,6,2,1,2.15,09/22/1989,1,07/07/2064,171,30
Bobby,Hall,435669681,01/22/1966,2,8,0,1.76,02/28/1989,5,09/03/2080,179,28
Gregory,Wagner,566845816,06/06/1960,1,8,2,1.77,01/02/2011,5,11/02/2059,174,7
Willie,Cox,520888621,12/23/1972,2,1,0,2.16,03/16/1978,6,03/05/2044,116,36
Ralph,Clark,845564348,02/26/1988,3,5,0,2.05,12/09/2007,2,09/03/2053,162,2
Jeremy,Hansen,826754834,03/07/1960,6,3,2,2.09,03/15/2006,2,09/23/2058,197,12
Billy,Murphy,301754278,06/24/1983,0,8,2,1.34,05/11/1978,5,02/27/2058,125,39
Phillip,Mcdonald,840554931,03/08/1984,3,6,0,1.8,08/03/1972,1,12/14/2076,82,29
Steve,Smith,669944998,11/20/1984,0,5,2,1.26,06/06/1982,1,04/08/2034,179,39
Gerald,Mills,468911762,11/21/1989,5,2,1,1.51,11/06/1959,2,,83,32
Gary,Evans,508831483,12/21/1962,5,2,1,1.51,10/13/1987,3,02/24/2076,38,22
Matthew,Garza,285926481,01/29/1986,6,9,2,1.28,10/31/2013,4,01/12/2049,94,19
Eric,Roberts,251003066,10/29/1968,4,3,2,1.57,08/04/1977,4,07/30/2061,44,21
John,Kelly,499409928,12/22/1991,5,5,1,1.64,10/19/2012,2,03/05/2099,112,9
Justin,Perry,352070246,10/05/1987,4,1,1,1.49,04/24/1985,4,03/29/2085,84,13
Louis,Bradley,748637524,05/21/1957,3,7,1,2.09,07/07/1957,3,04/27/2068,173,35
Richard,Hanson,798934493,03/16/1954,2,2,1,1.96,01/03/1959,4,04/02/2099,102,11
Douglas,Fowler,713747498,12/10/1954,3,8,2,1.64,06/10/1965,0,10/06/2061,181,20
Todd,Spencer,551454597,07/28/1992,3,6,1,1.73,08/30/1982,6,11/23/2033,120,17
Joe,Morales,816268710,06/18/1978,6,0,1,2.11,10/06/1984,2,09/03/2047,126,29
Antonio,Harrison,764160741,01/04/1985,0,9,2,1.55,01/09/1994,6,05/22/2060,124,7
Jeffrey,Young,192975002,05/09/1956,4,4,0,1.87,02/14/1996,5,12/10/2090,86,8
Paul,Phillips,515024787,03/24/1970,1,1,1,1.76,01/04/2006,2,01/10/2066,73,25
Philip,Turner,304983977,07/10/1950,0,8,1,1.53,06/01/1966,1,07/30/2047,193,12
Harold,Peters,796722580,12/19/1953,6,9,0,1.93,01/05/2014,4,07/21/2039,128,35
Robert,Hicks,170858926,08/09/1981,1,2,1,2.16,12/04/1978,5,08/09/2052,177,14
Edward,Stone,776827782,01/10/1990,4,4,1,2.14,12/21/1983,4,07/29/2062,132,26
Alan,Cook,607675319,02/01/1971,1,2,2,1.76,11/30/2012,6,08/27/2058,155,12
Brandon,Barnes,818398446,10/08/1981,0,7,2,1.48,02/14/2002,2,06/06/2075,113,21
Scott,Hayes,458200608,02/09/1988,4,4,1,1.56,08/22/1978,4,07/05/2022,174,30
Donald,Warren,419833621,11/25/1992,6,2,0,1.26,10/20/1985,6,07/20/2015,135,27
Ernest,Rivera,737585123,01/30/1958,1,6,1,1.94,12/23/2001,6,09/12/2087,198,30
Ryan,Grant,839780777,10/14/1991,1,9,2,1.86,01/12/1971,0,04/08/2096,95,34
Alan,Burton,444491553,12/31/1981,6,1,2,1.72,02/11/1966,6,04/24/2094,184,1
Keith,Edwards,375548275,02/12/1968,3,5,2,1.5,01/03/1958,3,11/08/2085,65,26
Stephen,Ramirez,189781465,10/31/1981,1,6,0,1.86,02/03/1972,5,05/07/2029,180,4
Roger,Hall,424875196,01/08/1953,5,8,0,1.8,09/09/2000,5,11/30/2059,197,23
Dennis,Mason,465289326,06/05/1985,0,6,0,2.12,08/03/1995,1,02/19/2097,172,26
Frank,Marshall,588549184,03/10/1990,0,9,0,1.39,10/29/1973,6,09/20/2054,193,7
Roy,Watson,571927587,03/27/1987,4,4,0,1.54,05/21/2012,2,08/04/2091,198,22
Joshua,Miller,316740111,06/18/1982,4,9,0,1.44,03/21/1989,5,05/30/2097,143,39
Jason,Morales,720705509,12/03/1954,5,5,2,1.77,12/07/2005,1,11/04/2034,51,0
Randy,Wood,512940913,08/29/1973,0,3,0,1.36,07/04/1988,5,05/09/2037,119,23
Ryan,Powell,467731550,07/29/1977,4,7,2,1.89,12/18/1972,4,12/14/2066,61,14
Carl,Flores,139712235,07/21/1976,6,2,2,1.4,08/24/1994,6,,73,34
Ryan,Robinson,535168037,04/10/1952,4,4,0,2.17,07/15/2004,4,11/29/2084,60,25
Roger,Duncan,478111885,09/30/1961,1,6,1,2.06,04/30/1975,2,05/22/2028,99,27
Willie,Boyd,429087017,03/28/1982,2,3,2,1.8,06/04/1989,5,04/26/2038,134,32
Antonio,Fernandez,306256517,07/19/1970,2,8,2,1.55,07/29/2010,3,02/09/2064,37,32
Wayne,Bryant,491380066,11/02/1981,0,7,0,1.88,07/07/1961,6,01/08/2036,102,1
Michael,Frazier,510077067,06/03/1965,6,8,1,1.54,02/12/1977,3,07/03/2053,174,19
Keith,Thompson,596094821,02/26/1954,5,3,2,1.43,11/01/1978,6,08/25/2044,137,12
Robert,Adams,737916572,12/14/1984,3,0,2,2.05,10/01/1974,0,,120,37
Adam,Vasquez,573302242,01/06/1957,6,8,1,1.55,04/27/1976,2,05/27/2017,90,30
Kevin,Johnston,466706802,09/17/1986,4,0,2,1.82,01/15/1969,2,02/23/2061,92,10
Steve,Mason,263855121,11/20/1986,4,8,2,1.8,05/17/1976,0,01/12/2064,161,34
Arthur,Medina,285635337,12/25/1972,5,3,2,1.74,10/11/1974,4,03/11/2073,109,21
Craig,Fields,180254195,05/25/1981,1,7,1,1.92,09/02/1957,6,11/06/2066,108,40
Mark,Miller,383910761,08/06/1957,2,3,0,1.58,10/18/1961,2,10/20/2017,70,14
Joe,Cunningham,737548553,08/04/1989,0,6,2,1.91,09/19/1959,1,08/01/2066,181,1
Earl,Cox,745525248,07/01/1990,4,4,1,2.13,03/24/1999,6,01/10/2031,162,4
Eric,Brown,138662817,02/01/1986,4,5,2,1.44,12/17/1971,2,,163,35
Shawn,Phillips,534674144,08/09/1956,3,4,1,1.63,03/20/1971,4,11/20/2034,196,10
Peter,Kennedy,276726291,04/03/1966,0,3,2,1.59,07/19/1958,4,08/13/2080,41,38
Carlos,Medina,311345693,09/12/1982,6,2,1,1.57,03/15/1969,1,11/18/2044,89,29
Andrew,Larson,861777275,10/05/1969,4,9,0,1.68,05/24/1985,6,05/13/2036,76,36
Jack,Parker,636704079,03/09/1993,5,1,2,1.95,09/23/1981,6,06/24/2034,157,13
Ryan,Little,794811688,12/16/1962,2,4,2,1.35,10/15/1996,3,,177,11
Andrew,Sims,378347008,01/17/1992,0,2,0,1.57,04/18/1993,0,,131,4
Jack,Dunn,684767859,12/17/1963,1,5,1,2.12,09/04/1998,0,08/25/2058,178,15
Carlos,Cruz,340503604,06/11/1974,3,0,0,1.86,06/14/1989,5,01/23/2079,170,13
Johnny,Williamson,567158548,07/10/1982,0,7,1,2.12,09/12/1968,0,,185,12
David,Mcdonald,629133026,09/14/1967,6,0,0,1.43,08/04/2010,1,03/02/2021,135,6
Timothy,Daniels,544729742,07/11/1966,0,4,0,1.52,09/30/1978,5,12/15/2058,92,13
Henry,Nguyen,534078558,01/06/1981,1,5,0,1.94,07/05/1996,4,01/19/2036,143,31
Todd,Williamson,475651552,02/27/1958,3,1,2,1.95,07/09/2007,1,11/28/2054,171,24
Henry,Ramos,724564882,01/25/1954,5,6,2,1.62,03/27/1963,2,,134,27
Jeremy,Morris,882924726,12/03/1963,3,2,1,1.93,03/19/2007,2,04/03/2072,135,9
Stephen,Harvey,572461121,04/28/1956,3,9,2,1.95,01/08/2006,4,04/09/2083,130,1
Billy,Larson,844950708,06/12/1971,5,1,2,1.54,01/31/1956,1,02/19/2024,179,23
Walter,Fernandez,133702879,12/12/1971,1,2,0,1.53,05/27/1968,4,05/21/2080,70,38
Adam,Andrews,737667269,09/24/1979,6,1,2,1.32,07/26/1980,5,,71,25
Stephen,Griffin,573102835,08/30/1955,0,6,1,1.36,03/29/1978,5,02/16/2049,93,13
Gregory,Stewart,423790699,09/08/1990,1,0,2,1.25,06/06/2007,1,,185,6
Charles,Anderson,144986869,08/03/1962,0,7,1,1.41,07/06/1963,0,11/30/2075,70,33
Chris,Howard,306677454,07/30/1955,1,0,2,1.93,07/07/1968,0,03/26/2059,122,18
Craig,Alvarez,694517034,01/07/1979,5,9,0,2.01,01/02/2010,1,,72,11
Victor,Berry,167754112,08/11/1978,4,3,1,2.13,05/28/2003,6,05/27/2058,183,30
James,Reynolds,598798201,06/02/1969,4,6,1,1.38,04/12/1984,5,03/27/2040,52,11
Mark,Harvey,704197228,03/22/1952,1,8,2,1.7,03/09/2003,2,02/27/2037,185,1
Stephen,Carr,186984983,05/21/1981,6,0,0,1.38,02/07/1974,3,06/26/2073,194,9
Jeffrey,Meyer,589359713,07/23/1984,3,6,1,1.21,02/07/1986,3,09/14/2021,113,24
Louis,Perez,351729160,06/17/1966,6,3,2,1.26,02/17/1960,6,07/02/2070,124,18
Henry,Sanchez,352273342,02/08/1961,1,8,0,1.62,02/03/1965,0,10/16/2048,196,36
Thomas,Wood,291021979,11/29/1971,0,2,2,2.03,11/26/1981,6,09/13/2073,167,4
Bobby,Castillo,277171558,06/05/1964,1,7,0,1.39,10/12/2013,2,03/14/2058,72,35
Mark,Lopez,730601941,12/18/1964,1,0,0,1.73,12/30/1995,2,04/16/2090,109,1
Justin,Moreno,585844604,11/29/1985,2,0,0,2.01,06/12/1957,4,01/16/2066,128,27
Russell,Graham,208865139,07/07/1961,5,5,0,1.45,10/12/1968,0,11/24/2047,108,5
Timothy,Romero,131517391,09/05/1950,2,1,1,1.35,10/08/1988,3,06/12/2027,77,5
Willie,Henderson,348888381,08/31/1962,1,5,2,1.55,05/31/1978,0,03/07/2053,83,19
Jason,Little,791524421,03/27/1970,6,3,0,1.5,01/16/1990,1,05/02/2071,85,14
Martin,Morales,853357623,07/11/1966,6,7,2,1.54,02/25/2003,4,01/05/2051,117,6
Dennis,Ruiz,195913647,11/15/1985,0,8,1,2.03,11/06/2012,4,,189,23
Matthew,Collins,484607094,04/27/1975,3,0,2,2.14,06/08/1968,5,06/14/2071,172,40
Ralph,Burton,170189641,10/12/1970,3,3,1,1.79,05/16/1958,0,11/07/2044,127,8
Benjamin,Hansen,426573905,07/17/1990,6,0,0,2.01,03/21/1980,2,01/25/2097,141,8
Larry,Owens,519824877,04/04/1984,0,9,2,1.73,12/25/1992,1,05/20/2097,179,39";

            #endregion

            _inmates = new List<Inmate.Inmate>();

            int currentCell = 0;
            int currentCapacity = 0;
            Random rand = new Random();

            foreach ( var line in inmatesAsText.Split( '\n' ) )
            {
                string[] i = line.Split( ',' );

                int health = Int32.Parse( i[ 6 ] );

                if ( currentCapacity == _cells[ currentCell ].Capacity )
                {
                    currentCell++;
                    currentCapacity = 0;
                }
                
                _inmates.Add(
                             new Inmate.Inmate
                             {
                                 FirstName = i[ 0 ],
                                 LastName = i[ 1 ],
                                 Gender = Enums.Gender.Male,
                                 IdNumber = rand.Next( 111111111, 888888888 ),
                                 DateOfBirth = DateTime.ParseExact( i[ 3 ], "MM/dd/yyyy", null ),
                                 Eye = (Enums.EyeColor)rand.Next(0,6),
                                 Hair = (Enums.HairColor)rand.Next( 0, 9 ),
                                 Health = (Enums.Health)health,
                                 Height = decimal.Parse( i[ 7 ] ),
                                 IncarcerationDate = DateTime.ParseExact( i[ 8 ], "MM/dd/yyyy", null ),
                                 InCustody = health != 4 && health != 3,
                                 Religion = (Enums.Religion)rand.Next( 0, 6 ),
                                 ReleaseDate =
                                     string.IsNullOrWhiteSpace( i[ 10 ] )
                                         ? (DateTime?)null
                                         : DateTime.ParseExact( i[ 10 ], "MM/dd/yyyy", null ),
                                 Weight = decimal.Parse( i[ 11 ] ),
                                 MonthlyVisits = (byte)rand.Next( 0, 30 ),
                                 Cell = _cells[ currentCell ]
                             } );
            }
        }

        private static void FillGuard()
        {
            _guards = new List<Guard>
            {
                new Guard
                {
                    FirstName = "Tammy",
                    LastName = "Stewart",
                    Gender = Enums.Gender.Female,
                    IdNumber = 202552155,
                    Username = "tammyst6",
                    Password = "theslammer",
                    DateOfBirth = new DateTime()
                },
                new Guard
                {
                    FirstName = "Joan",
                    LastName = "Henry",
                    Gender = Enums.Gender.Female,
                    IdNumber = 228513654,
                    Username = "joanhen7",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1969, 8, 1 )
                },
                new Guard
                {
                    FirstName = "Eugene",
                    LastName = "Graham",
                    Gender = Enums.Gender.Male,
                    IdNumber = 303153542,
                    Username = "eugenegr7",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1975, 6, 30 )
                },
                new Guard
                {
                    FirstName = "Lois",
                    LastName = "Holmes",
                    Gender = Enums.Gender.Female,
                    IdNumber = 321554852,
                    Username = "loishol4",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1984, 5, 2 )
                },
                new Guard
                {
                    FirstName = "Victor",
                    LastName = "Cook",
                    Gender = Enums.Gender.Male,
                    IdNumber = 154897625,
                    Username = "viccook8",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1984, 5, 31 )
                },
                new Guard
                {
                    FirstName = "Brandon",
                    LastName = "Smith",
                    Gender = Enums.Gender.Male,
                    IdNumber = 215364856,
                    Username = "bransm7",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1987, 3, 15 )
                },
                new Guard
                {
                    FirstName = "Alice",
                    LastName = "Palmer",
                    Gender = Enums.Gender.Female,
                    IdNumber = 213584852,
                    Username = "alicepal0",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1959, 5, 25 )
                },
                new Guard
                {
                    FirstName = "Linda",
                    LastName = "Arnold",
                    Gender = Enums.Gender.Female,
                    IdNumber = 951842364,
                    Username = "lindaarn3",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1988, 8, 8 )
                },
                new Guard
                {
                    FirstName = "Cynthia",
                    LastName = "Payne",
                    Gender = Enums.Gender.Female,
                    IdNumber = 184532112,
                    Username = "cynthiap9",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1954, 9, 5 )
                },
                new Guard
                {
                    FirstName = "Andrew",
                    LastName = "Adams",
                    Gender = Enums.Gender.Male,
                    IdNumber = 156155435,
                    Username = "andrewad5",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1977, 9, 24 )
                }
            };
        }

        private static void FillOfficers()
        {
            _officers = new List<Officer>
            {
                new Officer
                {
                    FirstName = "Catherine",
                    LastName = "Powell",
                    Gender = Enums.Gender.Female,
                    IdNumber = 589645213,
                    Username = "catherinepo9",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1966, 1, 18 )
                },
                new Officer
                {
                    FirstName = "Jacqueline",
                    LastName = "George",
                    Gender = Enums.Gender.Female,
                    IdNumber = 215123912,
                    Username = "jacquelineg5",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1977, 2, 28 )
                },
                new Officer
                {
                    FirstName = "Patricia",
                    LastName = "Hawkins",
                    Gender = Enums.Gender.Female,
                    IdNumber = 203158565,
                    Username = "patrichaw1",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1989, 1, 6 )
                },
                new Officer
                {
                    FirstName = "Jacqueline",
                    LastName = "Larson",
                    Gender = Enums.Gender.Female,
                    IdNumber = 254810230,
                    Username = "jacquelar3",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1979, 8, 22 )
                },
                new Officer
                {
                    FirstName = "Julia",
                    LastName = "George",
                    Gender = Enums.Gender.Female,
                    IdNumber = 584685489,
                    Username = "juliage3",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1972, 8, 5 )
                },
                new Officer
                {
                    FirstName = "Gary",
                    LastName = "Walker",
                    Gender = Enums.Gender.Male,
                    IdNumber = 514896521,
                    Username = "garywal7",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1979, 4, 16 )
                },
                new Officer
                {
                    FirstName = "Linda",
                    LastName = "Bowman",
                    Gender = Enums.Gender.Female,
                    IdNumber = 512486845,
                    Username = "lindabow9",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1984, 8, 27 )
                },
                new Officer
                {
                    FirstName = "David",
                    LastName = "Hernandez",
                    Gender = Enums.Gender.Male,
                    IdNumber = 215054812,
                    Username = "davidher5",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1986, 6, 19 )
                },
                new Officer
                {
                    FirstName = "Alan",
                    LastName = "Ryan",
                    Gender = Enums.Gender.Male,
                    IdNumber = 521354685,
                    Username = "alanryan5",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1995, 4, 26 )
                },
                new Officer
                {
                    FirstName = "Jason",
                    LastName = "Grant",
                    Gender = Enums.Gender.Male,
                    IdNumber = 230156482,
                    Username = "jasongr7",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1985, 11, 30 )
                }
            };
        }

        private static void FillMedical()
        {
            _medicalStaff = new List<Medical.Medical>
            {
                new Medical.Medical
                {
                    FirstName = "Anna",
                    LastName = "Larson",
                    Gender = Enums.Gender.Female,
                    IdNumber = 569865398,
                    Username = "iAnnalLar9",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1982, 2, 4 ),
                    Expertises = Enums.MedicalExperties.Dentist
                },
                new Medical.Medical
                {
                    FirstName = "Lisa",
                    LastName = "Flores",
                    Gender = Enums.Gender.Female,
                    IdNumber = 568975423,
                    Username = "ilisaflos0",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1985, 5, 4 ),
                    Expertises = Enums.MedicalExperties.Nurse
                },
                new Medical.Medical
                {
                    FirstName = "Earl",
                    LastName = "Wilson",
                    Gender = Enums.Gender.Male,
                    IdNumber = 968542365,
                    Username = "earlwil5",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1991, 7, 11 ),
                    Expertises = Enums.MedicalExperties.Radiologist | Enums.MedicalExperties.Physicians
                },
                new Medical.Medical
                {
                    FirstName = "Emily",
                    LastName = "Stanley",
                    Gender = Enums.Gender.Female,
                    IdNumber = 236598746,
                    Username = "emilynley6",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1988, 12, 1 ),
                    Expertises = Enums.MedicalExperties.SocialWorker
                },
                new Medical.Medical
                {
                    FirstName = "Phillip",
                    LastName = "Fisher",
                    Gender = Enums.Gender.Male,
                    IdNumber = 423659863,
                    Username = "phillip56",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1979, 4, 12 ),
                    Expertises = Enums.MedicalExperties.Physicians | Enums.MedicalExperties.Nurse
                },
                new Medical.Medical
                {
                    FirstName = "Jerry",
                    LastName = "Fox",
                    Gender = Enums.Gender.Male,
                    IdNumber = 201547859,
                    Username = "Jerryf8",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1985, 7, 2 ),
                    Expertises = Enums.MedicalExperties.Physicians | Enums.MedicalExperties.Radiologist
                },
                new Medical.Medical
                {
                    FirstName = "Jack",
                    LastName = "Fuller",
                    Gender = Enums.Gender.Male,
                    IdNumber = 253015891,
                    Username = "jackful3",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1975, 12, 25 ),
                    Expertises = Enums.MedicalExperties.Physicians | Enums.MedicalExperties.Other
                },
                new Medical.Medical
                {
                    FirstName = "Heather",
                    LastName = "Rogers",
                    Gender = Enums.Gender.Female,
                    IdNumber = 623015952,
                    Username = "heatherr7",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1992, 11, 21 ),
                    Expertises = Enums.MedicalExperties.Dentist
                },
                new Medical.Medical
                {
                    FirstName = "Robert",
                    LastName = "Williamson",
                    Gender = Enums.Gender.Male,
                    IdNumber = 785963201,
                    Username = "roberwil8",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1977, 10, 16 ),
                    Expertises = Enums.MedicalExperties.Psychiatrist | Enums.MedicalExperties.SocialWorker
                },
                new Medical.Medical
                {
                    FirstName = "Harold",
                    LastName = "Bishop",
                    Gender = Enums.Gender.Male,
                    IdNumber = 423069410,
                    Username = "harodbis2",
                    Password = "theslammer",
                    Expertises = Enums.MedicalExperties.Nurse | Enums.MedicalExperties.Other
                }
            };
        }

        private static void FillWarehouses()
        {
            _warehouses = new List<Warehouse>();

            for ( var i = 0; i < _buildings.Count && i < _maintenanceStaff.Count; i++ )
            {
                _warehouses.Add(
                                new Warehouse
                                {
                                    Building = _buildings[ i ],
                                    //Manager = _maintenanceStaff[ i ]
                                } );
            }
        }

        private static void FillMaintenance()
        {
            _maintenanceStaff = new List<Maintenance.Maintenance>
            {
                new Maintenance.Maintenance
                {
                    FirstName = "Joe",
                    LastName = "Bradley",
                    Gender = Enums.Gender.Male,
                    IdNumber = 354440977,
                    Username = "jbradleyb",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1982, 12, 12 ),
                    Job = Enums.MaintenanceType.Cleaner | Enums.MaintenanceType.Cook
                },
                new Maintenance.Maintenance
                {
                    FirstName = "Clarence",
                    LastName = "Adams",
                    Gender = Enums.Gender.Male,
                    IdNumber = 364295387,
                    Username = "cadams8",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1980, 5, 5 ),
                    Job = Enums.MaintenanceType.Cleaner | Enums.MaintenanceType.Handyman
                },
                new Maintenance.Maintenance
                {
                    FirstName = "Andrew",
                    LastName = "Frazier",
                    Gender = Enums.Gender.Male,
                    IdNumber = 830912772,
                    Username = "afrazierd",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1982, 4, 19 ),
                    Job =
                        Enums.MaintenanceType.Cleaner | Enums.MaintenanceType.Handyman |
                        Enums.MaintenanceType.Electrician
                },
                new Maintenance.Maintenance
                {
                    FirstName = "Ryan",
                    LastName = "Montgomery",
                    Gender = Enums.Gender.Male,
                    IdNumber = 757561641,
                    Username = "rmontgomeryh",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1990, 10, 06 ),
                    Job = Enums.MaintenanceType.General
                },
                new Maintenance.Maintenance
                {
                    FirstName = "James",
                    LastName = "Jordan",
                    Gender = Enums.Gender.Male,
                    IdNumber = 241042695,
                    Username = "jjordani",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1987, 1, 25 ),
                    Job = Enums.MaintenanceType.Logistician
                },
                new Maintenance.Maintenance
                {
                    FirstName = "Lawrence",
                    LastName = "Taylor",
                    Gender = Enums.Gender.Male,
                    IdNumber = 266563019,
                    Username = "ltaylorj",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1976, 6, 12 ),
                    Job = Enums.MaintenanceType.Cleaner | Enums.MaintenanceType.Cook | Enums.MaintenanceType.Handyman
                },
                new Maintenance.Maintenance
                {
                    FirstName = "Donna",
                    LastName = "Ferguson",
                    Gender = Enums.Gender.Female,
                    IdNumber = 851132533,
                    Username = "dfergusonl",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1975, 9, 12 ),
                    Job = Enums.MaintenanceType.Logistician | Enums.MaintenanceType.Cook
                }
            };
        }

        private static void FillWorks()
        {
            _works = new List<Work>
            {
                new Work
                {
                    Job = Enums.MaintenanceType.Cleaner,
                    IsCompleted = true,
                    WorkDate = new DateTime( 2013, 01, 12 ),
                    Worker = _maintenanceStaff.Find( m => m.Job == Enums.MaintenanceType.Cleaner ),
                    WorkType = Enums.WorkType.Other
                },
                new Work
                {
                    Job = Enums.MaintenanceType.Cook,
                    IsCompleted = true,
                    WorkDate = new DateTime( 2014, 06, 12 ),
                    Worker = _maintenanceStaff.Find( m => m.Job == Enums.MaintenanceType.Cook ),
                    WorkType = Enums.WorkType.Repair
                },
                new Work
                {
                    Job = Enums.MaintenanceType.Electrician,
                    IsCompleted = false,
                    WorkDate = new DateTime( 2016, 11, 4 ),
                    WorkType = Enums.WorkType.Building
                },
                new Work
                {
                    Job = Enums.MaintenanceType.Cleaner,
                    IsCompleted = false,
                    WorkDate = new DateTime( 2015, 06, 12 ),
                    Worker = _maintenanceStaff.Find( m => m.Job == Enums.MaintenanceType.Cleaner ),
                    WorkType = Enums.WorkType.Other
                },
                new Work
                {
                    Job = Enums.MaintenanceType.Handyman,
                    IsCompleted = false,
                    WorkDate = new DateTime( 2015, 01, 12 ),
                    Worker = _maintenanceStaff.Find( m => m.Job == Enums.MaintenanceType.Handyman ),
                    WorkType = Enums.WorkType.Other
                },
                new Work
                {
                    Job = Enums.MaintenanceType.General,
                    IsCompleted = true,
                    WorkDate = new DateTime( 2013, 01, 12 ),
                    Worker = _maintenanceStaff.Find( m => m.Job == Enums.MaintenanceType.General ),
                    WorkType = Enums.WorkType.Other
                },
                new Work
                {
                    Job = Enums.MaintenanceType.Cleaner,
                    IsCompleted = false,
                    Worker = _maintenanceStaff.Find( m => m.Job == Enums.MaintenanceType.Cleaner ),
                    WorkType = Enums.WorkType.Other
                }
            };









        }

        private static void FillSecretaries()
        {
            _secretaries = new List<Secretary>
            {
                new Secretary
                {
                    FirstName = "Sarah",
                    LastName = "Brown",
                    Gender = Enums.Gender.Female,
                    IdNumber = 627548228,
                    Username = "sbrown2",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1982, 03, 21 )
                },
                new Secretary
                {
                    FirstName = "Laura",
                    LastName = "Henry",
                    Gender = Enums.Gender.Female,
                    IdNumber = 755050371,
                    Username = "lhenry6",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1975, 11, 01 )
                },
                new Secretary
                {
                    FirstName = "Robert",
                    LastName = "Wheeler",
                    Gender = Enums.Gender.Male,
                    IdNumber = 553281208,
                    Username = "rwheeler3",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1988, 06, 15 )
                },
                new Secretary
                {
                    FirstName = "Melissa",
                    LastName = "Fowler",
                    Gender = Enums.Gender.Female,
                    IdNumber = 336312379,
                    Username = "mfowler03",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1982, 03, 14 )
                }
            };
        }

        private static void FillWardens()
        {
            _wardens = new List<Warden>
            {
                new Warden
                {
                    FirstName = "Walter",
                    LastName = "Gonzalez",
                    Gender = Enums.Gender.Male,
                    IdNumber = 488336043,
                    Username = "wgonzalez2",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1970, 10, 10 )
                },
                new Warden
                {
                    FirstName = "Alan",
                    LastName = "Walker",
                    Gender = Enums.Gender.Male,
                    IdNumber = 301883013,
                    Username = "awalker0",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1967, 05, 01 )
                },
                new Warden
                {
                    FirstName = "Steven",
                    LastName = "Powell",
                    Gender = Enums.Gender.Male,
                    IdNumber = 270025840,
                    Username = "spowell0",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1981, 04, 21 )
                }
            };
        }

        private static void FillOffices()
        {
            _offices = new List<Office>();

            foreach ( var building in _buildings )
            {
                _offices.Add( new Office {Building = building} );
                _offices.Add( new Office {Building = building} );
                _offices.Add( new Office {Building = building} );
            }
        }

        private static void FillAdministrator()
        {
            _administrators = new List<Administrator>
            {
                new Administrator
                {
                    FirstName = "dadi",
                    LastName = "graber",
                    Gender = Enums.Gender.Male,
                    IdNumber = 488336043,
                    Username = "abc",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1970, 10, 10 )
                },
                new Administrator
                {
                    FirstName = "jaccob",
                    LastName = "israekli",
                    Gender = Enums.Gender.Male,
                    IdNumber = 301883013,
                    Username = "a",
                    Password = "1",
                    DateOfBirth = new DateTime( 1967, 05, 01 )
                },
                new Administrator
                {
                    FirstName = "stev",
                    LastName = "steve",
                    Gender = Enums.Gender.Male,
                    IdNumber = 270025840,
                    Username = "werter",
                    Password = "theslammer",
                    DateOfBirth = new DateTime( 1981, 04, 21 )
                }
            };
        }

        private static void FillBuildings()
        {
            _buildings = new List<Building>();

            foreach (
                var name in
                    "Apricot,Aubergine,Avocado,Clementine,Fig,Guava,Honeydew,Kiwi,Nectarine,Pumpkin,Turnip".Split( ',' )
                ) _buildings.Add( new Building {Name = name} );
        }


        #region Fields and Lists

        private static List<Warden> _wardens;
        private static List<Secretary> _secretaries;
        private static List<Medical.Medical> _medicalStaff;
        private static List<Maintenance.Maintenance> _maintenanceStaff;
        private static List<Guard> _guards;
        private static List<Officer> _officers;
        private static List<Inmate.Inmate> _inmates;
        private static List<Visitor> _visitors;
        private static List<Administrator> _administrators;

        private static List<Work> _works; 

        private static List<Building> _buildings;
        private static List<Office> _offices;
        private static List<Block> _blocks;
        private static List<Warehouse> _warehouses;
        private static List<Cell> _cells;
        private static List<SHU> _shus;
        private static int Officer;

        #endregion
    }
}