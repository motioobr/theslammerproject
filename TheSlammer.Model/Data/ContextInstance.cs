﻿using System.Runtime.Remoting.Messaging;

namespace TheSlammer.Model.Data
{
    public class ContextInstance
    {
        private static Context _data;

        private ContextInstance() {}

        public static Context Data { get  { return _data ?? ( _data = new Context() ); } }
    }
}