﻿using System;
using System.Security.Cryptography;
using System.Text;
using TheSlammer.Model.Interfaces;

namespace TheSlammer.Model.People
{
    public abstract class User : Person, IUser
    {
        protected User( Enums.Role role ) : base( role ) {}


        #region Ecryption

        public static string Encrypt( string plainText )
        {
            byte[] plainTextBytes = Encoding.ASCII.GetBytes( plainText );

            AesCryptoServiceProvider aes = new AesCryptoServiceProvider
            {
                BlockSize = 128,
                KeySize = 256,
                Key = Encoding.ASCII.GetBytes( "TheSlammer'sReallySafePrivateKey" ),
                IV = Encoding.ASCII.GetBytes( "OurSafePublicKey" ),
                Padding = PaddingMode.PKCS7,
                Mode = CipherMode.CBC
            };

            ICryptoTransform crypto = aes.CreateEncryptor( aes.Key, aes.IV );
            byte[] encryptedBytes = crypto.TransformFinalBlock( plainTextBytes, 0, plainText.Length );

            crypto.Dispose();

            return Convert.ToBase64String( encryptedBytes );
        }

        protected static string Decrypt( string encryptedText )
        {
            byte[] encryptedBytes = Convert.FromBase64String( encryptedText );

            AesCryptoServiceProvider aes = new AesCryptoServiceProvider
            {
                BlockSize = 128,
                KeySize = 256,
                Key = Encoding.ASCII.GetBytes( "TheSlammer'sReallySafePrivateKey" ),
                IV = Encoding.ASCII.GetBytes( "OurSafePublicKey" ),
                Padding = PaddingMode.PKCS7,
                Mode = CipherMode.CBC
            };

            ICryptoTransform crypto = aes.CreateDecryptor( aes.Key, aes.IV );
            byte[] decryptedBytes = crypto.TransformFinalBlock( encryptedBytes, 0, encryptedBytes.Length );

            crypto.Dispose();

            return Encoding.ASCII.GetString( decryptedBytes );
        }

        #endregion


        #region Fields/Properties

        /// <summary>Holds the unencrypted value of the password</summary>
        private string _password;

        public string Username { get; set; }

        /// <summary>Will always return or set an encrypted version of the password.</summary>
        public string Password
        {
            get { return _password; }
            set { _password = value; } /*
            get { return Encrypt( _password ?? string.Empty ); }
            set
            {
                _password = ( string.IsNullOrWhiteSpace( value ) )
                                ? string.Empty
                                : Decrypt( value );
            }
            */
        }

        #endregion
    }
}