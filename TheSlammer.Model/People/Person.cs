﻿using System;
using TheSlammer.Model.Interfaces;

namespace TheSlammer.Model.People
{
    public abstract class Person : IPerson
    {
        protected Person( Enums.Role role )
        {
            _role = role;
        }


        #region Fields/Properties

        /// <summary>Should be set on construction based on the object type, not a dynamic value</summary>
        private readonly Enums.Role _role;

        public Enums.Role PersonRole { get { return _role; } }

        public int PersonID { get; set; }

        public int IdNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public Enums.Gender Gender { get; set; }

        // The concatenated version of the user's name
        public string Name { get { return FirstName + " " + LastName; } }

        #endregion
    }
}