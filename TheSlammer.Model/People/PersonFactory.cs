﻿using System.ComponentModel;
using TheSlammer.Model.Management;
using TheSlammer.Model.Security;
using TheSlammer.Model.Visit;

namespace TheSlammer.Model.People
{
    public sealed class PersonFactory
    {
        /// <summary>Generate a person based strictly on their role</summary>
        /// <param name="role">The person's role</param>
        /// <returns>An object that inherits the Person class, or null</returns>
        public static Person GeneratePerson( Enums.Role role )
        {
            switch ( role )
            {
                case Enums.Role.Guard :
                    return new Guard();

                case Enums.Role.Inmate :
                    return new Inmate.Inmate();

                case Enums.Role.Maintenance :
                    return new Maintenance.Maintenance();

                case Enums.Role.Medical :
                    return new Medical.Medical();

                case Enums.Role.Officer :
                    return new Officer();

                case Enums.Role.Secretary :
                    return new Secretary();

                case Enums.Role.Visitor :
                    return new Visitor();

                case Enums.Role.Warden :
                    return new Warden();

                default :
                    throw new InvalidEnumArgumentException();
            }
        }
    }
}