﻿using System;
using System.Collections.Generic;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Visit
{
    [Authorization(
        Level.Management,
        Level.Management | Level.Security,
        Level.Management | Level.Security,
        Level.Management )]
    public class Visitor : Person, IVisitor
    {
        public Visitor() : base( Enums.Role.Visitor )
        {
            Visits = new List<Visit>();
        }


        #region Fields/Properties

        public virtual ICollection<Visit> Visits { get; set; }

        #endregion


        
    }
}