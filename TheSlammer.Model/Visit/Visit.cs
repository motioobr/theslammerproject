﻿using System;
using TheSlammer.Model.Interfaces;

namespace TheSlammer.Model.Visit
{
    [Authorization(
        Level.Management | Level.Officer,
        Level.Management | Level.Security,
        Level.Management | Level.Security,
        Level.Management | Level.Officer )]
    public class Visit : IVisit
    {
        #region Fields/Properties

        public int VisitID { get; set; }
        public Enums.Relationship Relation { get; set; }
        public bool WasAttended { get; set; }
        public bool WasIntturupted { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Durtion { get; set; }
        public int InmateID { get; set; }
        public virtual Inmate.Inmate Inmate { get; set; }
        public int VisitorID { get; set; }
        public virtual Visitor Visitor { get; set; }

        #endregion
    }
}