﻿using System;
using System.ComponentModel;

namespace TheSlammer.Model
{
    public class Enums
    {
        #region Location related

        public enum Location
        {
            Building,
            Block,
            Cell,
            Office,
            Warehouse
        }

        #endregion


        /// <summary>The relationship between the inmate and their visitor</summary>
        public enum Relationship : byte
        {
            Father,
            Mother,
            Child,
            Spouse,
            Sibling,
            Friend,
            Legal,
            Medical,
            Other
        };


        #region Maintenance details

        /// <summary>List of maintenance job types, uses bit-flags for multi-select</summary>
        [Flags]
        public enum MaintenanceType : byte
        {
            Cleaner = 1,
            Handyman = 2,
            Electrician = 4,
            Logistician = 8,
            Cook = 16,
            General = 32
        };

        /// <summary>Type of job done by a maintenance worker</summary>
        public enum WorkType
        {
            Building,
            Repair,
            Cleaning,
            Other
        }

        #endregion


        #region Physical details

        /// <summary>List of religions used for inmates</summary>
        public enum Religion
        {
            Jewish,
            Muslim,
            Christian,
            Atheist,
            Buddhist,
            Hindu,
            Other
        };

        /// <summary>The various roles of different users/nonusers</summary>
        public enum Role : int
        {
            Warden = 1,
            Secretary = 2,
            Officer = 4,
            Guard = 8,
            Medical = 16,
            Administrator = 32,
            Maintenance = 64,
            Inmate = 128,
            Visitor = 256
        }

        /// <summary>List of various eye colors</summary>
        public enum EyeColor : byte
        {
            Amber,
            Blue,
            Brown,
            Gray,
            Green,
            Hazel,
            [Description( "Red Violet" )] RedViolet
        };

        public enum Gender : byte
        {
            Male,
            Female
        };

        /// <summary>List of various hair colors</summary>
        public enum HairColor : byte
        {
            Black,
            Brown,
            Blond,
            Auburn,
            Chestnut,
            Red,
            Gray,
            White,
            Bald,
            Other
        };

        #endregion


        #region Medical related

        /// <summary>List of various medical expertise, uses bit-flags to allow multi-select</summary>
        [Flags]
        public enum MedicalExperties : byte
        {
            Dentist = 1,
            Nurse = 2,
            Physicians = 4,
            Psychiatrist = 8,
            Radiologist = 16,
            [Description( "Social Worker" )] SocialWorker = 32,
            Other = 64
        };

        /// <summary>Health types and statuses</summary>
        public enum Health : byte
        {
            Undefined,
            Good,
            Fair,
            Serious,
            Critical
        };

        /// <summary>Type of medical treatments</summary>
        public enum TreatmentType : byte
        {
            Checkup,
            Bandage,
            Stitches,
            FirstAid,
            Cast,
            Medication,
            Dental,
            SocialWork,
            Mental,
            Other
        }

        #endregion
    }
}