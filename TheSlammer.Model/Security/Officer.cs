﻿using System;
using System.Collections.Generic;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;

namespace TheSlammer.Model.Security
{
    [Authorization(
        Level.Management,
        Level.Management | Level.Security | Level.Medical,
        Level.Management | Level.Security,
        Level.Management )]
    public class Officer : Security, IOfficer
    {
        public Officer() : base( Enums.Role.Officer )
        {
            Blocks = new List<Block>();
        }


        #region Fields/Properties

        public virtual ICollection<Block> Blocks { get; set; }

        #endregion


        
    }
}