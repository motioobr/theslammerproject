﻿namespace TheSlammer.Model.Security
{
    public class AccessLevel
    {
        public enum DangerLevel
        {
            Minimum,
            Low,
            Medium,
            High,
            Maximum
        };

        public enum SecurityLevel
        {
            Minimum,
            Low,
            Medium,
            High,
            Maximum
        };
    }
}