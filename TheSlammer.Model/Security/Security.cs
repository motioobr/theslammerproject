﻿using System;
using System.Data.Entity;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Security
{
    public abstract class Security : User
    {
        protected Security( Enums.Role role ) : base( role ) {}

        public virtual bool CreateInmate( Inmate.Inmate inmate )
        {
            try
            {
                ContextInstance.Data.Inmates.Add(inmate);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool UpdateInmate(Inmate.Inmate inmate)
        {
            try
            {
                ContextInstance.Data.Entry(inmate).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool DeleteInmate(IInmate inmate)
        {
            try
            {
                //ContextInstance.Data.Inmates.Remove((Inmate.Inmate)inmate);
                //ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        
    }
}