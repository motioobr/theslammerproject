﻿using System;
using System.Collections.Generic;
using TheSlammer.Model.Data;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;

namespace TheSlammer.Model.Security
{
    [Authorization(
        Level.Management,
        Level.Management | Level.Security | Level.Medical,
        Level.Management | Level.Security,
        Level.Management )]
    public class Guard : Security, IGuard
    {
        public Guard() : base( Enums.Role.Guard )
        {
            Cells = new List<Cell>();
        }


        #region Fields/Properties

        public virtual ICollection<Cell> Cells { get; set; }

        #endregion


        
    }
}