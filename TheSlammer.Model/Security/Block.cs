﻿using System.Collections.Generic;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;

namespace TheSlammer.Model.Security
{
    public class Block : Room, IBlock
    {
        public Block()
        {
            Cells = new List<Cell>();
        }

        

        public new virtual string ToString()
        {
            return Building.Name + " - " + RoomID;
        }

        #region Fields/Properties

        public string Name
        {
            get { return this.ToString(); }
        }

        #region Entity Framework Navigation Properties

        public int OfficerID { get; set; }
        public virtual Officer Officer { get; set; }
        public virtual ICollection<Cell> Cells { get; set; }

        #endregion


        #endregion
    }
}