﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Maintenance
{
    public class Administrator : User, IAdministrator
    {
        public Administrator() : base(Enums.Role.Administrator) { }

        #region Entity Framework Navigation Properties   
     
        public virtual ICollection<Work> Works { get; set; }
        public virtual ICollection<Maintenance> Maintenances { get; set; }

        #endregion

      

        public bool CreateWork(Work newWork)
        {
            try
            {
                ContextInstance.Data.Works.Add(newWork);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }  
        
        }


        public bool UpdateWork(Work update)
        {
            try
            {
                ContextInstance.Data.Entry( ContextInstance.Data.Works.First( w => w.WorkID == update.WorkID ) ).State =
                    System.Data.Entity.EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }          
            
        }

        public bool DeleteWork(Work delete)
        {
            try
            {
                ContextInstance.Data.Works.Remove(delete);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CreateMaintenance( Maintenance newMaintenance )
        {
            return new Warden().CreateMaintenance( newMaintenance );
        }

        public bool UpdateMaintenance( Maintenance update )
        {
            return new Warden().UpdateMaintenance( update );
        }

        public bool DeleteMaintenance(Maintenance delete)
        {
            return new Warden().DeleteMaintenance( delete );
        }
       
    }
}
