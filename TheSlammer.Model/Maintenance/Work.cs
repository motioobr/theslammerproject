﻿using System;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Maintenance
{
    [Authorization(
        Level.Management | Level.Maintenance,
        Level.Management | Level.Maintenance,
        Level.Management | Level.Maintenance,
        Level.Management | Level.Maintenance )]
    public class Work : IWork
    {
        #region Field/Properties

        public int WorkID { get; set; }
        public Enums.MaintenanceType Job { get; set; }
        public DateTime? WorkDate { get; set; }
        public bool IsCompleted { get; set; }
        public int? WorkerID { get; set; }
        public virtual Maintenance Worker { get; set; }
        public Enums.WorkType WorkType { get; set; }

        #endregion
    }
}