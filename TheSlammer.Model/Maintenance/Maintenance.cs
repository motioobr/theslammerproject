﻿using System;
using System.Collections.Generic;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Maintenance
{
    [Authorization(
        Level.Management,
        Level.Management | Level.Medical | Level.Maintenance,
        Level.Management | Level.Maintenance,
        Level.Management )]
    public class Maintenance : User, IMaintenance
    {
        public Maintenance() : base( Enums.Role.Maintenance )
        {
            Works = new List<Work>();
        }


        #region Fields/Properties

        public Enums.MaintenanceType Job { get; set; }


        #region Entity Framework Navigation Properties
        public virtual ICollection<Work> Works { get; set; }

        #endregion


        #endregion


        #region Methods

        /// <summary>Adds a new JobType to the list of jobs</summary>
        /// <param name="newJob">The new expertise</param>
        public void AddExperties( Enums.MaintenanceType newJob )
        {
            Job |= newJob;
        }

        public static void AddExperties(Enums.MaintenanceType newJob, IMaintenance maintenance)
        {
            maintenance.Job |= newJob;
        }

        /// <summary>Removes a JobType from the list of jobs</summary>
        /// <param name="job">The expertise to remove</param>
        public void RemoveExpertise( Enums.MaintenanceType job )
        {
            Job &= ~job;
        }

        public static void RemoveExpertise(Enums.MaintenanceType job, IMaintenance maintenance)
        {
            maintenance.Job &= ~job;
        }

        /// <summary>Checks if the current maintenance worker already has a given JobType</summary>
        /// <param name="job">The job to check for</param>
        /// <returns>True if the maintenance worker already has the job, otherwise, false</returns>
        public bool HasExpertise( Enums.MaintenanceType job )
        {
            return Job.HasFlag( job );
        }

        public static bool HasExpertise(Enums.MaintenanceType job, IMaintenance maintenance)
        {
            return maintenance.Job.HasFlag(job);
        }

        #endregion

        
    }
}