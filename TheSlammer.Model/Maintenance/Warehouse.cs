﻿using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;

namespace TheSlammer.Model.Maintenance
{
    public class Warehouse : Room, IWarehouse
    {
        #region Fields/Properties


        #region Entity Framework Navigation Properties

        public int ManagerID { get; set; }
        //public virtual Maintenance Manager { get; set; }

        #endregion


        #endregion
    }
}