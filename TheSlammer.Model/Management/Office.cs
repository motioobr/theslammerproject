﻿using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;

namespace TheSlammer.Model.Management
{
    public class Office : Room, IOffice {}
}