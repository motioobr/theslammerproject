﻿using System;
using System.Data.Entity;
using System.Globalization;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.People;
using TheSlammer.Model.Security;
using TheSlammer.Model.Visit;
using TheSlammer.Model.Medical;

namespace TheSlammer.Model.Management
{
    public abstract class Management : User, IManagement
    {
        protected Management( Enums.Role role ) : base( role ) {}


        #region Fields/Properties

        public int OfficeID { get; set; }
        public virtual Office Office { get; set; }

        #endregion

        
        #region Create various types


        #region People types

        public virtual bool CreateSecretary( Secretary secretary )
        {
            try
            {
                ContextInstance.Data.Secretaries.Add(secretary);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool CreateGuard( Guard guard )
        {
            try
            {
                ContextInstance.Data.Guards.Add(guard);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool CreateOfficer( Officer officer )
        {
            try
            {
                ContextInstance.Data.Officers.Add(officer);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool CreateInmate( Inmate.Inmate inmate)
        {
            try
            {
                ContextInstance.Data.Inmates.Add(inmate);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool CreateMedical(Medical.Medical medical )
        {
            try
            {
                ContextInstance.Data.Medical.Add(medical);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool CreateAdminisrator(Administrator administrator)
        {
            try
            {
                ContextInstance.Data.Administrators.Add(administrator);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool CreateMaintenance( Maintenance.Maintenance maintenance )
        {
            try
            {
                ContextInstance.Data.Maintenance.Add(maintenance);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool CreateVisitor( Visitor visitor )
        {
            try
            {
                ContextInstance.Data.Visitors.Add(visitor);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion


        #region Other types

        public virtual bool CreateWork()
        {
            throw new NotImplementedException();
        }

        public virtual bool CreateTreatment()
        {
            throw new NotImplementedException();
        }

        public virtual bool CreateVisit()
        {
            throw new NotImplementedException();
        }

        #endregion


        #endregion        


        #region Update various types

        #region People types

        public virtual bool UpdateSecretary(Secretary secretary)
        {
            try
            {
                ContextInstance.Data.Entry(secretary).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool UpdateOfficer(Officer officer)
        {
            try
            {
                ContextInstance.Data.Entry(officer).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        } 
        
        public virtual bool UpdateGuard(Guard guard)
        {
            try
            {
                ContextInstance.Data.Entry(guard).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool UpdateMedical(Medical.Medical medical)
        {
            try
            {
                ContextInstance.Data.Entry(medical).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool UpdateAdministrator(Administrator administrator)
        {
            try
            {
                ContextInstance.Data.Entry(administrator).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool UpdateMaintenance(Maintenance.Maintenance maintenance)
        {
            try
            {
                ContextInstance.Data.Entry(maintenance).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool UpdateInmate(Inmate.Inmate inmate)
        {

            try
            {
                ContextInstance.Data.Entry(inmate).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public virtual bool UpdateVisitor(Visitor visitor)
        {
            try
            {
                ContextInstance.Data.Entry(visitor).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #endregion


        #region Delete People types

        public virtual bool DeleteSecretaries(IManagement secretaries)
        {
            try
            {
                ContextInstance.Data.Secretaries.Remove( (Secretary) secretaries );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;     
            }
        }

        public virtual bool DeleteGuards(IGuard guard)
        {
            try
            {
                ContextInstance.Data.Guards.Remove( (Guard)guard );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public virtual bool DeleteOfficer( IOfficer officer )
        {
            try
            {
                ContextInstance.Data.Officers.Remove( (Officer)officer );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }            
        }

        public virtual bool DeleteMedical(IMedical medical)
        {
            try
            {
                ContextInstance.Data.Medical.Remove( (Medical.Medical)medical );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public virtual bool DeleteMaintenance(IMaintenance maintenance)
        {
            try
            {
                ContextInstance.Data.Maintenance.Remove( (Maintenance.Maintenance)maintenance );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public virtual bool DeleteInmate(IInmate inmate)
        {
            try
            {
                ContextInstance.Data.Inmates.Remove( (Inmate.Inmate)inmate );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public virtual bool DeleteVisitor( IVisitor visitor )
        {
            try
            {
                ContextInstance.Data.Visitors.Remove( (Visitor)visitor );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public virtual bool DeleteAdministrator( IAdministrator  administrator)
        {
            try
            {
                ContextInstance.Data.Administrators.Remove((Administrator)administrator);
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion




        
    }
}