﻿using System;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;

namespace TheSlammer.Model.Management
{
    [Authorization(
        Level.Management,
        Level.Medical | Level.Management,
        Level.Management,
        Level.Management )]
    public class Secretary : Management, ISecretary
    {
        public Secretary() : base( Enums.Role.Secretary ) {}

        
    }
}