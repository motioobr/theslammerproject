﻿using System;
using System.Data.Entity;
using TheSlammer.Model.Data;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Security;

namespace TheSlammer.Model.Management
{
    [Authorization(
        Level.Warden,
        Level.Management | Level.Medical,
        Level.Management,
        Level.Warden )]
    public class Warden : Management, IWarden
    {
        public Warden() : base( Enums.Role.Warden ) {}

        public virtual bool CreateWarden( IWarden warden )
        {
            try
            {
                //ContextInstance.Data.Entry( (Warden)warden ).State = EntityState.Modified;
                ContextInstance.Data.Wardens.Add( (Warden)warden );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public virtual bool UpdateWarden(IWarden warden)
        {
            try
            {
                ContextInstance.Data.Entry( (Warden)warden ).State = EntityState.Modified;
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public virtual bool DeleteWarden( IWarden warden )
        {
            try
            {
                ContextInstance.Data.Wardens.Remove( (Warden)warden );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        #region Create Buildings 

        public bool CreateBlock( IBlock block )
        {
            try
            {
                ContextInstance.Data.Blocks.Add( (Block)block );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public bool CreateBuilding( IBuilding building )
        {
            try
            {
                ContextInstance.Data.Buildings.Add( (Building)building );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public bool CreateCell( ICell cell )
        {
            try
            {
                ContextInstance.Data.Cells.Add( (Cell)cell );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public bool CreateOffice( IOffice office )
        {
            try
            {
                ContextInstance.Data.Offices.Add( (Office)office );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        public bool CreateWarehouse( IWarehouse warehouse )
        {
            try
            {
                ContextInstance.Data.Warehouses.Add( (Warehouse)warehouse );
                ContextInstance.Data.SaveChanges();
                return true;
            }
            catch ( Exception )
            {
                return false;
            }
        }

        #endregion


    }
}