﻿using System;
using System.Diagnostics;
using System.Reflection;
using TheSlammer.Model.People;

namespace TheSlammer.Model
{
    /// <summary>
    ///     List of the various levels of authorization
    ///     Directly related to Person.Role's values
    /// </summary>
    [Flags]
    public enum Level : byte
    {
        None = 0,

        Warden = 1,
        Secretary = 2,

        /// <summary>Auxiliary level that includes Warden and Secretary</summary>
        Management = Warden | Secretary,

        Officer = 4,
        Guard = 8,

        /// <summary>Auxiliary level that includes Officer and Guard</summary>
        Security = Officer | Guard,

        Medical = 16,
        Maintenance = 32,

        /// <summary>Auxiliary level that includes all user roles</summary>
        Users = Management | Security | Medical | Maintenance,

        Inmate = 64,
        Visitor = 128,

        /// <summary>Auxiliary level that includes Inmates & Visitors</summary>
        NonUser = Inmate | Visitor,

        /// <summary>Auxiliary level that includes Roles/Levels</summary>
        All = 255
    }

    [AttributeUsage( AttributeTargets.Class )]
    public class Authorization : Attribute
    {
        private readonly Level _create;
        private readonly Level _delete;
        private readonly Level _read;
        private readonly Level _update;

        public Authorization( Level create, Level read, Level update, Level delete )
        {
            _create = create;
            _read = read;
            _update = update;
            _delete = delete;
        }

        public Authorization( Level all )
        {
            _create = _read = _update = _delete = all;
        }

        /// <summary>Get the CRUD Authorizing attributes from a class</summary>
        /// <typeparam name="TClass">The class who's attributes are needed</typeparam>
        /// <returns>An authorization object</returns>
        private static Authorization GetAttributes<TClass>() where TClass : class
        {
            return typeof( TClass ).GetCustomAttribute<Authorization>();
        }

        private static bool Can( Enums.Role userLevel, Level crudType )
        {
            try
            {
                return ( userLevel.HasFlag( crudType ) );
            }
            catch ( Exception e )
            {
                Debug.WriteLine( "Error " + e );
                return false;
            }
        }

        public static bool CanCreate<TClass>( Enums.Role userLevel ) where TClass : class
        {
            return Can( userLevel, GetAttributes<TClass>()._create );
        }

        public static bool CanCreate<TClass>() where TClass : class
        {
            throw new NotImplementedException();
            //return Can( LoggedUser.Instance.PersonRole, GetAttributes<TClass>()._create );
        }

        public static bool CanRead<TClass>( Enums.Role userLevel ) where TClass : class
        {
            return Can( userLevel, GetAttributes<TClass>()._read );
        }

        public static bool CanRead<TClass>() where TClass : class
        {
            throw new NotImplementedException();
            //return Can( LoggedUser.Instance.PersonRole, GetAttributes<TClass>()._read );
        }

        public static bool CanUpdate<TClass>( Enums.Role userLevel ) where TClass : class
        {
            return Can( userLevel, GetAttributes<TClass>()._update );
        }

        public static bool CanUpdate<TClass>() where TClass : class
        {
            throw new NotImplementedException();
            //return Can( LoggedUser.Instance.PersonRole, GetAttributes<TClass>()._update );
        }

        public static bool CanDelete<TClass>( Enums.Role userLevel ) where TClass : class
        {
            return Can( userLevel, GetAttributes<TClass>()._delete );
        }

        public static bool CanDelete<TClass>() where TClass : class
        {
            throw new NotImplementedException();
            //return Can( LoggedUser.Instance.PersonRole, GetAttributes<TClass>()._delete );
        }
    }
}