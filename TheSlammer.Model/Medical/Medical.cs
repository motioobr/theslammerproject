﻿using System;
using System.Collections.Generic;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Model.People;

namespace TheSlammer.Model.Medical
{
    [Authorization(
        Level.Management,
        Level.Management | Level.Medical,
        Level.Management | Level.Medical,
        Level.Management )]
    public class Medical : User, IMedical
    {
        public Medical() : base( Enums.Role.Medical )
        {
            Treatments = new List<Treatment>();
        }

        


        #region Fields/Properties

        public Enums.MedicalExperties Expertises { get; set; }
        public virtual ICollection<Treatment> Treatments { get; set; }

        #endregion


        #region Methods

        /// <summary>Adds a new expertise to the list of expertise</summary>
        /// <param name="newExpertise">The new expertise</param>
        public void AddExperties( Enums.MedicalExperties newExpertise )
        {
            Expertises |= newExpertise;
        }

        public static void AddExperties(Enums.MedicalExperties newExpertise , IMedical medical)
        {
            medical.Expertises |= newExpertise;
        }

        /// <summary>Removes an expertise from the list of expertise</summary>
        /// <param name="expertise">The expertise to remove</param>
        public void RemoveExpertise( Enums.MedicalExperties expertise )
        {
            Expertises &= ~expertise;
        }

        public static void RemoveExpertise(Enums.MedicalExperties expertise, IMedical medical)
        {
            medical.Expertises &= ~expertise;
        }

        /// <summary>Checks if the current staff member already has a given expertise</summary>
        /// <param name="expertise">The expertise to check for</param>
        /// <returns>True if the staff member already has the expertise, otherwise, false</returns>
        public bool HasExpertise( Enums.MedicalExperties expertise )
        {
            return Expertises.HasFlag( expertise );
        }

        public static bool HasExpertise(Enums.MedicalExperties expertise, IMedical medical)
        {
            return medical.Expertises.HasFlag(expertise);
        }
        #endregion
    }
}