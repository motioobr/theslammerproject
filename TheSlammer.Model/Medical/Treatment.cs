﻿using System;
using TheSlammer.Model.Interfaces;

namespace TheSlammer.Model.Medical
{
    [Authorization(
        Level.Management | Level.Secretary | Level.Medical,
        Level.Management | Level.Medical,
        Level.Management | Level.Medical,
        Level.Management | Level.Medical )]
    public class Treatment : ITreatment
    {
        #region Fields/Properties

        public int TreatmentID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool InTreatment { get; set; }
        public Enums.TreatmentType Type { get; set; }
        public int PatientID { get; set; }
        public virtual Inmate.Inmate Patient { get; set; }
        public int DoctorID { get; set; }
        public virtual Medical Doctor { get; set; }
        
        #endregion
    }
}