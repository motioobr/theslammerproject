﻿using System.Drawing;

namespace TheSlammer.Assets.Styles
{
    public static class Palette
    {
        public enum Type
        {
            Blue,
            Turquoise,
            Yellow,
            Red,
            Green,
            Orange,
            Purple,
            Grey,
            Black,
            White
        }

        public static Color ColorFromType( Type type, bool hoverColor = false )
        {
            switch ( type )
            {
                case Type.Blue :
                    return hoverColor ? BlueDark : BlueLight;

                case Type.Turquoise :
                    return hoverColor ? TurquoiseLight : TurquoiseDark;

                case Type.Yellow :
                    return hoverColor ? YellowDark : YellowLight;

                case Type.Red :
                    return hoverColor ? RedDark : RedLight;

                case Type.Green :
                    return hoverColor ? GreenDark : GreenLight;

                case Type.Purple :
                    return hoverColor ? PurpleDark : PurpleLight;

                case Type.Orange :
                    return hoverColor ? OrangeLight : OrangeDark;

                case Type.Grey :
                    return hoverColor ? GreyDark : GreyLight;

                case Type.Black :
                    return hoverColor ? BlackLight : BlackDark;

                case Type.White :
                    return hoverColor ? WhiteDark : WhiteLight;

                default :
                    return Color.White;
            }
        }


        #region Color Palette

        public static readonly Color GreyLight = Color.FromArgb( 188, 188, 188 );
        public static readonly Color GreyDark = Color.FromArgb( 103, 103, 103 );
        public static readonly Color WhiteLight = Color.FromArgb( 241, 242, 247 );
        public static readonly Color WhiteDark = Color.FromArgb( 225, 225, 235 );
        public static readonly Color White = Color.FromArgb( 255, 255, 255 );
        public static readonly Color TurquoiseLight = Color.FromArgb( 37, 181, 172 );
        public static readonly Color TurquoiseDark = Color.FromArgb( 12, 151, 142 );
        public static readonly Color BlueLight = Color.FromArgb( 40, 183, 226 );
        public static readonly Color BlueDark = Color.FromArgb( 14, 153, 196 );
        public static readonly Color YellowLight = Color.FromArgb( 252, 180, 101 );
        public static readonly Color YellowDark = Color.FromArgb( 232, 160, 82 );
        public static readonly Color GreenLight = Color.FromArgb( 104, 189, 124 );
        public static readonly Color GreenDark = Color.FromArgb( 74, 159, 95 );
        public static readonly Color RedLight = Color.FromArgb( 239, 82, 83 );
        public static readonly Color RedDark = Color.FromArgb( 209, 52, 54 );
        public static readonly Color BlackDark = Color.FromArgb( 50, 50, 58 );
        public static readonly Color BlackLight = Color.FromArgb( 70, 70, 78 );
        public static readonly Color PurpleLight = Color.FromArgb( 153, 114, 181 );
        public static readonly Color PurpleDark = Color.FromArgb( 123, 74, 141 );
        public static readonly Color OrangeLight = Color.FromArgb( 250, 133, 100 );
        public static readonly Color OrangeDark = Color.FromArgb( 215, 113, 80 );

        #endregion
    }
}