﻿using System.Drawing;
using System.Windows.Forms;

namespace TheSlammer.Assets.Styles
{
    public static class Utility
    {
        public static void DrawBorder( Border side, int thickness, Color color, Control control, PaintEventArgs e )
        {
            Point p1;
            Point p2;

            switch ( side )
            {
                case Border.Top :
                    p1 = new Point( 0, 0 );
                    p2 = new Point( control.Size.Width, 0 );
                    break;

                case Border.Right :
                    p1 = new Point( control.Size.Width, 0 );
                    p2 = new Point( control.Size.Width, control.Size.Height );
                    break;

                case Border.Bottom :
                    p1 = new Point( 0, control.Size.Height );
                    p2 = new Point( control.Size.Width, control.Size.Height );
                    break;

                case Border.Left :
                    p1 = new Point( 0, 0 );
                    p2 = new Point( 0, control.Size.Height );
                    break;

                default :
                    return;
            }

            e.Graphics.DrawLine( new Pen( color, thickness ), p1, p2 );
        }

        public static void CenterControl( Control control, Control parent )
        {
            HorizontalCenterControl( control, parent );
            VerticalCenterControl( control, parent );
        }

        public static void HorizontalCenterControl( Control control, Control parent )
        {
            control.Location = new Point(
                ( parent.Width - control.Width ) / 2,
                control.Location.Y
                );
        }

        public static void VerticalCenterControl( Control control, Control parent )
        {
            control.Location = new Point(
                control.Location.X,
                ( parent.Height - control.Height ) / 2
                );
        }


        #region Enums

        public enum Size
        {
            Default,
            Medium,
            Large,
            Block
        }

        public enum Border
        {
            Top,
            Right,
            Bottom,
            Left
        }

        #endregion
    }
}