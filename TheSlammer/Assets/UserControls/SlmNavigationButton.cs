﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Views;

namespace TheSlammer.Assets.UserControls
{
    internal class SlmNavigationButton : UserControl
    {
        internal SlmNavigationButton()
        {
            Size = new Size( 300, 45 );
            Padding = new Padding( 0 );
            Margin = new Padding( 0 );


            _subMenu = new Panel()
            {
                Visible = false,
                Location =  new Point(0,0),
                AutoSize = true,
                Parent = ParentForm
            };     
           
            _navMenuButton = new SlmButton
            {
                SlmButtonSize = SlmButton.ButtonSize.Navigation,
                BackgroundColor = Palette.Type.Black,
                Dock = DockStyle.Top,
                Parent = this
            };
           
            _navMenuButton.MouseEnter += ShowSubmenu;
        }
       
        private void ShowSubmenu( object sender, EventArgs e )
        {
            _subMenu.Visible = true;
        }

        private static void CreateSubMenu( Dictionary<string, SlmButton> buttons, string title, MainDashboard parent )
        {
            if ( parent == null )
                return;

            Panel subMenu = new Panel
            {
                BackColor = Palette.BlackLight,
                AutoSize = true,
                Padding = new Padding( 0 ),
 
              Parent = parent.ContentPanel
            };

            

            Utility.CenterControl( subMenu, parent );
        }

        public void AddSubmenuButton(string text, CustomFonts.Icon icon, EventHandler onclick)
        {
            SlmButton submenuButton = new SlmButton()
            {
                Dock = DockStyle.Fill,
                Text = text,
                SlmButtonSize = SlmButton.ButtonSize.ExtraLarge,
                BackgroundColor = Palette.Type.Black,
                IconType = icon
            };
            submenuButton.Click += onclick;
            _subMenu.Controls.Add(submenuButton);
        }

        #region Fields/Properties

        private readonly SlmButton _navMenuButton;
        private Panel _subMenu;
        private SlmButton[] buttons = new SlmButton[10];
        public CustomFonts.Icon IconType
        {
            get { return _navMenuButton.IconType; }
            set { _navMenuButton.IconType = value; }
        }

        public new virtual string Text { get { return _navMenuButton.Text; } set { _navMenuButton.Text = value; } }
        

        #endregion
    }
}

/*
 button.Value.Dock = DockStyle.Fill;
                button.Value.Text = button.Key;
                button.Value.SlmButtonSize = SlmButton.ButtonSize.ExtraLarge;
                button.Value.BackgroundColor = Palette.Type.Black;
 
 */