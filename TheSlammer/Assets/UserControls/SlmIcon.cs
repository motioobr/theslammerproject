﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    public class SlmIcon : PictureBox
    {
        public SlmIcon() : this( CustomFonts.Icon.Success, 16 ) {}

        public SlmIcon( CustomFonts.Icon type, int size )
        {
            IconFont = null;
            BackColor = Color.Transparent;

            Width = size;
            Height = size;

            IconType = type;

            InActiveColor = Palette.Type.Blue;
            ActiveColor = Palette.Type.Green;

            MouseEnter += IconMouseEnter;
            MouseLeave += IconMouseLeave;
        }


        #region Public


        #region Public Properties

        public CustomFonts.Icon IconType
        {
            get { return _iconType; }
            set
            {
                _iconType = value;
                Invalidate();
            }
        }

        public Palette.Type ActiveColor
        {
            get { return _activeColorType; }
            set
            {
                _activeColorType = value;
                _activeColor = Palette.ColorFromType( value, true );
                ActiveBrush = new SolidBrush( _activeColor );
                Invalidate();
            }
        }

        public Palette.Type InActiveColor
        {
            get { return _inActiveColorType; }
            set
            {
                _inActiveColorType = value;
                _inActiveColor = Palette.ColorFromType( value );
                InActiveBrush = new SolidBrush( _inActiveColor );
                IconBrush = InActiveBrush;
                Invalidate();
            }
        }

        public new int Width
        {
            set
            {
                base.Width = value;
                IconFont = null;
                Invalidate();
            }
            get { return base.Width; }
        }

        public new int Height
        {
            set
            {
                base.Height = value;
                IconFont = null;
                Invalidate();
            }
            get { return base.Height; }
        }

        #endregion


        #endregion


        #region Private


        #region Properties & Attributes

        private CustomFonts.Icon _iconType = CustomFonts.Icon.Success;
        private Color _activeColor = Color.Black;
        private Color _inActiveColor = Color.Black;
        private Palette.Type _activeColorType;
        private Palette.Type _inActiveColorType;
        private Font IconFont { get; set; }
        private Brush IconBrush { get; set; } // brush currently in use
        private Brush ActiveBrush { get; set; } // brush to use when hovered over
        private Brush InActiveBrush { get; set; } // brush to use when not hovered over

        #endregion


        #region Event Handlers

        public void RaiseMouseEnter()
        {
            IconMouseEnter( null, EventArgs.Empty );
        }

        public void RaiseMouseLeave()
        {
            IconMouseLeave( null, EventArgs.Empty );
        }

        protected void IconMouseLeave( object sender, EventArgs e )
        {
            IconBrush = InActiveBrush;
            Invalidate();
        }

        protected void IconMouseEnter( object sender, EventArgs e )
        {
            IconBrush = ActiveBrush;
            Invalidate();
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            var graphics = e.Graphics;

            graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphics.SmoothingMode = SmoothingMode.HighQuality;

            if ( IconFont == null ) SetFontSize( graphics );

            // Measure string so that we can center the icon.
            SizeF stringSize = graphics.MeasureString( CustomFonts.GetIcon( IconType ), IconFont, Width );
            float w = stringSize.Width;
            float h = stringSize.Height;

            // center icon
            float left = Math.Abs( w - Width );
            float top = Math.Abs( h - Height );

            // Pull the left starting point
            left = left == 0
                       ? (float)( -Width * .08 )
                       : (float)( -left * .08 );


            // Draw string to screen.
            graphics.DrawString( CustomFonts.GetIcon( IconType ), IconFont, IconBrush, new PointF( left, top ) );

            base.OnPaint( e );
        }

        #endregion


        #region Methods

        private void SetFontSize( Graphics g )
        {
            IconFont = GetAdjustedFont( g, CustomFonts.GetIcon( IconType ), Width, Height, 4, true );
        }

        private static Font GetIconFont( float size )
        {
            return DEVELOPMENT.israfi ? new Font("David", 14, FontStyle.Regular) : new Font(CustomFonts.FlatIcon, size, GraphicsUnit.Point);
        }

        private static Font GetAdjustedFont(
            Graphics g,
            string graphicString,
            int containerWidth,
            int maxFontSize,
            int minFontSize,
            bool smallestOnFail )
        {
            for ( double adjustedSize = maxFontSize; adjustedSize >= minFontSize; adjustedSize -= 0.5 )
            {
                Font testFont = GetIconFont( (float)adjustedSize );

                SizeF adjustedSizeNew = g.MeasureString( graphicString, testFont );

                if ( containerWidth + ( containerWidth * .16 ) > Convert.ToInt32( adjustedSizeNew.Width ) ) return testFont;
            }

            return GetIconFont( smallestOnFail ? minFontSize : maxFontSize );
        }

        #endregion


        #endregion
    }
}