﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    public class SlmButton : UserControl
    {
        public SlmButton()
        {
            InitializeDefaults();
            InitializeComponents();
        }

        private void InitializeDefaults()
        {
            _size = ButtonSize.Medium;
            _textColor = Palette.Type.White;
            _textHoverColor = Palette.Type.White;
            _backgroundColor = Palette.Type.Turquoise;
            _backgroundHoverColor = Palette.Type.Turquoise;

            ForeColor = Palette.ColorFromType( _textColor );
            BackColor = Palette.ColorFromType( _backgroundColor );
            Font = CustomFonts.GetFont( CustomFonts.Size.Small );
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            Margin = new Padding( 5 );
            Padding = new Padding( 8, 0, 8, 0 );
            base.Text = "SlmButton";
        }

        private void InitializeComponents()
        {
            _icon = new SlmIcon
            {
                Location = new Point( 5, 5 ),
                ActiveColor = TextColor,
                InActiveColor = TextHoverColor,
                TabStop = false,
                Parent = this
            };

            _text = new Label
            {
                Dock = DockStyle.Fill,
                Font = CustomFonts.GetFont( CustomFonts.Size.Small ),
                TextAlign = ContentAlignment.MiddleLeft,
                AutoSize = true,
                Text = base.Text,
                TabStop = false,
                Parent = this
            };

            MouseEnter += ActiveHover;
            _text.MouseEnter += ActiveHover;
            _icon.MouseEnter += ActiveHover;

            MouseLeave += DeactivateHover;
            _text.MouseLeave += DeactivateHover;
            _icon.MouseLeave += DeactivateHover;

            _text.Click += Clicked;
            _icon.Click += Clicked;
        }

        private void SetColor()
        {
            BackColor = _text.BackColor = _icon.BackColor = Palette.ColorFromType( _backgroundColor );
            ForeColor = _text.ForeColor = _icon.ForeColor = Palette.ColorFromType( _textColor );
            _icon.ActiveColor = TextColor;
        }


        #region Enums

        public enum ButtonSize
        {
            Small,
            Medium,
            Large,
            ExtraLarge,
            Navigation
        }

        public enum IconLocation
        {
            Left,
            Right,
            None
        }

        #endregion


        #region Events

        private void Clicked( object sender, EventArgs e )
        {
            OnClick( e );
        }

        private void DeactivateHover( object sender, EventArgs e )
        {
            SetColor();
        }

        private void ActiveHover( object sender, EventArgs e )
        {
            BackColor = _text.BackColor = _icon.BackColor = Palette.ColorFromType( BackgroundHoverColor, true );
            ForeColor = _text.ForeColor = _icon.ForeColor = Palette.ColorFromType( TextHoverColor, true );
            _icon.ActiveColor = TextHoverColor;
        }


        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );

            switch ( _size )
            {
                case ButtonSize.Small :
                    Height = 22;
                    _text.Font = CustomFonts.GetFont( CustomFonts.Size.Default );
                    break;

                case ButtonSize.Medium :
                    Height = 26;
                    _text.Font = CustomFonts.GetFont( CustomFonts.Size.Small );
                    break;

                case ButtonSize.Large :
                    Height = 33;
                    _text.Font = CustomFonts.GetFont( CustomFonts.Size.Medium );
                    break;

                case ButtonSize.ExtraLarge :
                    Height = 36;
                    _text.Font = CustomFonts.GetFont( CustomFonts.Size.Large );
                    break;

                case ButtonSize.Navigation :
                    Size = new Size( 300, 45 );
                    _text.Font = CustomFonts.GetFont( CustomFonts.Size.Medium );
                    break;
            }

            _icon.Width = _icon.Height = (int)( Height - ( Height * 0.2 ) );
            _text.MinimumSize = new Size( 0, Height );

            switch ( IconPosition )
            {
                case IconLocation.Right :
                    _text.Padding = new Padding( 0, 0, _icon.Bounds.Left, 0 );
                    _icon.Visible = true;
                    _icon.Dock = DockStyle.Right;
                    break;

                case IconLocation.Left :
                    _text.Padding = new Padding( _icon.Bounds.Right, 0, 0, 0 );
                    _icon.Visible = true;
                    _icon.Dock = DockStyle.Left;
                    break;

                case IconLocation.None :
                    _icon.Visible = false;
                    _text.Padding = new Padding( 0 );
                    break;
            }
        }

        #endregion


        #region Fields/ Properties

        private ButtonSize _size;
        private SlmIcon _icon;
        private Label _text;
        private Palette.Type _backgroundColor;
        private Palette.Type _backgroundHoverColor;
        private Palette.Type _textColor;
        private Palette.Type _textHoverColor;

        public IconLocation IconPosition { get; set; }

        public Palette.Type BackgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                _backgroundColor = value;
                SetColor();
            }
        }

        public Palette.Type BackgroundHoverColor
        {
            get { return _backgroundHoverColor; }
            set
            {
                _backgroundHoverColor = value;
                SetColor();
            }
        }

        public Palette.Type TextColor
        {
            get { return _textColor; }
            set
            {
                _textColor = value;
                SetColor();
            }
        }

        public Palette.Type TextHoverColor
        {
            get { return _textHoverColor; }
            set
            {
                _textHoverColor = value;
                SetColor();
            }
        }

        public CustomFonts.Icon IconType
        {
            get { return _icon.IconType; }
            set
            {
                _icon.IconType = value;
                Invalidate();
            }
        }

        public new virtual string Text { get { return base.Text; } set { base.Text = _text.Text = value.ToUpper(); } }

        public ButtonSize SlmButtonSize
        {
            get { return _size; }
            set
            {
                _size = value;
                OnResize( EventArgs.Empty );
            }
        }


        #region Disable Properties from editor

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual RightToLeft RightToLeft
        {
            get { return base.RightToLeft; }
            set { base.RightToLeft = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual string Tag { set { base.Tag = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual BorderStyle BorderStyle
        {
            get { return base.BorderStyle; }
            set { base.BorderStyle = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual AnchorStyles Anchor { get { return base.Anchor; } set { base.Anchor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual ImeMode ImeMode { get { return base.ImeMode; } set { base.ImeMode = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Padding Margin { get { return base.Margin; } set { base.Margin = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Padding Padding { get { return base.Padding; } set { base.Padding = value; } }


        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Size Size { get { return base.Size; } set { base.Size = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Color ForeColor { get { return base.ForeColor; } set { base.ForeColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Color BackColor { get { return base.BackColor; } set { base.BackColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new DockStyle Dock { get { return base.Dock; } set { base.Dock = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Font Font { get { return base.Font; } set { base.Font = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new AutoSizeMode AutoSizeMode { get { return base.AutoSizeMode; } set { base.AutoSizeMode = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new bool AutoSize { get { return base.AutoSize; } set { base.AutoSize = value; } }

        #endregion


        #endregion
    }
}