﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    public class SlmComboBox : UserControl
    {
        public SlmComboBox()
        {
            Initialize();
        }

        private void Initialize()
        {
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;

            _comboBox = new ComboBox
            {
                BackColor = Palette.White,
                FlatStyle = FlatStyle.Flat,
                ForeColor = Palette.GreyDark,
                DropDownStyle = ComboBoxStyle.DropDownList,
                Sorted = true,
                Dock = DockStyle.Fill,
                Font = CustomFonts.GetFont( CustomFonts.Size.Large )
            };
            _comboBox.SelectedValueChanged +=
                delegate { if ( SelectedValueChanged != null ) SelectedValueChanged( new object(), EventArgs.Empty ); };

            _description = new Label
            {
                Text = "Description help text",
                Font = CustomFonts.GetFont( CustomFonts.Size.Small ),
                ForeColor = Palette.GreyLight,
                Size = new Size( 250, 20 ),
                TextAlign = ContentAlignment.TopLeft,
                Location = new Point( 0, 45 ),
                TabStop = false,
                Parent = this
            };

            _icon = new SlmIcon
            {
                Dock = DockStyle.Left,
                IconType = CustomFonts.Icon.Success,
                Size = new Size( 28, 50 ),
                TabStop = false,
                Margin = new Padding( 0 ),
                Visible = false
            };

            Panel textPanel = new Panel
            {
                BackColor = Palette.White,
                Margin = new Padding( 0 ),
                Padding = new Padding( 7, 3, 3, 3 ),
                AutoSize = true,
                Size = new Size( 250, 43 ),
                TabStop = false,
                Controls = {_icon, _comboBox},
                Parent = this
            };

            _comboBox.Click += delegate { ClearError(); };
        }


        #region Set/Unset Errors

        public void ClearError()
        {
            _icon.Visible = false;
            _description.Text = _descriptionMessage;
            _description.ForeColor = Palette.GreyLight;
            _description.Visible = !string.IsNullOrWhiteSpace( _descriptionMessage );
        }

        public void SetError( string errorMessage )
        {
            _icon.Visible = true;
            _icon.ActiveColor = Palette.Type.Red;
            _icon.InActiveColor = Palette.Type.Red;
            _icon.IconType = CustomFonts.Icon.Danger;

            _description.Text = errorMessage;
            _description.Visible = true;
            _description.ForeColor = Palette.RedLight;
        }

        public void SetSuccess()
        {
            ClearError();

            _icon.Visible = true;
            _icon.ActiveColor = Palette.Type.Green;
            _icon.InActiveColor = Palette.Type.Green;
            _icon.IconType = CustomFonts.Icon.Success;
        }

        #endregion


        #region Fields/Properties

        private ComboBox _comboBox;
        private Label _description;
        private SlmIcon _icon;
        private string _descriptionMessage;

        public string Description
        {
            get { return _descriptionMessage; }
            set
            {
                _descriptionMessage = _description.Text = value;
                _description.Visible = !string.IsNullOrWhiteSpace( _descriptionMessage );
            }
        }


        #region Combo Box Neccesary properties

        public string DisplayMember { get { return _comboBox.DisplayMember; } set { _comboBox.DisplayMember = value; } }

        public string ValueMember { get { return _comboBox.ValueMember; } set { _comboBox.ValueMember = value; } }

        public Object SelectedItem { get { return _comboBox.SelectedItem; } set { _comboBox.SelectedItem = value; } }

        public Object SelectedValue { get { return _comboBox.SelectedValue; } set { _comboBox.SelectedValue = value; } }

        public Object DataSource { get { return _comboBox.DataSource; } set { _comboBox.DataSource = value; } }

        public event EventHandler SelectedValueChanged;

        #endregion


        #region Disable Properties from editor

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual string Text { get { return base.Text; } set { base.Text = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Size Size { get { return base.Size; } set { base.Size = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Color ForeColor { get { return base.ForeColor; } set { base.ForeColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Color BackColor { get { return base.BackColor; } set { base.BackColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual DockStyle Dock { get { return base.Dock; } set { base.Dock = value; } }


        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Font Font { get { return base.Font; } set { base.Font = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual AutoSizeMode AutoSizeMode
        {
            get { return base.AutoSizeMode; }
            set { base.AutoSizeMode = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual bool AutoSize { get { return base.AutoSize; } set { base.AutoSize = value; } }

        #endregion


        #endregion
    }
}