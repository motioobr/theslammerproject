﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    public class SlmTextBox : TextBox
    {
        #region Constructors

        public SlmTextBox()
        {
            Initialize();
        }

        #endregion


        #region Fields


        #region Protected Fields

        protected string WaterMarkText = "SlmTextBox";
        protected Color WaterMarkColor;
        protected Color WaterMarkActiveColor;

        #endregion


        #region Private Fields

        private Panel _waterMarkContainer;
        private Font _waterMarkFont;
        private SolidBrush _waterMarkBrush;

        #endregion


        #endregion


        #region Private Methods

        /// <summary>Initializes watermark properties and adds SlmTextBox events</summary>
        private void Initialize()
        {
            // Sets some default values to the watermark properties
            WaterMarkColor = Color.LightGray;
            WaterMarkActiveColor = Color.Gray;
            _waterMarkFont = Font;
            _waterMarkBrush = new SolidBrush( WaterMarkActiveColor );
            _waterMarkContainer = null;

            // Draw the watermark, so we can see it in design time
            DrawWaterMark();

            // EventHandlers which contains function calls. 
            // Either to draw or to remove the watermark
            Enter += ThisHasFocus;
            Leave += ThisWasLeaved;
            TextChanged += ThisTextChanged;
        }

        /// <summary>Removes the watermark if it should</summary>
        private void RemoveWaterMark()
        {
            if ( _waterMarkContainer == null ) return;

            Controls.Remove( _waterMarkContainer );
            _waterMarkContainer = null;
        }

        /// <summary>Draws the watermark if the text length is 0</summary>
        private void DrawWaterMark()
        {
            Text = Text.Trim();

            if ( _waterMarkContainer != null || TextLength > 0 ) return;

            _waterMarkContainer = new Panel();
            _waterMarkContainer.Paint += waterMarkContainer_Paint;
            _waterMarkContainer.Invalidate();
            _waterMarkContainer.Click += waterMarkContainer_Click;
            Controls.Add( _waterMarkContainer );
        }

        #endregion


        #region Eventhandlers


        #region WaterMark Events

        private void waterMarkContainer_Click( object sender, EventArgs e )
        {
            Focus();
        }

        private void waterMarkContainer_Paint( object sender, PaintEventArgs e )
        {
            _waterMarkContainer.Location = new Point( 2, 0 );
            _waterMarkContainer.Height = Height;
            _waterMarkContainer.Width = Width;
            _waterMarkContainer.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;

            _waterMarkBrush = ContainsFocus ? new SolidBrush( WaterMarkActiveColor ) : new SolidBrush( WaterMarkColor );

            e.Graphics.DrawString(
                                  WaterMarkText,
                                  _waterMarkFont,
                                  _waterMarkBrush,
                                  new PointF( -2f, 1f )
                );
        }

        #endregion


        #region SlmTextBox Events

        private void ThisHasFocus( object sender, EventArgs e )
        {
            //if focused use focus color
            _waterMarkBrush = new SolidBrush( WaterMarkActiveColor );

            //The watermark should not be drawn if the user has already written some text
            if ( TextLength > 0 ) return;

            RemoveWaterMark();
            DrawWaterMark();
        }

        private void ThisWasLeaved( object sender, EventArgs e )
        {
            Text = Text.Trim();

            if ( TextLength > 0 ) RemoveWaterMark();
            else Invalidate();
        }

        private void ThisTextChanged( object sender, EventArgs e )
        {
            //If the text of the textbox is not empty
            if ( TextLength > 0 ) RemoveWaterMark();
            else DrawWaterMark();
        }


        #region Overrided Events

        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );
            //Draw the watermark even in design time
            DrawWaterMark();
        }

        protected override void OnInvalidated( InvalidateEventArgs e )
        {
            base.OnInvalidated( e );
            //Check if there is a watermark
            if ( _waterMarkContainer != null )
                //if there is a watermark it should also be invalidated();
                _waterMarkContainer.Invalidate();
        }

        #endregion


        #endregion


        #endregion


        #region Properties

        [Category( "Watermark attributes" )]
        [Description( "Sets the text of the watermark" )]
        public string WaterMark
        {
            get { return WaterMarkText; }
            set
            {
                WaterMarkText = value;
                Invalidate();
            }
        }

        [Category( "Watermark attributes" )]
        [Description( "When the control gains focus, this color will be used as the watermark's fore-color" )]
        public Color WaterMarkActiveForeColor
        {
            get { return WaterMarkActiveColor; }

            set
            {
                WaterMarkActiveColor = value;
                Invalidate();
            }
        }

        [Category( "Watermark attributes" )]
        [Description( "When the control looses focus, this color will be used as the watermark's fore-color" )]
        public Color WaterMarkForeColor
        {
            get { return WaterMarkColor; }

            set
            {
                WaterMarkColor = value;
                Invalidate();
            }
        }

        [Category( "Watermark attributes" )]
        [Description( "The font used on the watermark. Default is the same as the control" )]
        public Font WaterMarkFont
        {
            get { return _waterMarkFont; }

            set
            {
                _waterMarkFont = value;
                Invalidate();
            }
        }

        #endregion
    }
}