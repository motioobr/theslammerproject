﻿using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Model.Interfaces;

namespace TheSlammer.Assets.UserControls
{
    [System.ComponentModel.DesignerCategory("")]
    abstract public class SlmPage : UserControl
    {
        protected SlmPage()
        {
            Initialize();
        }

        private void Initialize()
        {
            BackColor       = Palette.WhiteLight;
            ForeColor       = Palette.GreyDark;
            Font            = CustomFonts.GetFont( CustomFonts.Size.Default );
            Dock            = DockStyle.Fill;
            _borderSize     = 10;

            _title = new Label
            {
                Text      = "Page Title",
                BackColor = Palette.TurquoiseLight,
                ForeColor = Palette.White,
                Font      = CustomFonts.GetFont( CustomFonts.Size.Large ),
                Height    = 75,
                Dock      = DockStyle.Top,
                TextAlign = ContentAlignment.MiddleLeft,
                TabStop   = false,
                Padding   = new Padding( 5, 5, 5, _borderSize ),
                Parent    = this
            };

            ParentChanged += delegate
            {

                if (Parent == null || Parent.Parent == null) return;

                var parent = Parent.Parent as SlmModal;
                if (parent == null) return;

                _borderSize = parent.BorderWidth;
                _title.BackColor = Palette.WhiteLight;
                _title.Padding = new Padding(5, 5, 5, 5 + _borderSize);

                Invalidate();
            };

            _title.Paint +=
                ( s, e ) =>
                {
                    Utility.DrawBorder(
                                       Utility.Border.Bottom,
                                       _borderSize,
                                       Palette.TurquoiseDark,
                                       _title,
                                       e
                        );
                };

            ControlAdded += delegate { _title.SendToBack(); };
        }


        #region Fields

        private Label _title;
        private int _borderSize;

        public string Title { get { return _title.Text; } set { _title.Text = value.ToUpper(); } }
        public Point ContentStart { get { return new Point( 0, _title.Bottom ); } }

        #endregion
        abstract public void FillData(IPerson person);
        abstract public void GetData();
    }
}




 