﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory("")]
    public class SlmDateBox : UserControl
    {
        public SlmDateBox()
        {
            Initialize();
        }

        private void Initialize()
        {
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;

            _dateBox = new DateTimePicker
            {
                BackColor = Palette.White,
                Margin = new Padding(0),
                ShowCheckBox = true,
                Checked = false,
                ShowUpDown = true,
                ForeColor = Palette.GreyDark,
                Format = DateTimePickerFormat.Short,
                Dock = DockStyle.Fill,
                Font = CustomFonts.GetFont(CustomFonts.Size.Large)
            };

            _description = new Label
            {
                Text = "Description help text",
                Font = CustomFonts.GetFont(CustomFonts.Size.Small),
                ForeColor = Palette.GreyLight,
                Size = new Size(250, 20),
                TextAlign = ContentAlignment.TopLeft,
                Location = new Point(0, 45),
                TabStop = false,
                Parent = this
            };

            _icon = new SlmIcon
            {
                Dock = DockStyle.Right,
                IconType = CustomFonts.Icon.Success,
                Size = new Size(28, 50),
                TabStop = false,
                Margin = new Padding(0),
                Visible = false
            };

            Panel textPanel = new Panel
            {
                BackColor = Palette.White,
                Margin = new Padding(0),
                Padding = new Padding(5),
                AutoSize = true,
                Size = new Size(250, 45),
                TabStop = false,
                Controls = { _icon, _dateBox },
                Parent = this
            };
        }


        #region Set/Unset Errors

        public void ClearError()
        {
            _icon.Visible = false;
            _description.Text = _descriptionMessage;
            _description.ForeColor = Palette.GreyLight;
            _description.Visible = !string.IsNullOrWhiteSpace(_descriptionMessage);
        }

        public void SetError(string errorMessage)
        {
            _icon.Visible = true;
            _icon.ActiveColor = Palette.Type.Red;
            _icon.InActiveColor = Palette.Type.Red;
            _icon.IconType = CustomFonts.Icon.Danger;

            _description.Text = errorMessage;
            _description.Visible = true;
            _description.ForeColor = Palette.RedLight;
        }

        public void SetSuccess()
        {
            ClearError();

            _icon.Visible = true;
            _icon.ActiveColor = Palette.Type.Green;
            _icon.InActiveColor = Palette.Type.Green;
            _icon.IconType = CustomFonts.Icon.Success;
        }

        #endregion


        #region Fields/Properties

        private DateTimePicker _dateBox;
        private Label _description;
        private SlmIcon _icon;
        private string _descriptionMessage;

        public string Description
        {
            get { return _descriptionMessage; }
            set
            {
                _descriptionMessage = _description.Text = value;
                _description.Visible = !string.IsNullOrWhiteSpace(_descriptionMessage);
            }
        }


        #region Date Box Neccesary properties

        public bool Checked { get { return _dateBox.Checked; } set { _dateBox.Checked = value; } }

        public DateTimePickerFormat Format { get { return _dateBox.Format; } set { _dateBox.Format = value; } }

        public bool ShowCheckBox { get { return _dateBox.ShowCheckBox; } set { _dateBox.ShowCheckBox = value; } }

        public bool ShowUpDown { get { return _dateBox.ShowUpDown; } set { _dateBox.ShowUpDown = value; } }

        public DateTime Value
        {
            get
            {
                return _dateBox.Value;
            }
            set
            {
                if (value <= _dateBox.MaxDate && value >= _dateBox.MinDate)
                    _dateBox.Value = value;
            }
        }

        #endregion


        #region Disable Properties from editor

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new virtual string Text { get { return base.Text; } set { base.Text = value; } }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new virtual Size Size { get { return base.Size; } set { base.Size = value; } }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new virtual Color ForeColor { get { return base.ForeColor; } set { base.ForeColor = value; } }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new virtual Color BackColor { get { return base.BackColor; } set { base.BackColor = value; } }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new virtual DockStyle Dock { get { return base.Dock; } set { base.Dock = value; } }


        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new virtual Font Font { get { return base.Font; } set { base.Font = value; } }


        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new virtual bool AutoSize { get { return base.AutoSize; } set { base.AutoSize = value; } }

        #endregion


        #endregion
    }
}