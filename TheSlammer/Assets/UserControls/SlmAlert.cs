﻿using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    internal class SlmAlert : SlmModal
    {
        protected SlmAlert( string mes = "message", string title = "success" )
        {
            SlmModalType = ModalType.Green;
            Text = title;

            _icon = new SlmIcon
            {
                IconType = CustomFonts.Icon.Success,
                ActiveColor = Palette.Type.Green,
                InActiveColor = Palette.Type.Green,
                Size = new Size( 120, 120 ),
                Location = new Point( 0, AccessHeight + 10 ),
                Parent = this
            };

            Label message = new Label
            {
                Text = mes,
                Font = CustomFonts.GetFont( CustomFonts.Size.Block ),
                AutoSize = true,
                TextAlign = ContentAlignment.MiddleCenter,
                Padding = new Padding( 30, 0, 30, 0 ),
                MaximumSize = new Size( 350, 300 ),
                Location = new Point( 0, _icon.Bottom + 10 ),
                Parent = this
            };
            message.Paint += delegate( object sender, PaintEventArgs args )
            {
                message.Size = args.Graphics.MeasureString( message.Text, message.Font ).ToSize();
            };

            SlmButton acceptButton = new SlmButton
            {
                SlmButtonSize = SlmButton.ButtonSize.Large,
                IconPosition = SlmButton.IconLocation.Left,
                Text = "Accept",
                BackgroundColor = Palette.Type.Grey,
                BackgroundHoverColor = Palette.Type.Green,
                Parent = Footer
            };
            acceptButton.Click += delegate
            {
                DialogResult = DialogResult.OK;
                Close();
            };

            Utility.HorizontalCenterControl( _icon, this );
            Utility.HorizontalCenterControl( message, this );
            Utility.CenterControl( acceptButton, Footer );
        }


        #region Static Methods

        internal static DialogResult ShowError( string message = "An error occurred", string title = "Error!" )
        {
            SlmAlert alert = new SlmAlert( message, title );

            _icon.IconType = CustomFonts.Icon.X;
            _icon.ActiveColor = Palette.Type.Red;
            _icon.InActiveColor = Palette.Type.Red;
            alert.SlmModalType = ModalType.Red;

            alert.ShowDialog();

            return alert.DialogResult;
        }

        internal static DialogResult ShowWarning( string message = "You have been warned", string title = "Warning" )
        {
            SlmAlert alert = new SlmAlert( message, title );

            _icon.IconType = CustomFonts.Icon.Warning;
            _icon.ActiveColor = Palette.Type.Yellow;
            _icon.InActiveColor = Palette.Type.Yellow;
            alert.SlmModalType = ModalType.Yellow;

            alert.ShowDialog();

            return alert.DialogResult;
        }

        internal static DialogResult ShowSuccess( string message = "You were successful", string title = "Success" )
        {
            SlmAlert alert = new SlmAlert( message, title );

            alert.ShowDialog();

            return alert.DialogResult;
        }

        internal static DialogResult ShowMessage( string message = "Just a message", string title = "Information" )
        {
            SlmAlert alert = new SlmAlert( message, title );

            _icon.IconType = CustomFonts.Icon.Information;
            _icon.ActiveColor = Palette.Type.Blue;
            _icon.InActiveColor = Palette.Type.Blue;
            alert.SlmModalType = ModalType.Blue;

            alert.ShowDialog();

            return alert.DialogResult;
        }

        #endregion


        #region Fields/Properties

        private static SlmIcon _icon;

        #endregion
    }
}