﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    public class SlmTextBoxPlus : UserControl
    {
        #region Enums

        public enum TextBoxType
        {
            Default,
            Alpha,
            Numberic,
            AlphaNumeric,
            AlphaNoWhiteSpace
        }

        #endregion


        public SlmTextBoxPlus()
        {
            Initialize();
        }

        private void Initialize()
        {
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;

            _textBox = new SlmTextBox
            {
                BorderStyle = BorderStyle.None,
                Dock = DockStyle.Fill,
                Font = CustomFonts.GetFont( CustomFonts.Size.Large ),
                WaterMark = "SlmTextBox",
                WaterMarkActiveForeColor = Palette.GreyDark,
                WaterMarkFont = CustomFonts.GetFont( CustomFonts.Size.Medium ),
                WaterMarkForeColor = Palette.GreyLight,
                Margin = new Padding( 0 )
            };

            _description = new Label
            {
                Text = "Description help text",
                Font = CustomFonts.GetFont( CustomFonts.Size.Small ),
                ForeColor = Palette.GreyLight,
                Size = new Size( 250, 20 ),
                TextAlign = ContentAlignment.TopLeft,
                Location = new Point( 0, 45 ),
                Parent = this
            };

            _icon = new SlmIcon
            {
                Dock = DockStyle.Right,
                IconType = CustomFonts.Icon.Success,
                Size = new Size( 28, 50 ),
                TabStop = false,
                Margin = new Padding( 0 ),
                Visible = false
            };

            _textPanel = new Panel
            {
                Location = new Point( 0, 0 ),
                BackColor = Color.White,
                Margin = new Padding( 0 ),
                Padding = new Padding( 7, 7, 0, 7 ),
                Size = new Size( 250, 45 ),
                Controls = {_icon, _textBox},
                Parent = this
            };

            TextBoxValidation = TextBoxType.Default;
            _textBox.KeyPress += ValidateTextBoxType;
        }


        #region Event validation

        private void ValidateTextBoxType( object sender, KeyPressEventArgs e )
        {
            OnKeyPress( e );

            switch ( TextBoxValidation )
            {
                case TextBoxType.Alpha :
                    e.Handled = !char.IsLetter( e.KeyChar )
                                && !char.IsWhiteSpace( e.KeyChar )
                                && e.KeyChar != 8;
                    break;

                case TextBoxType.Numberic :
                    e.Handled = !char.IsDigit( e.KeyChar )
                                && e.KeyChar != 8;
                    break;

                case TextBoxType.AlphaNoWhiteSpace :
                    e.Handled = !char.IsLetter( e.KeyChar )
                                && e.KeyChar != 8;
                    break;

                case TextBoxType.AlphaNumeric :
                    e.Handled = !char.IsLetter( e.KeyChar )
                                && !char.IsDigit( e.KeyChar )
                                && e.KeyChar != 8;
                    break;
            }
        }

        #endregion


        #region Set/Unset Errors

        public void ClearError()
        {
            _icon.Visible = false;
            _description.Text = _descriptionMessage;
            _description.ForeColor = Palette.GreyLight;
        }

        public void SetError( string errorMessage )
        {
            _icon.Visible = true;
            _icon.ActiveColor = Palette.Type.Red;
            _icon.InActiveColor = Palette.Type.Red;
            _icon.IconType = CustomFonts.Icon.Danger;

            _description.Text = errorMessage;
            _description.ForeColor = Palette.RedLight;
        }

        public void SetSuccess()
        {
            ClearError();

            _icon.Visible = true;
            _icon.ActiveColor = Palette.Type.Green;
            _icon.InActiveColor = Palette.Type.Green;
            _icon.IconType = CustomFonts.Icon.Success;
        }

        #endregion


        #region Fields/Properties

        private Panel _textPanel;
        private SlmTextBox _textBox;
        private Label _description;
        private SlmIcon _icon;
        private string _descriptionMessage;


        public string Description
        {
            get { return _descriptionMessage; }
            set
            {
                _descriptionMessage = _description.Text = value;
                _description.Visible = !string.IsNullOrWhiteSpace( _descriptionMessage );
            }
        }

        public TextBoxType TextBoxValidation { get; set; }

        public new virtual string Text { get { return _textBox.Text; } set { _textBox.Text = value; } }

        public string Watermark { get { return _textBox.WaterMark; } set { _textBox.WaterMark = value; } }

        public bool UsePasswordChar
        {
            get { return _textBox.UseSystemPasswordChar; }
            set { _textBox.UseSystemPasswordChar = value; }
        }

        public new virtual int TabIndex { get { return _textBox.TabIndex; } set { _textBox.TabIndex = value; } }


        #region Disable Properties from editor

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Size Size { get { return base.Size; } set { base.Size = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Color ForeColor { get { return base.ForeColor; } set { base.ForeColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Color BackColor { get { return base.BackColor; } set { base.BackColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new DockStyle Dock { get { return base.Dock; } set { base.Dock = value; } }


        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new Font Font { get { return base.Font; } set { base.Font = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new AutoSizeMode AutoSizeMode { get { return base.AutoSizeMode; } set { base.AutoSizeMode = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new bool AutoSize { get { return base.AutoSize; } set { base.AutoSize = value; } }

        #endregion


        #endregion
    }
}