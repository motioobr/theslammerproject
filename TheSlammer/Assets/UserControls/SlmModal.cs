﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Properties;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    // [System.ComponentModel.DesignerCategory( "" )]
    public class SlmModal : Form
    {
        public SlmModal()
        {
            InitializeDefualts();
            InitializeComponents();
        }

        private void InitializeDefualts()
        {
            _type = ModalType.Blue;
            _size = ModalSize.Small;

            BorderWidth = 10;

            BackColor = Palette.WhiteLight;
            ForeColor = Palette.GreyDark;

            FormBorderStyle = FormBorderStyle.None;
            StartPosition = FormStartPosition.CenterParent;
            Margin = new Padding( 0 );
            ControlBox = false;

            Icon = Resources.Icon;
        }

        private void InitializeComponents()
        {
            Header = new Panel
            {
                Dock = DockStyle.Top,
                Height = AccessHeight,
                BackColor = Palette.WhiteDark,
                ForeColor = Palette.GreyLight,
                Margin = new Padding( 0 ),
                Padding = new Padding( 10, 0, 10, BorderWidth ),
                TabStop = false,
                Parent = this
            };

            Footer = new Panel
            {
                Dock = DockStyle.Bottom,
                Height = AccessHeight,
                BackColor = Palette.WhiteDark,
                ForeColor = Palette.GreyLight,
                Margin = new Padding( 0 ),
                Padding = new Padding( 10, BorderWidth / 2, 10, 0 ),
                TabStop = false,
                Parent = this
            };

            _title = new Label
            {
                ForeColor = Palette.GreyDark,
                Font = CustomFonts.GetFont( CustomFonts.Size.Medium ),
                Dock = DockStyle.Fill,
                TextAlign = ContentAlignment.MiddleLeft,
                Text = "Modal Title",
                Margin = new Padding( 0 ),
                TabStop = false,
                Parent = Header
            };

            _exitButton = new SlmIcon
            {
                IconType = CustomFonts.Icon.X,
                InActiveColor = Palette.Type.Grey,
                ActiveColor = Palette.Type.Red,
                Dock = DockStyle.Right,
                Width = 20,
                TabStop = false,
                Parent = Header
            };


            #region Register all events

            ControlAdded += delegate
                            {
                                Header.SendToBack();
                                Footer.SendToBack();
                            };

            Header.Paint +=
                ( s, e ) =>
                {
                    Utility.DrawBorder(
                                       Utility.Border.Bottom,
                                       BorderWidth,
                                       Palette.ColorFromType( (Palette.Type)_type ),
                                       (Control)s,
                                       e
                        );
                };

            _exitButton.Click +=
                delegate
                {
                    DialogResult = DialogResult.Abort;
                    Close();
                };

            Footer.Paint +=
                ( s, e ) =>
                {
                    Utility.DrawBorder(
                                       Utility.Border.Top,
                                       BorderWidth / 2,
                                       Palette.ColorFromType( (Palette.Type)_type ),
                                       (Control)s,
                                       e
                        );
                };

            Footer.Resize += delegate { Footer.Height = AccessHeight; };

            Resize += delegate { SetSize(); };

            #endregion
        }


        #region Set Attributes

        private void SetSize()
        {
            WindowState = FormWindowState.Normal;

            switch ( _size )
            {
                case ModalSize.Small :
                    Size = new Size( 300, 350 );
                    break;

                case ModalSize.Medium :
                    Size = new Size( 500, 400 );
                    break;

                case ModalSize.Large :
                    Size = new Size( 600, 475 );
                    break;

                case ModalSize.ExtraLarge :
                    Size = new Size( 800, 700 );
                    break;

                case ModalSize.Block :
                    WindowState = FormWindowState.Maximized;
                    break;
            }
        }

        #endregion


        #region Enums

        public enum ModalSize
        {
            Small,
            Medium,
            Large,
            ExtraLarge,
            Block
        }

        public enum ModalType
        {
            Blue = Palette.Type.Blue,
            Green = Palette.Type.Green,
            Orange = Palette.Type.Orange,
            Purple = Palette.Type.Purple,
            Red = Palette.Type.Red,
            Turquoise = Palette.Type.Turquoise,
            Yellow = Palette.Type.Yellow
        }

        #endregion


        #region Fields/Properties

        protected const int AccessHeight = 45;
        protected Panel Header;
        public Panel Footer;
        private Label _title;
        private SlmIcon _exitButton;
        private ModalSize _size;
        private ModalType _type;

        public int BorderWidth { get; set; }

        public virtual ModalSize SlmModalSize
        {
            get { return _size; }
            set
            {
                _size = value;
                OnResize( EventArgs.Empty );
            }
        }

        public new virtual string Text
        {
            get { return base.Text.ToUpper(); }
            set
            {
                _title.Text = base.Text = value.ToUpper();
                Invalidate();
            }
        }

        public ModalType SlmModalType
        {
            get { return _type; }
            set
            {
                _type = value;
                Invalidate();
            }
        }


        #region Disable designer properties

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual FormStartPosition StartPosition
        {
            get { return base.StartPosition; }
            set { base.StartPosition = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual int TabIndex { get { return base.TabIndex; } set { base.TabIndex = value; } }


        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Cursor Cursor { get { return base.Cursor; } set { base.Cursor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual FormBorderStyle FormBorderStyle
        {
            get { return base.FormBorderStyle; }
            set { base.FormBorderStyle = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual RightToLeft RightToLeft
        {
            get { return base.RightToLeft; }
            set { base.RightToLeft = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Size AutoScrollMargin
        {
            get { return base.AutoScrollMargin; }
            set { base.AutoScrollMargin = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Size AutoScrollMinSize
        {
            get { return base.AutoScrollMinSize; }
            set { base.AutoScrollMinSize = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Point Location { get { return base.Location; } set { base.Location = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual AutoScaleMode AutoScaleMode
        {
            get { return base.AutoScaleMode; }
            set { base.AutoScaleMode = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual bool ControlBox { get { return base.ControlBox; } set { base.ControlBox = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Size MinimumSize { get { return base.MinimumSize; } set { base.MinimumSize = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual AutoSizeMode AutoSizeMode
        {
            get { return base.AutoSizeMode; }
            set { base.AutoSizeMode = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual bool AutoSize { get { return base.AutoSize; } set { base.AutoSize = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Size MaximumSize { get { return base.MaximumSize; } set { base.MaximumSize = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual SizeF AutoScaleDimensions
        {
            get { return base.AutoScaleDimensions; }
            set { base.AutoScaleDimensions = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Size ClientSize { get { return base.ClientSize; } set { base.ClientSize = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual bool MaximizeBox { get { return base.MaximizeBox; } set { base.MaximizeBox = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual bool MinimizeBox { get { return base.MinimizeBox; } set { base.MinimizeBox = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Size Size { get { return base.Size; } set { base.Size = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Color ForeColor { get { return base.ForeColor; } set { base.ForeColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Color BackColor { get { return base.BackColor; } set { base.BackColor = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual DockStyle Dock { get { return base.Dock; } set { base.Dock = value; } }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public new virtual Font Font { get { return base.Font; } set { base.Font = value; } }

        #endregion


        #endregion
    }
}