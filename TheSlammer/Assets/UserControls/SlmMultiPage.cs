﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Model;
using TheSlammer.Model.Data;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Management;
using TheSlammer.Model.Medical;
using TheSlammer.Model.Security;
using TheSlammer.Model.Visit;
using TheSlammer.Views.Pages;

namespace TheSlammer.Assets.UserControls
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory("")]

    public class SlmMultiPage : SlmModal
    {
        private IPerson _person;
        private Enums.Role _addedRole;

        //todo add details for the inmate -  cell etc.
        public SlmMultiPage(IPerson person, List<UserControl> pages, Enums.Role role)
        {
            _person = person;
            Initialize();

            Pages = pages;
            _addedRole = role;
            _pageIndex = 0;
            SetCurrent( 0 );
        }

        private void Initialize()
        {
            SlmModalType = ModalType.Blue;
            SlmModalSize = ModalSize.ExtraLarge;
            Text = "SlmMultiPage";
            Dock = DockStyle.Fill;
            
            _pnlContent = new Panel
            {
                Dock = DockStyle.Fill,
                Parent = this
            };

            _btnCancel = new SlmButton
            {
                Dock = DockStyle.Right,
                IconType = CustomFonts.Icon.Danger,
                Text= "Cancel",
                SlmButtonSize = SlmButton.ButtonSize.Large,
                IconPosition = SlmButton.IconLocation.Left,
                BackgroundColor = Palette.Type.Red,
                BackgroundHoverColor = Palette.Type.Red,
                TextColor = Palette.Type.White,
                TextHoverColor = Palette.Type.White
            };

            _btnSave = new SlmButton
            {
                Dock = DockStyle.Right,
                IconType = CustomFonts.Icon.Success,
                Text = "Save",
                SlmButtonSize = SlmButton.ButtonSize.Large,
                IconPosition = SlmButton.IconLocation.Left,
                BackgroundColor = Palette.Type.Green,
                BackgroundHoverColor = Palette.Type.Green,
                TextColor = Palette.Type.White,
                TextHoverColor = Palette.Type.White
            };

            _btnPrevious = new SlmButton
            {
                Dock = DockStyle.Left,
                IconType = CustomFonts.Icon.ArrowLeft,
                Text = "Previous",
                SlmButtonSize = SlmButton.ButtonSize.Large,
                IconPosition = SlmButton.IconLocation.Left,
                BackgroundColor = Palette.Type.Turquoise,
                BackgroundHoverColor = Palette.Type.Turquoise,
                TextColor = Palette.Type.White,
                TextHoverColor = Palette.Type.White

            };
           
            _btnNext = new SlmButton
            {
                Dock = DockStyle.Left,
                IconType = CustomFonts.Icon.ArrowRight,
                Text = "Next",
                SlmButtonSize = SlmButton.ButtonSize.Large,
                //TODO - DBS - Change the icon to the write side
                IconPosition = SlmButton.IconLocation.Left,
                BackgroundColor = Palette.Type.Turquoise,
                BackgroundHoverColor = Palette.Type.Turquoise,
                TextColor = Palette.Type.White,
                TextHoverColor = Palette.Type.White
            };

            Footer.Controls.Add( _btnSave );
            Footer.Controls.Add( _btnCancel );
            Footer.Controls.Add( _btnNext );
            Footer.Controls.Add( _btnPrevious );

            _btnNext.Click += delegate { SetCurrent( ++_pageIndex ); };
            _btnPrevious.Click += delegate { SetCurrent( --_pageIndex ); };
            _btnCancel.Click += delegate { Close(); };
            _btnSave.Click += Submit;
        }

        private void Submit( object sender, EventArgs e )
        {
            GetData(); 

            var warden = LoggedUser.Instance as Warden;
            var security = LoggedUser.Instance as Security;
            var management = LoggedUser.Instance as Management;
            var administrator = LoggedUser.Instance as Administrator;

            
            switch ( _addedRole )
            {
                case Enums.Role.Warden :
                    if (warden == null) { SlmAlert.ShowError("There was an error while saving"); break; }
                    //TODO add a modify page for a management with office information
                    ( (IWarden)_person ).Office =
                        new Office {Building = ContextInstance.Data.Buildings.FirstOrDefault()};

                    if ( _person.PersonID == 0 )    warden.CreateWarden( _person as IWarden );
                    else    warden.UpdateWarden( _person as IWarden );

                    break;

                case Enums.Role.Secretary :
                    if (management == null) { SlmAlert.ShowError("There was an error while saving"); break; }
                    //TODO add a modify page for a management with office information
                    ((ISecretary)_person).Office =
                       new Office { Building = ContextInstance.Data.Buildings.FirstOrDefault() };

                    if (_person.PersonID == 0) management.CreateSecretary(_person as Secretary);
                    else management.UpdateSecretary(_person as Secretary);

                    break;

                case Enums.Role.Officer :
                    if (management == null) { SlmAlert.ShowError("There was an error while saving"); break; }

                    if ( _person.PersonID == 0 ) management.CreateOfficer( _person as Officer );
                    else management.UpdateOfficer( _person as Officer );

                    break;

                case Enums.Role.Guard :
                    if (management == null) { SlmAlert.ShowError("There was an error while saving"); break; }

                    if ( _person.PersonID == 0 )
                        management.CreateGuard( _person as Guard );
                    else
                        management.UpdateGuard(_person as Guard);

                    break;

                case Enums.Role.Medical :
                    if (management == null) { SlmAlert.ShowError("There was an error while saving"); break; }

                    if ( _person.PersonID == 0 )
                        management.CreateMedical( _person as Medical );
                    else
                        management.UpdateMedical(_person as Medical);
                    break;

                case Enums.Role.Administrator:
                    if (management == null) { SlmAlert.ShowError("There was an error while saving"); break; }

                    if ( _person.PersonID == 0 &&
                         management.CreateAdminisrator( _person as Administrator ) )
                        SlmAlert.ShowSuccess("Administrator was saved");
                    else if ( _person.PersonID == 0 &&
                              management.UpdateAdministrator( _person as Administrator ) )
                        SlmAlert.ShowSuccess("Administrator was saved");
                    else
                        SlmAlert.ShowError("There was an error while saving");

                    break;

                case Enums.Role.Maintenance :
                    if (management == null && administrator == null) { SlmAlert.ShowError("There was an error while saving"); break; }

                    if ( _person.PersonID == 0 && management != null &&
                         management.CreateMaintenance( _person as Maintenance ) ) 
                        SlmAlert.ShowSuccess( "Maintenance was saved" );

                    else if (_person.PersonID == 0 && administrator != null &&
                         administrator.CreateMaintenance( _person as Maintenance ) )
                        SlmAlert.ShowSuccess("Maintenance was updated");

                    else if ( _person.PersonID != 0 && management != null &&
                              management.UpdateMaintenance( _person as Maintenance ) )
                        SlmAlert.ShowSuccess("Maintenance was update");

                    else if (_person.PersonID != 0 && administrator != null &&
                              administrator.UpdateMaintenance(_person as Maintenance))
                        SlmAlert.ShowSuccess("Maintenance was update");
                    else
                        SlmAlert.ShowError( "There was an error while saving" );

                    break;

                case Enums.Role.Inmate :
                    if ( management != null && _person.PersonID == 0 ) management.CreateInmate( _person as Inmate );
                    else if (management != null && _person.PersonID != 0 ) management.UpdateInmate(_person as Inmate);
                    else if (security != null && _person.PersonID == 0 ) security.CreateInmate(_person as Inmate);
                    else if (security != null && _person.PersonID != 0) security.UpdateInmate(_person as Inmate);
                    break;
                case Enums.Role.Visitor :
                    if ( management == null ) {SlmAlert.ShowError( "There was an error while saving" ); break;}

                    if ( _person.PersonID == 0 )
                        management.CreateVisitor( _person as Visitor );
                    else
                        management.UpdateVisitor(_person as Visitor);
                    break;
                default :
                    SlmAlert.ShowError("There was an error while saving");
                    break;
            }
            //closed the window form
            Close();
        }


        private void SetCurrent( int index )
        {
            if ( _pnlContent.Controls.Count != 0 )
            {
                GetData();
                _pnlContent.Controls.RemoveAt(0);
            }

            var slmPage = Pages[ index ] as SlmPage;
            if ( slmPage != null ) slmPage.FillData( _person );

            Pages[index].Dock = DockStyle.Fill;
            _pnlContent.Controls.Add(Pages[index]);
            HasPage();
        }

        private void GetData()
        {
            var slmPage = _pnlContent.Controls[0] as SlmPage;
            if (slmPage != null) slmPage.GetData();
        }
        private void HasPage()
        {
            _btnNext.Visible = Pages.Count != 1;
            _btnPrevious.Visible = Pages.Count != 1;

            _btnPrevious.Enabled = _pageIndex != 0;
            _btnNext.Enabled = _pageIndex != Pages.Count - 1;
        }


        #region Fields/Properties

        private Panel _pnlContent;
        private int _pageIndex;
        protected List<UserControl> Pages;
        private SlmButton _btnNext;
        private SlmButton _btnPrevious;
        private SlmButton _btnSave;
        private SlmButton _btnCancel;

        #endregion
    }
}