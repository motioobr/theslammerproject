﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using TheSlammer.Properties;

namespace TheSlammer.Assets.Fonts
{
    public static class CustomFonts
    {
        public static Font GetFont( Size size )
        {
            switch ( size )
            {
                case Size.Small :
                    return SmallFont;

                case Size.Medium :
                    return MediumtFont;

                case Size.Large :
                    return LargeFont;

                case Size.Block :
                    return BlockFont;

                default :
                    return DefaultFont;
            }
        }

        public static string GetIcon( Icon icon )
        {
            return char.ConvertFromUtf32( (int)icon );
        }


        #region Enums

        public enum Size
        {
            Default,
            Small,
            Medium,
            Large,
            Block
        }

        public enum Icon
        {
            ArrowLeft   = 0xE000,
            ArrowRight  = 0xE001,
            Danger      = 0xE002,
            Maintenance = 0xE003,
            Medical     = 0xE004,
            Office      = 0xE005,
            Officer     = 0xE006,
            Plus        = 0xE007,
            Room        = 0xE008,
            Secretary   = 0xE009,
            Warden      = 0xE00A,
            Logout      = 0xE00B,
            MenuToggle  = 0xE00C,
            Warehouse   = 0xE00D,
            Visitor     = 0xE00E,
            Visit       = 0xE00F,
            Success     = 0xE010,
            Clock       = 0xE011,
            Inmate      = 0xE012,
            X           = 0xE013,
            Warning     = 0xE014,
            Exclamation = 0xE015,
            Block       = 0xE016,
            Information = 0xE017,
            Treatment   = 0xE018,
            Minus       = 0xE019,
            Edit        = 0xE01A,
            Guard       = 0xE01B,
            Trash       = 0xE01C,
            Search      = 0xE01D,
            Open        = 0xE01E,
            Work        = 0xE01F
        }

        #endregion


        #region Load and install fonts

        [DllImport( "gdi32.dll" )]
        private static extern IntPtr AddFontMemResourceEx(
            IntPtr pbfont,
            uint cbfont,
            IntPtr pdv,
            [In] ref uint pcFonts );

        private static void InitializeFonts()
        {
            PrivateFontCollection pfc = new PrivateFontCollection();

            LoadFont( Resources.Ubuntu_R, ref pfc );
            LoadFont( Resources.Ubuntu_B, ref pfc );
            LoadFont( Resources.Ubuntu_L, ref pfc );
            LoadFont( Resources.flaticon, ref pfc );

            foreach ( FontFamily fontFamily in pfc.Families )
            {
                switch ( fontFamily.Name )
                {
                    case "Ubuntu" :
                        _ubuntu = fontFamily;
                        break;

                    case "Ubuntu Light" :
                        _ubuntuLight = fontFamily;
                        break;

                    case "flaticon" :
                        _flatIcon = fontFamily;
                        break;
                }
            }
        }

        private static void LoadFont( byte[] resource, ref PrivateFontCollection pfc )
        {
            byte[] fontArray = resource;
            int length = resource.Length;

            IntPtr ptr = Marshal.AllocCoTaskMem( length );

            Marshal.Copy( fontArray, 0, ptr, length );

            uint cFonts = 0;

            AddFontMemResourceEx( ptr, (uint)fontArray.Length, IntPtr.Zero, ref cFonts );

            pfc.AddMemoryFont( ptr, length );

            Marshal.FreeCoTaskMem( ptr );
        }

        #endregion


        #region Fields/Properties

        private static FontFamily _ubuntu;
        private static FontFamily _ubuntuLight;
        private static FontFamily _flatIcon;


        #region Global font styles

        private static readonly Font DefaultFont = DEVELOPMENT.israfi ? new Font("David", 14, FontStyle.Regular): new Font(
            UbuntuLight, 9, FontStyle.Regular, GraphicsUnit.Point, 0 );

        private static readonly Font SmallFont = DEVELOPMENT.israfi ? new Font("David", 14, FontStyle.Regular) : new Font(
            UbuntuLight, 11, FontStyle.Regular, GraphicsUnit.Point, 0 );

        private static readonly Font MediumtFont = DEVELOPMENT.israfi ? new Font("David", 14, FontStyle.Regular) : new Font(
            UbuntuLight, 14, FontStyle.Bold, GraphicsUnit.Point, 0 );

        private static readonly Font LargeFont = DEVELOPMENT.israfi ? new Font("David", 14, FontStyle.Regular) : new Font(
            Ubuntu, 18, FontStyle.Regular, GraphicsUnit.Point, 0 );

        private static readonly Font BlockFont = DEVELOPMENT.israfi ? new Font("David", 14, FontStyle.Regular) : new Font(
            Ubuntu, 20, FontStyle.Bold, GraphicsUnit.Point, 0 );

        #endregion


        public static FontFamily Ubuntu
        {
            get
            {
                if ( _ubuntu == null ) InitializeFonts();
                return _ubuntu;
            }
        }

        public static FontFamily UbuntuLight
        {
            get
            {
                if ( _ubuntu == null ) InitializeFonts();
                return _ubuntuLight;
            }
        }

        public static FontFamily FlatIcon
        {
            get
            {
                if ( _ubuntu == null ) InitializeFonts();
                return _flatIcon;
            }
        }

        #endregion
    }
}