﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.People;

namespace TheSlammer.Views.Pages
{
    [DesignerCategory("")]
    [ToolboxItem(false)]
    public class ModifyPerson : SlmPage
    {
        private DateTimePicker dtpBirthDate;
        public  IPerson FormPerson;
        private GroupBox grbGender;
        private GroupBox grbName;
        private Label lblDateOfBirth;
        private BindingSource personBindingSource;
        private RadioButton rdbFemale;
        private RadioButton rdbMale;
        private SlmTextBoxPlus txtFirstName;
        private SlmTextBoxPlus txtIDNumber;
        private SlmTextBoxPlus txtLastName;

        
        
        public ModifyPerson()
        {
            InitializeComponent();           
        }

        public override void FillData( IPerson person )
        {
            FormPerson = person;

            txtFirstName.Text = FormPerson.FirstName;
            txtLastName.Text = FormPerson.LastName;
            txtIDNumber.Text = FormPerson.IdNumber.ToString();
            rdbMale.Checked = FormPerson.Gender == Enums.Gender.Male;
            if (FormPerson.DateOfBirth != null)
                dtpBirthDate.Value = FormPerson.DateOfBirth.Value;
            else
                dtpBirthDate.Checked = false;
        }

        public override void GetData()
        {
            FormPerson.FirstName = txtFirstName.Text;
            FormPerson.LastName = txtLastName.Text;
            FormPerson.IdNumber = Int32.Parse( txtIDNumber.Text );
            FormPerson.DateOfBirth = dtpBirthDate.Checked ? dtpBirthDate.Value : (DateTime?)null;
            rdbMale.Checked = FormPerson.Gender == Enums.Gender.Male;
            FormPerson.Gender = rdbMale.Checked ? Enums.Gender.Male : Enums.Gender.Female;
        }


       

        private void rdbMale_CheckedChanged(object sender, EventArgs e)
        {
            FormPerson.Gender = rdbMale.Checked
                                    ? Enums.Gender.Male
                                    : Enums.Gender.Female;
        }

        private void txtIDNumber_TextChanged(object sender, EventArgs e)
        {
            if (txtIDNumber.Text == "0") txtIDNumber.Text = "";
        }

        private void InitializeComponent()
        {
            lblDateOfBirth = new Label();
            txtIDNumber = new SlmTextBoxPlus();
            txtLastName = new SlmTextBoxPlus();
            txtFirstName = new SlmTextBoxPlus();
            dtpBirthDate = new DateTimePicker();
            grbGender = new GroupBox();
            rdbFemale = new RadioButton();
            rdbMale = new RadioButton();
            grbName = new GroupBox();
            grbGender.SuspendLayout();
            grbName.SuspendLayout();
            SuspendLayout();
            // 
            // lblDateOfBirth
            // 
            lblDateOfBirth.AutoSize = true;
            lblDateOfBirth.Location = new Point(409, 234);
            lblDateOfBirth.Name = "lblDateOfBirth";
            lblDateOfBirth.Size = new Size(82, 16);
            lblDateOfBirth.TabIndex = 11;
            lblDateOfBirth.Text = "Date Of Birth";
            // 
            // txtIDNumber
            // 
            txtIDNumber.Description = null;
            txtIDNumber.Location = new Point(418, 90);
            txtIDNumber.Margin = new Padding(3, 4, 3, 4);
            txtIDNumber.Name = "txtIDNumber";
            txtIDNumber.TabIndex = 3;
            txtIDNumber.TextBoxValidation = SlmTextBoxPlus.TextBoxType.Numberic;
            txtIDNumber.UsePasswordChar = false;
            txtIDNumber.Watermark = "Id Number";
            txtIDNumber.TextChanged += txtIDNumber_TextChanged;
            // 
            // txtLastName
            // 
            txtLastName.Description = null;
            txtLastName.Location = new Point(42, 107);
            txtLastName.Margin = new Padding(3, 4, 3, 4);
            txtLastName.Name = "txtLastName";
            txtLastName.TabIndex = 2;
            txtLastName.TextBoxValidation = SlmTextBoxPlus.TextBoxType.Alpha;
            txtLastName.UsePasswordChar = false;
            txtLastName.Watermark = "Last Name";
            // 
            // txtFirstName
            // 
            txtFirstName.Description = null;
            txtFirstName.Location = new Point(42, 23);
            txtFirstName.Margin = new Padding(3, 4, 3, 4);
            txtFirstName.Name = "txtFirstName";
            txtFirstName.TabIndex = 1;
            txtFirstName.TextBoxValidation = SlmTextBoxPlus.TextBoxType.Alpha;
            txtFirstName.UsePasswordChar = false;
            txtFirstName.Watermark = "First Name";
            // 
            // dtpBirthDate
            // 
            dtpBirthDate.Checked = true;
            dtpBirthDate.Format = DateTimePickerFormat.Short;
            dtpBirthDate.Location = new Point(412, 269);
            dtpBirthDate.Margin = new Padding(3, 4, 3, 4);
            dtpBirthDate.Name = "dtpBirthDate";
            dtpBirthDate.RightToLeft = RightToLeft.No;
            dtpBirthDate.ShowCheckBox = true;
            dtpBirthDate.ShowUpDown = true;
            dtpBirthDate.Size = new Size(227, 21);
            dtpBirthDate.TabIndex = 5;

            // 
            // grbGender
            // 
            grbGender.Controls.Add(rdbFemale);
            grbGender.Controls.Add(rdbMale);
            grbGender.Location = new Point(412, 160);
            grbGender.Margin = new Padding(3, 4, 3, 4);
            grbGender.Name = "grbGender";
            grbGender.Padding = new Padding(3, 4, 3, 4);
            grbGender.Size = new Size(233, 42);
            grbGender.TabIndex = 4;
            grbGender.TabStop = false;
            grbGender.Text = "Gender";
            // 
            // rdbFemale
            // 
            rdbFemale.AutoSize = true;
            rdbFemale.Location = new Point(127, 17);
            rdbFemale.Margin = new Padding(3, 4, 3, 4);
            rdbFemale.Name = "rdbFemale";
            rdbFemale.Size = new Size(67, 20);
            rdbFemale.TabIndex = 5;
            rdbFemale.Text = "Female";
            rdbFemale.UseVisualStyleBackColor = true;
            // 
            // rdbMale
            // 
            rdbMale.AutoSize = true;
            rdbMale.Checked = true;
            rdbMale.Location = new Point(26, 17);
            rdbMale.Margin = new Padding(3, 4, 3, 4);
            rdbMale.Name = "rdbMale";
            rdbMale.Size = new Size(54, 20);
            rdbMale.TabIndex = 4;
            rdbMale.TabStop = true;
            rdbMale.Text = "Male";
            rdbMale.UseVisualStyleBackColor = true;
            rdbMale.CheckedChanged += rdbMale_CheckedChanged;
            // 
            // grbName
            // 
            grbName.Controls.Add(txtFirstName);
            grbName.Controls.Add(txtLastName);
            grbName.Location = new Point(17, 90);
            grbName.Margin = new Padding(3, 4, 3, 4);
            grbName.Name = "grbName";
            grbName.Padding = new Padding(3, 4, 3, 4);
            grbName.Size = new Size(310, 211);
            grbName.TabIndex = 15;
            grbName.TabStop = false;
            grbName.Text = "Name";
            // 
            // ModifyPerson
            // 
            AutoScaleDimensions = new SizeF(7F, 16F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(grbName);
            Controls.Add(grbGender);
            Controls.Add(dtpBirthDate);
            Controls.Add(lblDateOfBirth);
            Controls.Add(txtIDNumber);
            Margin = new Padding(3, 4, 3, 4);
            Name = "ModifyPerson";
            Size = new Size(817, 615);
            Title = "PERSON INFORMATION";
            Controls.SetChildIndex(txtIDNumber, 0);
            Controls.SetChildIndex(lblDateOfBirth, 0);
            Controls.SetChildIndex(dtpBirthDate, 0);
            Controls.SetChildIndex(grbGender, 0);
            Controls.SetChildIndex(grbName, 0);
            grbGender.ResumeLayout(false);
            grbGender.PerformLayout();
            grbName.ResumeLayout(false);
            grbName.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }
    }
}