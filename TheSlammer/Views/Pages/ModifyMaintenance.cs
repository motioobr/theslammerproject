﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Maintenance;

namespace TheSlammer.Views.Pages
{
    [DesignerCategory( "" )]
    [ToolboxItem( false )]
    public class ModifyMaintenance : SlmPage
    {
        private CheckedListBox clbExpierense;
        public IMaintenance FormMaintenance;

        public ModifyMaintenance()
        {
            InitializeComponent();
            DataSource();
        }

        private void DataSource()
        {
            ( (ListBox)clbExpierense ).DataSource = Enum.GetValues( typeof ( Enums.MaintenanceType ) );
            clbExpierense.Height = ( clbExpierense.PreferredHeight + 20 );
        }

        public override void FillData( IPerson person )
        {
            FormMaintenance = (IMaintenance)person;

            ///connect to the current expierense data
            for ( int Index = 0; Index < clbExpierense.Items.Count; Index++ )
                if ( Maintenance.HasExpertise( (Enums.MaintenanceType)Math.Pow( 2, Index ), FormMaintenance ) )
                    clbExpierense.SetItemCheckState( Index, CheckState.Checked );
        }

        public override void GetData() {}


        private void clbExpierense_ItemCheck( object sender, ItemCheckEventArgs e )
        {
            // The current value is again of the status 
            /// if the user checked then the current value is unchecked 
            /// 
            if ( e.CurrentValue == CheckState.Checked ) 
                Maintenance.RemoveExpertise( (Enums.MaintenanceType)Math.Pow( 2, e.Index ), FormMaintenance );
            else 
                Maintenance.AddExperties( (Enums.MaintenanceType)Math.Pow( 2, e.Index ), FormMaintenance );
        }

        private void InitializeComponent()
        {
            clbExpierense = new CheckedListBox();
            SuspendLayout();

            // clbExpierense
            // 
            clbExpierense.CheckOnClick = true;
            clbExpierense.FormattingEnabled = true;
            clbExpierense.Location = new Point( 155, 136 );
            clbExpierense.Name = "clbExpierense";
            clbExpierense.Size = new Size( 211, 64 );
            clbExpierense.TabIndex = 1;
            clbExpierense.ItemCheck += clbExpierense_ItemCheck;
            // 
            // ModifyMaintenance
            // 
            AutoScaleDimensions = new SizeF( 6F, 13F );
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add( clbExpierense );
            Name = "ModifyMaintenance";
            Controls.SetChildIndex( clbExpierense, 0 );
            ResumeLayout( false );
            PerformLayout();
        }
    }
}