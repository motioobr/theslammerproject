﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.People;

namespace TheSlammer.Views.Pages
{
    [DesignerCategory( "" )]
    [ToolboxItem( false )]
    public class ModifyUser : SlmPage
    {
        private IUser FormUser;
        private SlmTextBoxPlus stbConfirmPassword;
        private SlmTextBoxPlus stbPassword;
        private SlmTextBoxPlus stbUsername;

        public ModifyUser(  )
        {
            InitializeComponent();

        }


        public override void FillData( IPerson person )
        {
            FormUser = (IUser)person;
            stbUsername.Text = FormUser.Username;
            stbPassword.Text = FormUser.Password;
        }

        public override void GetData()
        {
            FormUser.Username = stbUsername.Text;
            FormUser.Password = stbPassword.Text;
        }

        private void InitializeComponent()
        {
            stbUsername = new SlmTextBoxPlus();
            stbPassword = new SlmTextBoxPlus();
            stbConfirmPassword = new SlmTextBoxPlus();
            SuspendLayout();
            // 
            // stbUsername
            // 
            stbUsername.Location = new Point( 197, 117 );
            stbUsername.Margin = new Padding( 3, 4, 3, 4 );
            stbUsername.Name = "stbUsername";
            stbUsername.Size = new Size( 257, 21 );
            stbUsername.TabIndex = 1;
            stbUsername.Watermark = "Username";
            stbUsername.Description = "6 - 30 characters";
            stbUsername.TextBoxValidation = SlmTextBoxPlus.TextBoxType.AlphaNumeric;
            // 
            // stbPassword
            // 
            stbPassword.Location = new Point( 197, 192 );
            stbPassword.Margin = new Padding( 3, 4, 3, 4 );
            stbPassword.Name = "stbPassword";
            stbPassword.Size = new Size( 257, 21 );
            stbPassword.TabIndex = 2;
            stbPassword.Watermark = "Password";
            stbPassword.Description = "6 - 30 characters";
            stbPassword.TextBoxValidation = SlmTextBoxPlus.TextBoxType.AlphaNumeric;
            stbPassword.UsePasswordChar = true;
            // 
            // stbConfirmPassword
            // 
            stbConfirmPassword.Location = new Point( 197, 274 );
            stbConfirmPassword.Margin = new Padding( 3, 4, 3, 4 );
            stbConfirmPassword.Name = "stbConfirmPassword";
            stbConfirmPassword.Size = new Size( 257, 21 );
            stbConfirmPassword.TabIndex = 3;
            stbConfirmPassword.Watermark = "Confirm Password";
            stbConfirmPassword.Description = "6 - 30 characters";
            stbConfirmPassword.TextBoxValidation = SlmTextBoxPlus.TextBoxType.AlphaNumeric;
            stbConfirmPassword.UsePasswordChar = true;
            // 
            // ModifyUser
            // 
            AutoScaleDimensions = new SizeF( 7F, 16F );
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add( stbConfirmPassword );
            Controls.Add( stbPassword );
            Controls.Add( stbUsername );
            Margin = new Padding( 3, 4, 3, 4 );
            Name = "ModifyUser";
            Size = new Size( 817, 615 );
            Title = "USER PROFILE DETAILS";
            Controls.SetChildIndex( stbUsername, 0 );
            Controls.SetChildIndex( stbPassword, 0 );
            Controls.SetChildIndex( stbConfirmPassword, 0 );
            ResumeLayout( false );
            PerformLayout();
        }

      
    }
}