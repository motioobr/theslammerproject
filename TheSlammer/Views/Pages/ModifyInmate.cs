﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Security;

namespace TheSlammer.Views.Pages
{
    [DesignerCategory( "" )]
    [ToolboxItem( false )]
    public class ModifyInmate : SlmPage
    {
        private ComboBox cmbDangerLevel;
        private ComboBox cmbEyeColor;
        private ComboBox cmbHairColor;
        private ComboBox cmbHealth;
        private ComboBox cmbReligion;
        public IInmate FormInmate;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Label labEyeColor;
        private Label labHairColor;
        private Label labHealth;
        private Label labReligion;
        private Label lblDangerLevel;
        private SlmTextBoxPlus txtHeight;
        private SlmTextBoxPlus txtWeight;

        public ModifyInmate( )
        {
            InitializeComponent();
            DataSource();
        }

        private void DataSource()
        {
            cmbDangerLevel.DataSource = Enum.GetValues( typeof ( AccessLevel.DangerLevel ) );
            cmbEyeColor.DataSource = Enum.GetValues( typeof ( Enums.EyeColor ) );
            cmbHairColor.DataSource = Enum.GetValues( typeof ( Enums.HairColor ) );
            cmbHealth.DataSource = Enum.GetValues( typeof ( Enums.Health ) );
            cmbReligion.DataSource = Enum.GetValues( typeof ( Enums.Religion ) );
        }

        private void InitializeComponent()
        {
            cmbReligion = new ComboBox();
            cmbHealth = new ComboBox();
            cmbEyeColor = new ComboBox();
            cmbDangerLevel = new ComboBox();
            cmbHairColor = new ComboBox();
            lblDangerLevel = new Label();
            labEyeColor = new Label();
            labHairColor = new Label();
            labHealth = new Label();
            labReligion = new Label();
            groupBox1 = new GroupBox();
            txtWeight = new SlmTextBoxPlus();
            txtHeight = new SlmTextBoxPlus();
            groupBox2 = new GroupBox();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            SuspendLayout();
            // 
            // cmbReligion
            // 
            cmbReligion.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbReligion.FormattingEnabled = true;
            cmbReligion.Location = new Point( 33, 208 );
            cmbReligion.Margin = new Padding( 3, 4, 3, 4 );
            cmbReligion.Name = "cmbReligion";
            cmbReligion.Size = new Size( 227, 24 );
            cmbReligion.TabIndex = 13;
            // 
            // cmbHealth
            // 
            cmbHealth.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbHealth.FormattingEnabled = true;
            cmbHealth.Location = new Point( 33, 129 );
            cmbHealth.Margin = new Padding( 3, 4, 3, 4 );
            cmbHealth.Name = "cmbHealth";
            cmbHealth.Size = new Size( 227, 24 );
            cmbHealth.TabIndex = 12;
            // 
            // cmbEyeColor
            // 
            cmbEyeColor.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbEyeColor.FormattingEnabled = true;
            cmbEyeColor.Location = new Point( 40, 54 );
            cmbEyeColor.Margin = new Padding( 3, 4, 3, 4 );
            cmbEyeColor.Name = "cmbEyeColor";
            cmbEyeColor.Size = new Size( 227, 24 );
            cmbEyeColor.TabIndex = 7;
            // 
            // cmbDangerLevel
            // 
            cmbDangerLevel.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbDangerLevel.FormattingEnabled = true;
            cmbDangerLevel.Location = new Point( 33, 54 );
            cmbDangerLevel.Margin = new Padding( 3, 4, 3, 4 );
            cmbDangerLevel.Name = "cmbDangerLevel";
            cmbDangerLevel.Size = new Size( 227, 24 );
            cmbDangerLevel.TabIndex = 11;
            // 
            // cmbHairColor
            // 
            cmbHairColor.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbHairColor.FormattingEnabled = true;
            cmbHairColor.Location = new Point( 40, 129 );
            cmbHairColor.Margin = new Padding( 3, 4, 3, 4 );
            cmbHairColor.Name = "cmbHairColor";
            cmbHairColor.Size = new Size( 227, 24 );
            cmbHairColor.TabIndex = 8;
            // 
            // lblDangerLevel
            // 
            lblDangerLevel.AutoSize = true;
            lblDangerLevel.Location = new Point( 94, 22 );
            lblDangerLevel.Name = "lblDangerLevel";
            lblDangerLevel.Size = new Size( 82, 16 );
            lblDangerLevel.TabIndex = 16;
            lblDangerLevel.Text = "Danger Level";
            // 
            // labEyeColor
            // 
            labEyeColor.AutoSize = true;
            labEyeColor.Location = new Point( 118, 22 );
            labEyeColor.Name = "labEyeColor";
            labEyeColor.Size = new Size( 62, 16 );
            labEyeColor.TabIndex = 17;
            labEyeColor.Text = "Eye Color";
            // 
            // labHairColor
            // 
            labHairColor.AutoSize = true;
            labHairColor.Location = new Point( 117, 98 );
            labHairColor.Name = "labHairColor";
            labHairColor.Size = new Size( 63, 16 );
            labHairColor.TabIndex = 18;
            labHairColor.Text = "Hair Color";
            // 
            // labHealth
            // 
            labHealth.AutoSize = true;
            labHealth.Location = new Point( 114, 96 );
            labHealth.Name = "labHealth";
            labHealth.Size = new Size( 45, 16 );
            labHealth.TabIndex = 19;
            labHealth.Text = "Health";
            // 
            // labReligion
            // 
            labReligion.AutoSize = true;
            labReligion.Location = new Point( 106, 177 );
            labReligion.Name = "labReligion";
            labReligion.Size = new Size( 53, 16 );
            labReligion.TabIndex = 20;
            labReligion.Text = "Religion";
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add( txtWeight );
            groupBox1.Controls.Add( txtHeight );
            groupBox1.Controls.Add( labHairColor );
            groupBox1.Controls.Add( cmbHairColor );
            groupBox1.Controls.Add( cmbEyeColor );
            groupBox1.Controls.Add( labEyeColor );
            groupBox1.Location = new Point( 3, 78 );
            groupBox1.Margin = new Padding( 3, 4, 3, 4 );
            groupBox1.Name = "groupBox1";
            groupBox1.Padding = new Padding( 3, 4, 3, 4 );
            groupBox1.Size = new Size( 299, 338 );
            groupBox1.TabIndex = 21;
            groupBox1.TabStop = false;
            groupBox1.Text = "Person Description";
            // 
            // txtWeight
            // 
            txtWeight.Description = null;
            txtWeight.Location = new Point( 37, 177 );
            txtWeight.Margin = new Padding( 3, 4, 3, 4 );
            txtWeight.Name = "txtWeight";
            txtWeight.TabIndex = 9;
            txtWeight.TextBoxValidation = SlmTextBoxPlus.TextBoxType.Numberic;
            txtWeight.UsePasswordChar = false;
            txtWeight.Watermark = "Weight";
            // 
            // txtHeight
            // 
            txtHeight.Description = null;
            txtHeight.Location = new Point( 40, 270 );
            txtHeight.Margin = new Padding( 3, 4, 3, 4 );
            txtHeight.Name = "txtHeight";
            txtHeight.TabIndex = 10;
            txtHeight.TextBoxValidation = SlmTextBoxPlus.TextBoxType.Numberic;
            txtHeight.UsePasswordChar = false;
            txtHeight.Watermark = "Height";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add( cmbHealth );
            groupBox2.Controls.Add( lblDangerLevel );
            groupBox2.Controls.Add( labReligion );
            groupBox2.Controls.Add( cmbDangerLevel );
            groupBox2.Controls.Add( labHealth );
            groupBox2.Controls.Add( cmbReligion );
            groupBox2.Location = new Point( 336, 78 );
            groupBox2.Margin = new Padding( 3, 4, 3, 4 );
            groupBox2.Name = "groupBox2";
            groupBox2.Padding = new Padding( 3, 4, 3, 4 );
            groupBox2.Size = new Size( 307, 338 );
            groupBox2.TabIndex = 22;
            groupBox2.TabStop = false;
            groupBox2.Text = "Person Details";
            // 
            // ModifyInmate
            // 
            AutoScaleDimensions = new SizeF( 7F, 16F );
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add( groupBox2 );
            Controls.Add( groupBox1 );
            Margin = new Padding( 3, 4, 3, 4 );
            Name = "ModifyInmate";
            Size = new Size( 817, 615 );
            Title = "INMATE PROFILE";
            Controls.SetChildIndex( groupBox1, 0 );
            Controls.SetChildIndex( groupBox2, 0 );
            groupBox1.ResumeLayout( false );
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout( false );
            groupBox2.PerformLayout();
            ResumeLayout( false );
        }

        public override void FillData( IPerson person )
        {
            FormInmate = (IInmate)person;

            txtHeight.Text = FormInmate.Height.ToString();
            txtWeight.Text = FormInmate.Weight.ToString();
            cmbEyeColor.SelectedIndex = (int)FormInmate.Eye;
            cmbHairColor.SelectedIndex = (int)FormInmate.Hair;
            cmbHealth.SelectedIndex = (int)FormInmate.Health;
            cmbReligion.SelectedIndex = (int)FormInmate.Religion;
        }

        public override void GetData()
        {
            if ( FormInmate != null )
            {
                FormInmate.Height = decimal.Parse( txtHeight.Text );
                FormInmate.Weight = decimal.Parse( txtWeight.Text  );
                FormInmate.Eye = (Enums.EyeColor)cmbEyeColor.SelectedIndex;
                FormInmate.Hair = (Enums.HairColor)cmbHairColor.SelectedIndex;
                FormInmate.Health = (Enums.Health)cmbHealth.SelectedIndex;
                FormInmate.Religion = (Enums.Religion)cmbReligion.SelectedIndex;
                if ( FormInmate.PersonID == 0 )
                {
                    FormInmate.IncarcerationDate = DateTime.Now;
                    FormInmate.InCustody = true;
                }
                FormInmate.Cell = ContextInstance.Data.Cells.FirstOrDefault();
            }
        }
    }
}