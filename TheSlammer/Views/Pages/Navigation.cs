﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Management;
using TheSlammer.Model.Medical;
using TheSlammer.Model.Security;
using TheSlammer.Model.Visit;
using TheSlammer.Properties;
using TheSlammer.Views.Forms;
using TheSlammer.Views.GridViews;
using TheSlammer.Views.ModifyForms;
using TheSlammer.Views.ModifyForms.Works;

namespace TheSlammer.Views.Pages
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    [ToolboxItem( false )]
    internal class Navigation : UserControl
    {
        internal Navigation()
        {
            Initialize();
        }

        private void InitializeProfile()
        {
            _profile = new Panel
            {
                BackColor = Palette.BlackLight,
                ForeColor = Palette.White,
                Dock = DockStyle.Top,
                Margin = new Padding( 0 ),
                Size = new Size( 300, 100 ),
                Controls =
                {
                    new Label
                    {
                        Text = LoggedUser.Instance.Name,
                        Font = CustomFonts.GetFont( CustomFonts.Size.Medium ),
                        TextAlign = ContentAlignment.BottomCenter,
                        Location = new Point( 100, 0 ),
                        Size = new Size( 200, 50 ),
                        ForeColor = Palette.OrangeLight
                    },
                    new Label
                    {
                        Text = LoggedUser.Instance.PersonRole.ToString(),
                        Font = CustomFonts.GetFont( CustomFonts.Size.Small ),
                        TextAlign = ContentAlignment.TopCenter,
                        Location = new Point( 100, 50 ),
                        Size = new Size( 200, 50 )
                    },
                    new SlmIcon
                    {
                        IconType = CustomFonts.Icon.Warden,
                        InActiveColor = Palette.Type.Orange,
                        ActiveColor = Palette.Type.Orange,
                        Size = new Size( 90, 90 ),
                        Location = new Point( 10, 10 )
                    }
                }
            };
        }

        private void Initialize()
        {
            Dock = DockStyle.Left;
            BackColor = Palette.BlackDark;
            ForeColor = Palette.White;
            Font = CustomFonts.GetFont( CustomFonts.Size.Medium );

            InitializeProfile();
            InitializeLogoutButton();
            InitializeBanner();

            InitializeAddNewPerson();
            InitializeAddNewLocation();

            InitializeViewAllPeople();
            InitializeViewAllLocations();
            IntializeMaintenanceMenu();

            Control[] controls =
            {
                new SlmNavigationButton(),
                _viewAllLocations,
                _viewAllPeople,

                _addNewLocation,
                _addNewPerson,
                _viewMaintenceMenu,
                _profile,
                _logoutButton,
                _banner
            };
            Controls.AddRange( controls );

            Resize += delegate { Width = 300; };
        }

        private void InitializeBanner()
        {
            _banner = new Panel
            {
                AutoSize = true,
                Dock = DockStyle.Top,
                Controls =
                {
                    new PictureBox
                    {
                        BackColor = Palette.PurpleLight,
                        Dock = DockStyle.Top,
                        Image = Resources.LogoBanner,
                        Margin = new Padding( 0 ),
                        Size = new Size( 300, 75 ),
                        SizeMode = PictureBoxSizeMode.Zoom
                    },
                    new Label
                    {
                        Text = @"Navigation".ToUpper(),
                        BackColor = Palette.ColorFromType( Palette.Type.Orange ),
                        TextAlign = ContentAlignment.MiddleCenter,
                        Font = CustomFonts.GetFont( CustomFonts.Size.Medium ),
                        Dock = DockStyle.Top,
                        Size = new Size( 300, 45 )
                    }
                }
            };
        }

        private void InitializeLogoutButton()
        {
            _logoutButton = new SlmButton
            {
                IconType = CustomFonts.Icon.Logout,
                SlmButtonSize = SlmButton.ButtonSize.Navigation,
                BackgroundColor = Palette.Type.Orange,
                Dock = DockStyle.Bottom,
                IconPosition = SlmButton.IconLocation.Right,
                Text = "Logout"
            };
            _logoutButton.Click += delegate { Application.Exit(); };
        }

        private void InitializeViewAllPeople()
        {
            _viewAllPeople = new SlmButton
            {
                IconType = CustomFonts.Icon.Open,
                Text = "View All People"
            };
            CreateButton(_viewAllPeople);

         
        }

        private void InitializeViewAllLocations()
        {
            _viewAllLocations = new SlmButton
            {
                IconType = CustomFonts.Icon.Open,
                Text = "View All Locations"
            };

            CreateButton( _viewAllLocations );
        }

        private void IntializeMaintenanceMenu()
        {
            _viewMaintenceMenu = new SlmButton
            {
                IconType = CustomFonts.Icon.Maintenance,
                Text = "Maintenance menu"
            };

            _viewMaintenceMenu.Click += delegate { InitializeMaintenceSubMenu(); };
            CreateButton(_viewMaintenceMenu);
        }

        private void InitializeMaintenceSubMenu()
        {
             var parent = ParentForm as MainDashboard;
            if ( parent == null ) return;

            Dictionary<string, SlmButton> subMenuButtons = new Dictionary<string, SlmButton>
            {
                {"Add a new work", new SlmButton {IconType = CustomFonts.Icon.Plus}},
                {"Edit a work", new SlmButton {IconType = CustomFonts.Icon.Edit}},
                {"Delete a work", new SlmButton {IconType = CustomFonts.Icon.Trash}},
                {"View works", new SlmButton {IconType = CustomFonts.Icon.Work}},
                {"Matching a work", new SlmButton {IconType = CustomFonts.Icon.Work}},               
            };

            foreach (var button in subMenuButtons)
            {
                button.Value.Click += delegate { MaintenceMenu(button.Key); };
            }

            CreateSubMenu(subMenuButtons, "Maintenance menu", parent);
        }

        private void MaintenceMenu( string key )
        {
            var parent = Parent as MainDashboard;
            if ( parent == null ) return;
            parent.ContentPanel.Controls.Clear();

            switch ( key )
            {
                case "Add a new work":
                    new ModifyWorksForm().ShowDialog();
                    break;
                case "View works":
                    new ViewWorks { Parent = parent.ContentPanel };
                    break;
                case "Edit a work":
                    new EditWork {Parent = parent.ContentPanel};
                    break;
                case "Delete a work":
                    new DeleteWork { Parent = parent.ContentPanel };
                    break;
                case "Matching a work":
                    new Matching_a_work().ShowDialog();
                    break;
            }
        }


        #region Main navigation buttons

        private void InitializeAddNewPerson()
        {
            _addNewPerson = new SlmButton
            {
                IconType = CustomFonts.Icon.Plus,
                Text = "Add New Person"
            };
            _addNewPerson.Click += delegate { InitializeAddNewPersonSubMenu(); };

            CreateButton( _addNewPerson );
        }

        private void InitializeAddNewLocation()
        {
            _addNewLocation = new SlmButton
            {
                IconType = CustomFonts.Icon.Plus,
                Text = "Add New Location"
            };
            _addNewLocation.Click += delegate { InitializeAddNewLocationSubMenu(); };

            CreateButton( _addNewLocation );
        }

        private static void CreateButton( SlmButton button )
        {
            button.SlmButtonSize = SlmButton.ButtonSize.Navigation;
            button.BackgroundColor = Palette.Type.Black;
            button.Dock = DockStyle.Top;
        }

        #endregion


        #region Submenu initiliazation

        private void InitializeAddNewPersonSubMenu()
        {
            var parent = ParentForm as MainDashboard;
            if ( parent == null ) return;

            Dictionary<string, SlmButton> subMenuButtons = new Dictionary<string, SlmButton>
            {
                {"warden", new SlmButton {IconType = CustomFonts.Icon.Warden}},
                {"secretary", new SlmButton {IconType = CustomFonts.Icon.Secretary}},
                {"guard", new SlmButton {IconType = CustomFonts.Icon.Guard}},
                {"officer", new SlmButton {IconType = CustomFonts.Icon.Officer}},
                {"inmate", new SlmButton {IconType = CustomFonts.Icon.Inmate}},
                {"visitor", new SlmButton {IconType = CustomFonts.Icon.Visitor}},
                {"maintenance", new SlmButton {IconType = CustomFonts.Icon.Maintenance}},
                {"administrator", new SlmButton {IconType = CustomFonts.Icon.Work}},
                {"medical", new SlmButton {IconType = CustomFonts.Icon.Medical}}
            };
           
            foreach ( var button in subMenuButtons )
            {
                button.Value.Click += delegate { ModifyForm( button.Key ); };
            }

            CreateSubMenu( subMenuButtons, "Add a Person", parent );
        }

        private void ModifyForm( string role )
        {
            switch ( role )
            {
                case "warden":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Warden, new Warden()).ShowDialog();
                    break;
                case "secretary":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Secretary, new Secretary()).ShowDialog();
                    break;
                case "guard":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Guard, new Guard()).ShowDialog();
                    break;
                case "officer":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Officer, new Officer()).ShowDialog();
                    break;
                case "inmate":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Inmate, new Inmate()).ShowDialog();
                    break;
                case "visitor":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Visitor, new Visitor()).ShowDialog();
                    break;
                case "administrator":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Administrator, new Administrator()).ShowDialog();
                    break;
                case "maintenance":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Maintenance, new Maintenance()).ShowDialog();
                    break;
                case "medical":
                    new ModifyPersonFormFactory().Generate(Enums.Role.Medical, new Medical()).ShowDialog();
                    break;
            }
            
        }
        private void InitializeAddNewLocationSubMenu()
        {
            var parent = ParentForm as MainDashboard;
            if ( parent == null ) return;

            Dictionary<string, SlmButton> subMenuButtons = new Dictionary<string, SlmButton>
            {
                {"Building", new SlmButton {IconType = CustomFonts.Icon.Room}},
                {"Warehouse", new SlmButton {IconType = CustomFonts.Icon.Warehouse}},
                {"Office", new SlmButton {IconType = CustomFonts.Icon.Office}},
                {"Block", new SlmButton {IconType = CustomFonts.Icon.Block}},
                {"Cell", new SlmButton {IconType = CustomFonts.Icon.Exclamation}},
                {"SHU", new SlmButton {IconType = CustomFonts.Icon.Danger}},
            };

            CreateSubMenu( subMenuButtons, "Add a Location", parent );
        }

        private void InitializeViewAllPeopleSubMenu()
        {
        }

        private void InitializeViewAllLocationsSubMenu()
        {
        }

        private static void CreateSubMenu( Dictionary<string, SlmButton> buttons, string title, MainDashboard parent )
        {
            if ( parent == null ) return;
            parent.ContentPanel.Controls.Clear();

            SlmIcon exit = new SlmIcon
                                {
                                    IconType = CustomFonts.Icon.X,
                                    InActiveColor = Palette.Type.Grey,
                                    ActiveColor = Palette.Type.Red,
                                    Dock = DockStyle.Right,
                                    Width = 20,
                                    TabStop = false,
                                    // Location = new Point(Parent.Right - 40, Parent.Top + 5)
                                    
                                };
            TableLayoutPanel subMenu = new TableLayoutPanel
            {
                BackColor = Palette.BlackLight,
                AutoSize = true,
                Padding = new Padding( 0 ),
                ColumnStyles =
                {
                    new ColumnStyle( SizeType.Percent, 50F ),
                    new ColumnStyle( SizeType.Percent, 50F )
                },
                RowStyles = {new RowStyle( SizeType.AutoSize )},
                Controls =
                {
                    {
                        new Label
                        {
                            AutoSize = true,
                            Text = title.ToUpper(),
                            TextAlign = ContentAlignment.MiddleCenter,
                            ForeColor = Palette.GreyDark,
                            Dock = DockStyle.Fill,
                            Padding = new Padding( 5, 5, 5, 5 + parent.BorderWidth ),
                            Font = CustomFonts.GetFont( CustomFonts.Size.Block ),
                            Controls = { exit }
                        },
                        0, 0
                    }
                },
                Parent = parent.ContentPanel,
                //Visible = false
            };
            exit.Click += delegate { parent.ContentPanel.Controls.Clear(); };
            subMenu.SetColumnSpan( subMenu.Controls[ 0 ], 2 );
            subMenu.Controls[ 0 ].Paint +=
                ( s, e ) =>
                {
                    Utility.DrawBorder(
                                       Utility.Border.Bottom, parent.BorderWidth,
                                       Palette.ColorFromType( (Palette.Type)parent.SlmModalType ), subMenu.Controls[ 0 ],
                                       e );
                };

            int i = 1;
            foreach ( KeyValuePair<string, SlmButton> button in buttons )
            {                    
                int column = ( i + 1 ) % 2;
                int row = ( i + 1 % buttons.Count ) / 2;

                button.Value.Dock = DockStyle.Fill;
                button.Value.Text = button.Key;
                button.Value.SlmButtonSize = SlmButton.ButtonSize.ExtraLarge;
                button.Value.BackgroundColor = Palette.Type.Black;
                button.Value.Margin = new Padding( column == 0 ? 3 : 2, 2, column == 1 ? 3 : 2, 2 );

                subMenu.Controls.Add( button.Value, column, row );
                i++;
            }

            Utility.CenterControl(subMenu, subMenu.Parent);
            subMenu.Visible = true;
        }

        #endregion


        #region Fields/Properties

        private Panel _banner;
        private Panel _profile;
        private SlmButton _logoutButton;
        private SlmButton _addNewPerson;
        private SlmButton _addNewLocation;
        private SlmButton _viewAllPeople;
        private SlmButton _viewAllLocations;
        private SlmButton _viewMaintenceMenu;

        #endregion
    }
}
