﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Medical;

namespace TheSlammer.Views.Pages
{
    [DesignerCategory( "" )]
    [ToolboxItem( false )]
    public class ModifyMedical : SlmPage
    {
        private CheckedListBox clbExpierense;
        public IMedical FormMedical;

        public ModifyMedical( )
        {
            InitializeComponent();
            DataSource();
        }

        private void DataSource()
        {
            ( (ListBox)clbExpierense ).DataSource = Enum.GetValues( typeof ( Enums.MedicalExperties ) );
            clbExpierense.Height = ( clbExpierense.PreferredHeight + 20 );           
        }

        public override void FillData(IPerson person)
        {
            FormMedical = (IMedical)person;
            ///connect to the current experience data
            for (int Index = 0; Index < clbExpierense.Items.Count; Index++)
            {
                if (Medical.HasExpertise((Enums.MedicalExperties)Math.Pow(2, Index), FormMedical))
                    clbExpierense.SetItemCheckState(Index, CheckState.Checked);
            }
        }

        public override void GetData()
        {

        }
        private void clbExpierense_ItemCheck( object sender, ItemCheckEventArgs e )
        {
            /**
             * The current value is again of the status 
             * if the user checked then the current value is unchecked 
            **/
            if ( e.CurrentValue == CheckState.Checked ) Medical.RemoveExpertise( (Enums.MedicalExperties)Math.Pow( 2, e.Index ), FormMedical );
            else Medical.AddExperties( (Enums.MedicalExperties)Math.Pow( 2, e.Index ), FormMedical );
        }

        private void InitializeComponent()
        {
            clbExpierense = new CheckedListBox();
            SuspendLayout();
            // 
            // clbExpierense
            // 
            clbExpierense.AllowDrop = true;
            clbExpierense.BackColor = SystemColors.Window;
            clbExpierense.CheckOnClick = true;
            clbExpierense.FormattingEnabled = true;
            clbExpierense.Location = new Point( 227, 130 );
            clbExpierense.Margin = new Padding( 3, 4, 3, 4 );
            clbExpierense.Name = "clbExpierense";
            clbExpierense.Size = new Size( 264, 52 );
            clbExpierense.TabIndex = 1;
            clbExpierense.ItemCheck += clbExpierense_ItemCheck;
            // 
            // ModifyMedical
            // 
            AutoScaleDimensions = new SizeF( 7F, 16F );
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add( clbExpierense );
            Margin = new Padding( 3, 4, 3, 4 );
            Name = "ModifyMedical";
            Size = new Size( 817, 615 );
            Title = "MEDICAL EXPEREINCE";
            Controls.SetChildIndex( clbExpierense, 0 );
            ResumeLayout( false );
        }

     
    }
}