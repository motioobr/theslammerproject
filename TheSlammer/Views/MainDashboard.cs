﻿using System.Windows.Forms;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Views.Pages;

namespace TheSlammer.Views
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    internal class MainDashboard : SlmModal {

        internal MainDashboard()
        {
            SlmModalType = ModalType.Orange;
            SlmModalSize = ModalSize.Block;
            Text         = "DASHBOARD";

            Intialize();
        }

        private void Intialize()
        {
            ContentPanel = new Panel
            {
                BackColor = Palette.WhiteLight,
                Dock      = DockStyle.Fill,
                Margin    = new Padding( 0 ),
                Parent    = this
            };

            _mainNavigation = new Navigation { Parent = this };
            _mainNavigation.SendToBack();
        }


        #region Fields/Properties

        private Navigation _mainNavigation;
        public Panel ContentPanel;

        #endregion


    }
}