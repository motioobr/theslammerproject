﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model.Maintenance;

namespace TheSlammer.Views.Forms
{
    class DeleteWork : ViewWorks
    {
        private Panel _footerpanel;

        public DeleteWork()
        {
            InitializeFooter();
            Title = "Delete Work";

            SizeChanged += SetSizeLocation;
        }

        private void SetSizeLocation(object sender, EventArgs e)
        {
            _footerpanel = new Panel
            {
                Size = new Size(1,50),
                Parent = this,
                Dock = DockStyle.Bottom
            };

            _delete.Parent = _footerpanel;
            Utility.HorizontalCenterControl(_delete, _footerpanel);
            Utility.VerticalCenterControl(_delete, _footerpanel);
        }

        private void InitializeFooter()
        {
            

            _delete = new SlmButton
            {
                SlmButtonSize = SlmButton.ButtonSize.ExtraLarge,
                Text = "Delete",
                IconType = CustomFonts.Icon.Edit,
                BackgroundColor = Palette.Type.Yellow,
                BackgroundHoverColor = Palette.Type.Yellow,
            };

            _delete.Click += BtnDelete;

        }

        private void BtnDelete(object sender, EventArgs e)
        {
            if (_grid.GetSelectedRow() == null) return;

            var work = _grid.GetSelectedRow();

            if (work == null) return;

            var administrator = LoggedUser.Instance as Administrator;

            if ( administrator != null && administrator.DeleteWork( _grid.GetSelectedRow()) )
            {
                _grid.RefreshGrid();
                SlmAlert.ShowSuccess( "Deleted success" );
            }
            else
                SlmAlert.ShowError("There was an error while Deleted");
                
        }
    }
}
