﻿namespace TheSlammer.Views.Forms
{
    partial class Matching_a_work
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.jobsLabel = new System.Windows.Forms.Label();
            this.slmIcon1 = new TheSlammer.Assets.UserControls.SlmIcon();
            this.MatchLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.slmComboBox1 = new TheSlammer.Assets.UserControls.SlmComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SaveButton = new TheSlammer.Assets.UserControls.SlmButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slmIcon1)).BeginInit();
            this.SuspendLayout();
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(800, 45);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 655);
            this.Footer.Size = new System.Drawing.Size(800, 45);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.jobsLabel);
            this.panel4.Controls.Add(this.slmIcon1);
            this.panel4.Controls.Add(this.MatchLabel);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.slmComboBox1);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 45);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 192);
            this.panel4.TabIndex = 4;
            // 
            // jobsLabel
            // 
            this.jobsLabel.AutoSize = true;
            this.jobsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.jobsLabel.Location = new System.Drawing.Point(383, 147);
            this.jobsLabel.Name = "jobsLabel";
            this.jobsLabel.Size = new System.Drawing.Size(64, 25);
            this.jobsLabel.TabIndex = 5;
            this.jobsLabel.Text = "label2";
            // 
            // slmIcon1
            // 
            this.slmIcon1.ActiveColor = TheSlammer.Assets.Styles.Palette.Type.Green;
            this.slmIcon1.BackColor = System.Drawing.Color.Transparent;
            this.slmIcon1.IconType = TheSlammer.Assets.Fonts.CustomFonts.Icon.Danger;
            this.slmIcon1.InActiveColor = TheSlammer.Assets.Styles.Palette.Type.Blue;
            this.slmIcon1.Location = new System.Drawing.Point(635, 15);
            this.slmIcon1.Name = "slmIcon1";
            this.slmIcon1.Size = new System.Drawing.Size(34, 36);
            this.slmIcon1.TabIndex = 4;
            this.slmIcon1.TabStop = false;
            // 
            // MatchLabel
            // 
            this.MatchLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MatchLabel.AutoSize = true;
            this.MatchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.MatchLabel.Location = new System.Drawing.Point(370, 108);
            this.MatchLabel.Name = "MatchLabel";
            this.MatchLabel.Size = new System.Drawing.Size(283, 25);
            this.MatchLabel.TabIndex = 3;
            this.MatchLabel.Text = "The jobs of the maintenance is:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(25, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(306, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "Choose a work to match";
            // 
            // slmComboBox1
            // 
            this.slmComboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.slmComboBox1.DataSource = null;
            this.slmComboBox1.Description = "match \"job\" to \"job\"";
            this.slmComboBox1.DisplayMember = "e";
            this.slmComboBox1.Location = new System.Drawing.Point(354, 15);
            this.slmComboBox1.Name = "slmComboBox1";
            this.slmComboBox1.SelectedItem = null;
            this.slmComboBox1.SelectedValue = null;
            this.slmComboBox1.TabIndex = 1;
            this.slmComboBox1.ValueMember = "";
            this.slmComboBox1.SelectedValueChanged += new System.EventHandler(this.MatchJobType);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(23, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(292, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "Choose a Maintenance";
            // 
            // SaveButton
            // 
            this.SaveButton.BackgroundColor = TheSlammer.Assets.Styles.Palette.Type.Turquoise;
            this.SaveButton.BackgroundHoverColor = TheSlammer.Assets.Styles.Palette.Type.Turquoise;
            this.SaveButton.IconPosition = TheSlammer.Assets.UserControls.SlmButton.IconLocation.Left;
            this.SaveButton.IconType = TheSlammer.Assets.Fonts.CustomFonts.Icon.ArrowRight;
            this.SaveButton.Location = new System.Drawing.Point(606, 593);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.SlmButtonSize = TheSlammer.Assets.UserControls.SlmButton.ButtonSize.ExtraLarge;
            this.SaveButton.TabIndex = 6;
            this.SaveButton.TextColor = TheSlammer.Assets.Styles.Palette.Type.White;
            this.SaveButton.TextHoverColor = TheSlammer.Assets.Styles.Palette.Type.Purple;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 237);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(800, 370);
            this.panel3.TabIndex = 7;
            // 
            // Matching_a_work
            // 
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.panel4);
            this.Name = "Matching_a_work";
            this.SlmModalSize = TheSlammer.Assets.UserControls.SlmModal.ModalSize.ExtraLarge;
            this.Text = "SELECT A NEW MATCHING";
            this.Controls.SetChildIndex(this.Footer, 0);
            this.Controls.SetChildIndex(this.Header, 0);
            this.Controls.SetChildIndex(this.panel4, 0);
            this.Controls.SetChildIndex(this.SaveButton, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slmIcon1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private Assets.UserControls.SlmButton SAVE;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel4;
        private Assets.UserControls.SlmButton SaveButton;
        private System.Windows.Forms.Label label4;
        private Assets.UserControls.SlmComboBox slmComboBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label MatchLabel;
        private Assets.UserControls.SlmIcon slmIcon1;
        private System.Windows.Forms.Label jobsLabel;
    }
}