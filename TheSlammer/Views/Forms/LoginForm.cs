﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Properties;

namespace TheSlammer.Views.Forms
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    public class LoginForm : SlmModal
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            SlmModalType = ModalType.Orange;
            Text = "LOGIN";
            SlmModalSize = ModalSize.Small;

            _banner = new PictureBox
            {
                BackColor = Color.FromArgb( 153, 114, 181 ),
                Dock = DockStyle.Top,
                Image = Resources.LogoBanner,
                Size = new Size( Width, 75 ),
                SizeMode = PictureBoxSizeMode.Zoom,
                TabStop = false,
                Parent = this
            };

            _txtUsername = new SlmTextBoxPlus
            {
                Location = new Point( 0, _banner.Bottom + 30 ),
                Description = null,
                TabIndex = 1,
                TextBoxValidation = SlmTextBoxPlus.TextBoxType.AlphaNumeric,
                Watermark = "Username",
                Parent = this
            };

            _txtPassword = new SlmTextBoxPlus
            {
                Location = new Point( 0, _txtUsername.Bottom + 30 ),
                Description = null,
                TabIndex = 2,
                TextBoxValidation = SlmTextBoxPlus.TextBoxType.AlphaNumeric,
                UsePasswordChar = true,
                Watermark = "Password",
                Parent = this
            };

            _btnLogin = new SlmButton
            {
                BackgroundColor = Palette.Type.Purple,
                BackgroundHoverColor = Palette.Type.Purple,
                IconPosition = SlmButton.IconLocation.None,
                SlmButtonSize = SlmButton.ButtonSize.Large,
                TabIndex = 3,
                TextColor = Palette.Type.White,
                TextHoverColor = Palette.Type.White,
                Text = "Login",
                Parent = Footer
            };
            _btnLogin.Click += Login;

            Utility.HorizontalCenterControl( _txtUsername, this );
            Utility.HorizontalCenterControl( _txtPassword, this );
            Utility.CenterControl( _btnLogin, Footer );
        }

        private void Login( object sender, EventArgs e )
        {
            if ( LoggedUser.Authenticate( _txtUsername.Text, _txtPassword.Text ) )
            {
                SlmAlert.ShowSuccess(
                                     "Login Successful", 
                                     string.Format( "Welcome back: {0}.", LoggedUser.Instance.Name ) 
                                     );
                Close();
            }
            else SlmAlert.ShowWarning( "Login Error", "Username & password do not match. Please try again." );
        }


        #region Fields

        private PictureBox _banner;
        private SlmButton _btnLogin;
        private SlmTextBoxPlus _txtPassword;
        private SlmTextBoxPlus _txtUsername;

        #endregion
    }
}