﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Data;
using TheSlammer.Model.Maintenance;
using TheSlammer.Views.GridViews;

namespace TheSlammer.Views.Forms
{
    public partial class Matching_a_work : SlmModal
    {
        private DataGridView _workView;

        public Matching_a_work()
        {
            InitializeComponent();

            InitializeWorkView();

            SaveButton.Text = "Save";
            slmComboBox1.DataSource = ContextInstance.Data.Maintenance.ToList();
            slmComboBox1.DisplayMember = "Name";
            _workView.Columns["WorkID"].Visible =
            _workView.Columns["Worker"].Visible =
            _workView.Columns["WorkerID"].Visible = false;
        }

        private void InitializeWorkView()
        {
            _workView = new DataGridView
            {               
                BorderStyle = BorderStyle.None,
                Font = CustomFonts.GetFont(CustomFonts.Size.Medium),
                ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize,
                ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None,
                AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill,
                AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells,
                SelectionMode = DataGridViewSelectionMode.FullRowSelect,
                ScrollBars = ScrollBars.Both,
                Dock = DockStyle.Fill,
                ReadOnly = true,
                MultiSelect = false,
                DataSource = ContextInstance.Data.Works.Where(m => m.WorkerID == null).ToList(),
                Parent = panel3,
            };
            _workView.SelectionChanged += MatchJobType;

        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if ( !Matched() )
            {
                SlmAlert.ShowError( "The Maintenance not match the work" );
                return;
            }

            var work = _workView.SelectedRows[0].DataBoundItem as Work;       
            var administrator = LoggedUser.Instance as Administrator;

            if (work != null && administrator != null)
            {
                work.Worker = (Maintenance)slmComboBox1.SelectedItem;
                administrator.UpdateWork(work);
            }
            else
                SlmAlert.ShowError("An error occurred while saving, you are not permitted to assign");

            Close();
        }

        private void MatchJobType(object sender, EventArgs e)
        {

            if ( Matched() )
            {
                slmIcon1.IconType = CustomFonts.Icon.Success;
                slmIcon1.ActiveColor = slmIcon1.InActiveColor = Palette.Type.Green;
            }
            else
            {
                slmIcon1.IconType = CustomFonts.Icon.Danger;
                slmIcon1.ActiveColor = slmIcon1.InActiveColor = Palette.Type.Red;
            }
            jobsLabel.Text = "" +( (Maintenance)slmComboBox1.SelectedItem ).Job;
        }

        private bool Matched()
        {
            return slmComboBox1.SelectedItem != null && _workView.SelectedRows.Count != 0 &&
                   ( (Maintenance)slmComboBox1.SelectedItem ).HasExpertise(
                                                                           ( (Work)
                                                                             _workView.SelectedRows[ 0 ].DataBoundItem )
                                                                               .Job );
        }
    }
}   
