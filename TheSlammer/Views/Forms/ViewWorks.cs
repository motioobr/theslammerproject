﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Maintenance;
using TheSlammer.Views.GridViews;

namespace TheSlammer.Views.Forms
{
    internal class ViewWorks : SlmPage
    {
        internal ViewWorks()
        {
            Title = "View Works";

            InitializeOptionHeader();
            InitializeGrid();
            
            SizeChanged += SetSizeLocation;
        }

        private void SetSizeLocation(object sender, EventArgs e)
        {
           
            const int margin = 20;

            _grid.BringToFront();

            
            _searchByWorkType.Location = new Point(_optionHeader.Left + 4 * margin + _refresh.Width, 0);
            Utility.VerticalCenterControl(_searchByWorkType, _optionHeader);

            _searchByName.Location = new Point(_searchByWorkType.Right + margin, _searchByWorkType.Top);
            Utility.VerticalCenterControl(_searchByName, _optionHeader);

            _searchByDate.Location = new Point(_searchByName.Right + margin, _searchByWorkType.Top);

            _search.Location = new Point(_searchByDate.Right + margin / 2, _searchByWorkType.Top + 5);

            _refresh.Location = new Point(margin, _search.Location.Y);

        }

        private void InitializeGrid()
        {
            _grid = new WorkGridView ();
            _grid.Parent = this;
            _grid.Dock = DockStyle.Fill;

            RefreshGrid();
        }

        private void InitializeOptionHeader()
        {
            _optionHeader = new Panel
            {
                BackColor = Palette.GreyDark,
                Dock = DockStyle.Top,
                Parent = this
            };

            _searchByWorkType = new SlmComboBox
            {
                Description = "View by job",
                DataSource = Enum.GetValues(typeof(Enums.MaintenanceType)),
                SelectedItem = null,
                Parent = _optionHeader
            };
            _searchByWorkType.SelectedValueChanged += delegate { ViewByWorktype(); };

            _searchByName = new SlmComboBox
            {
                Description = "View by name",
                DataSource = ContextInstance.Data.Maintenance.ToList(),
                DisplayMember = "Name",
                SelectedItem = null,
                Parent = _optionHeader
            };
            _searchByName.SelectedValueChanged += delegate { ViewByName(); };

            _searchByDate = new SlmDateBox
            {
                ShowCheckBox = false,
                Description = "View by date",                
                Parent = _optionHeader
            };

            _search = new SlmIcon
            {
                Size = new Size(_searchByDate.Height - 20, _searchByDate.Height - 20),
                InActiveColor = Palette.Type.White,
                ActiveColor = Palette.Type.Green,
                IconType = CustomFonts.Icon.Search,
                Parent = _optionHeader
            };
            _search.Click += delegate { ViewByDate(); };

            _refresh = new SlmIcon
            {
                Size = _search.Size,
                InActiveColor = Palette.Type.White,
                ActiveColor = Palette.Type.Green,
                IconType = CustomFonts.Icon.Maintenance,
                Parent = _optionHeader
            };
            _refresh.Click += delegate { RefreshGrid(); };
        }

        


        #region Buttons

        private void ViewByName()
        {
            if ( _searchByName.SelectedItem == null ) 
                return;             
                               
            _grid.FilterByName( (Maintenance)_searchByName.SelectedItem);
            //_searchByWorkType.SelectedItem = null; 
        }

        private void ViewByDate()
        {
            //_searchByWorkType.SelectedItem = null;
            //_searchByName.SelectedItem = null;
            _grid.FilterByDate( _searchByDate.Value );
        }

        private void ViewByWorktype()
        {
            if ( _searchByWorkType.SelectedItem == null ) return;
                      
            _grid.FilterByJob((Enums.MaintenanceType)_searchByWorkType.SelectedItem);
           // _searchByName.SelectedItem = null;
        }

        private void RefreshGrid()
        {            
            _grid.Focus();
            
            _searchByWorkType.SelectedItem = null;
            _searchByName.SelectedItem = null;
            _grid.RefreshGrid();
        }
       

        #endregion


        #region Fields

        protected WorkGridView _grid;

        private Panel _optionHeader;
        private SlmComboBox _searchByWorkType;
        private SlmComboBox _searchByName;
        private SlmDateBox _searchByDate;
        private SlmIcon _search;
        private SlmIcon _refresh;

        protected SlmButton _edit;
        protected SlmButton _delete;
        private SlmButton _cancel;
        

        #endregion


        public override void FillData( IPerson person )
        {
            throw new NotImplementedException();
        }

        public override void GetData()
        {
            throw new NotImplementedException();
        }
    }
}
