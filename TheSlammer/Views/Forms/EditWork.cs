﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Views.ModifyForms.Works;

namespace TheSlammer.Views.Forms
{
    class EditWork : ViewWorks
    {
        private Panel _footerpanel;

        public EditWork()
        {
            InitializeFooter();
            Title = "edit Work";

            SizeChanged += SetSizeLocation;
        }
        
        private void SetSizeLocation(object sender, EventArgs e)
        {
            _footerpanel = new Panel
            {
                Size = new Size(1,50),
                Parent = this,
                Dock = DockStyle.Bottom
            };

            _edit.Parent = _footerpanel;
            Utility.HorizontalCenterControl(_edit, _footerpanel);
            Utility.VerticalCenterControl(_edit, _footerpanel);
        }
        
        private void InitializeFooter()
        {


            _edit = new SlmButton
            {
                SlmButtonSize = SlmButton.ButtonSize.ExtraLarge,
                Text = "Edit",
                IconType = CustomFonts.Icon.Edit,
                BackgroundColor = Palette.Type.Yellow,
                BackgroundHoverColor = Palette.Type.Yellow,
                
            };
            
            _edit.Click += EditClicked;
            
        }

        private void EditClicked(object sender, EventArgs e)
        {
            if (_grid.GetSelectedRow() == null) return;

            var work = _grid.GetSelectedRow();

            if (work == null) return;

            DialogResult result = new ModifyWorksForm
            {
                WorkType = work.WorkType,
                Job = work.Job,
                WorkID = work.WorkID,
                WorkDate = work.WorkDate,
                WorkerID = work.WorkerID,
                Worker = work.Worker,
            }.ShowDialog();
            if (result != DialogResult.OK) _grid.RefreshGrid();
        }
    }
}
