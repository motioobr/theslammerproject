﻿using System;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Management;
using TheSlammer.Model.Security;
using TheSlammer.Views.ModifyForms.Locations;

namespace TheSlammer.Views.ModifyForms
{
    internal class ModifyLocationsFormFactory : IFactory<Enums.Location, SlmModal, ILocation>
    {
        public SlmModal Generate( Enums.Location location, ILocation objectType = null )
        {
            switch ( location )
            {
                case Enums.Location.Building :
                    objectType = objectType ?? new Building();
                    return new ModifyBuildingForm( (IBuilding)objectType );

                case Enums.Location.Block :
                    objectType = objectType ?? new Block();
                    return new ModifyBlockForm( (IBlock)objectType );

                case Enums.Location.Cell :
                    objectType = objectType ?? new Cell();
                    return new ModifyCellForm( (ICell)objectType );

                case Enums.Location.Office :
                    objectType = objectType ?? new Office();
                    return new ModifyOfficeForm( (IOffice)objectType );

                case Enums.Location.Warehouse :
                    objectType = objectType ?? new Warehouse();
                    return new ModifyWarehouseForm( (IWarehouse)objectType );

                default:
                    throw new ArgumentOutOfRangeException( "location" );
            }
        }
    }
}