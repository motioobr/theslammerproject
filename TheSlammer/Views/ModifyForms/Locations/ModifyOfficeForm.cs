﻿using System;
using System.Drawing;
using System.Linq;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Management;

namespace TheSlammer.Views.ModifyForms.Locations
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    internal class ModifyOfficeForm : SlmModal
    {
        internal ModifyOfficeForm( IOffice office )
        {
            _office = office;

            SlmModalType = ModalType.Purple;
            Text = "Modify Office";

            SlmIcon icon = new SlmIcon
            {
                IconType = CustomFonts.Icon.Office,
                Size = new Size( 120, 120 ),
                Location = new Point( 0, AccessHeight + 20 ),
                ActiveColor = Palette.Type.Purple,
                InActiveColor = Palette.Type.Purple,
                Parent = this
            };

            _building = new SlmComboBox
            {
                Description = "Choose a building",
                ValueMember = "BuidlingID",
                DisplayMember = "Name",
                DataSource = ContextInstance.Data.Buildings.ToList(),
                SelectedItem = _office.BuildingID,
                Location = new Point( 0, icon.Bottom + 10 ),
                Parent = this
            };

            SlmButton submit = new SlmButton
            {
                Text = "Submit",
                SlmButtonSize = SlmButton.ButtonSize.Medium,
                BackgroundColor = Palette.Type.Purple,
                BackgroundHoverColor = Palette.Type.Purple,
                Location = new Point( 0, _building.Bottom + 10 ),
                Parent = Footer
            };
            submit.Click += Submit;

            Utility.HorizontalCenterControl( icon, this );
            Utility.HorizontalCenterControl( _building, this );
            Utility.CenterControl( submit, Footer );
        }

        private void Submit( object sender, EventArgs e )
        {
            _office.Building = (Building)_building.SelectedItem;

            var warden = LoggedUser.Instance as Warden;
            if ( warden == null ) return;

            if ( warden.CreateOffice( _office ) )
            {
                SlmAlert.ShowSuccess( "Office was successfully saved" );
                Close();
            }
            else SlmAlert.ShowError( "There was an error while trying to save your office" );
        }


        #region Fields

        private readonly IOffice _office;
        private readonly SlmComboBox _building;

        #endregion
    }
}