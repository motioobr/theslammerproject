﻿using System;
using System.Drawing;
using System.Linq;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Management;
using TheSlammer.Model.Security;

namespace TheSlammer.Views.ModifyForms.Locations
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    internal class ModifyBlockForm : SlmModal
    {
        internal ModifyBlockForm( IBlock block )
        {
            _block = block;

            SlmModalType = ModalType.Purple;
            Text = "Modify Block";

            SlmIcon icon = new SlmIcon
            {
                IconType = CustomFonts.Icon.Block,
                Size = new Size( 100, 100 ),
                Location = new Point( 0, AccessHeight + 5 ),
                ActiveColor = Palette.Type.Purple,
                InActiveColor = Palette.Type.Purple,
                Parent = this
            };

            _buildingList = new SlmComboBox
            {
                Description = "Choose a Building",
                ValueMember = "BuildingID",
                DisplayMember = "Name",
                DataSource = ContextInstance.Data.Buildings.OrderBy( b => b.Name ).ToList(),
                SelectedItem = _block.Building,
                Location = new Point( 0, icon.Bottom + 5 ),
                Parent = this
            };

            _officerList = new SlmComboBox
            {
                Description = "Assign an officer",
                ValueMember = "PersonID",
                DisplayMember = "Name",
                DataSource = ContextInstance.Data.Officers.OrderBy( o => o.FirstName ).ToList(),
                SelectedItem = _block.Officer,
                Location = new Point( 0, _buildingList.Bottom + 10 ),
                Parent = this
            };

            SlmButton submit = new SlmButton
            {
                Text = "Submit",
                SlmButtonSize = SlmButton.ButtonSize.Medium,
                BackgroundColor = Palette.Type.Purple,
                BackgroundHoverColor = Palette.Type.Purple,
                Location = new Point( 0, _buildingList.Bottom + 10 ),
                Parent = Footer
            };
            submit.Click += Submit;

            Utility.HorizontalCenterControl( icon, this );
            Utility.HorizontalCenterControl( _buildingList, this );
            Utility.HorizontalCenterControl( _officerList, this );
            Utility.CenterControl( submit, Footer );
        }

        private void Submit( object sender, EventArgs e )
        {
            _block.Building = (Building)_buildingList.SelectedItem;
            _block.Officer = (Officer)_officerList.SelectedItem;
            
            var warden = LoggedUser.Instance as Warden;
            if ( warden == null ) return;

            if ( warden.CreateBlock( _block ) )
            {
                SlmAlert.ShowSuccess( "Block was successfully saved" );
                Close();
            }
            else SlmAlert.ShowError( "There was an error while trying to save the Block" );
        }


        #region Fields

        private IBlock _block;
        private readonly SlmComboBox _buildingList;
        private readonly SlmComboBox _officerList;

        #endregion
    }
}