using System;
using System.Drawing;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;

namespace TheSlammer.Views.ModifyForms.Locations
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    internal class ModifyBuildingForm : SlmModal
    {
        internal ModifyBuildingForm( IBuilding building )
        {
            _building = building;

            SlmModalType = ModalType.Purple;
            Text = "Modify Building";
            
            SlmIcon icon = new SlmIcon
            {
                IconType = CustomFonts.Icon.Room,
                Size = new Size( 120, 120 ),
                Location = new Point( 0, AccessHeight + 20 ),
                ActiveColor = Palette.Type.Purple,
                InActiveColor = Palette.Type.Purple,
                Parent = this
            };
            
            _name = new SlmTextBoxPlus
            {
                Text = _building.Name,
                Watermark = "Building name",
                Description = "3-30 characters",
                Location = new Point( 0, icon.Bottom + 10 ),
                Parent = this
            };

            SlmButton submit = new SlmButton
            {
                Text = "Submit",
                SlmButtonSize = SlmButton.ButtonSize.Medium,
                BackgroundColor = Palette.Type.Purple,
                BackgroundHoverColor = Palette.Type.Purple,
                Parent = Footer
            };
            submit.Click += Submit;

            Utility.HorizontalCenterControl( _name, this );
            Utility.HorizontalCenterControl( icon, this );
            Utility.CenterControl( submit, Footer );
        }

        private bool ValidateFields()
        {
            if ( !Validation.HasLength( _name.Text, 3, 30 ) )
            {
                _name.SetError( "The string must be between 3-30 characters" );
                return false;
            }

            if ( Validation.BuildingNameExists( _name.Text, _building.BuildingID ) )
            {
                _name.SetError( "Building name already exists" );
                return false;
            }

            _name.SetSuccess();
            return true;
        }

        private void Submit( object sender, EventArgs e )
        {
            if ( !ValidateFields() ) return;

            _building.Name = _name.Text;

            var warden = LoggedUser.Instance as Warden;
            if ( warden == null ) return;

            if ( warden.CreateBuilding( _building ) )
            {
                SlmAlert.ShowSuccess( "Building was successfully saved" );
                Close();
            }
            else SlmAlert.ShowError( "There was an error while trying to save your building" );
        }


        #region Fields

        private SlmTextBoxPlus _name;
        private IBuilding _building;

        #endregion
    }
}