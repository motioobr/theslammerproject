﻿using System;
using System.Drawing;
using System.Linq;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Model.Security;

namespace TheSlammer.Views.ModifyForms.Locations
{
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    internal class ModifyCellForm : SlmModal
    {
        internal ModifyCellForm( ICell cell )
        {
            _cell = cell;

            SlmModalType = ModalType.Purple;
            Text = "Modify Cell";

            SlmIcon icon = new SlmIcon
            {
                IconType = CustomFonts.Icon.Danger,
                Size = new Size( 100, 100 ),
                Location = new Point( 0, AccessHeight + 5 ),
                ActiveColor = Palette.Type.Purple,
                InActiveColor = Palette.Type.Purple,
                Parent = this
            };

            _blockList = new SlmComboBox
            {
                Description = "Choose a Block",
                ValueMember = "BlockID",
                DisplayMember = "Name",
                DataSource = ContextInstance.Data.Blocks.ToList().OrderBy( b => b.Name ).ToList(),
                SelectedItem = _cell.Block,
                Location = new Point( 0, icon.Bottom + 5 ),
                Parent = this
            };

            _guardList = new SlmComboBox
            {
                Description = "Assign Guard",
                ValueMember = "PersonID",
                DisplayMember = "Name",
                SelectedItem = _cell.Guard,
                DataSource = ContextInstance.Data.Guards.OrderBy( p => p.FirstName ).ToList(),
                Location = new Point( 0, _blockList.Bottom + 10 ),
                Parent = this
            };

            SlmButton submit = new SlmButton
            {
                Text = "Submit",
                SlmButtonSize = SlmButton.ButtonSize.Medium,
                BackgroundColor = Palette.Type.Purple,
                BackgroundHoverColor = Palette.Type.Purple,
                Location = new Point( 0, 0 ),
                Parent = Footer
            };
            submit.Click += Submit;

            Utility.HorizontalCenterControl( icon, this );
            Utility.HorizontalCenterControl( _blockList, this );
            Utility.HorizontalCenterControl( _guardList, this );
            Utility.CenterControl( submit, Footer );
        }

        private void Submit( object sender, EventArgs e )
        {
            _cell.Block = (Block)_blockList.SelectedItem;
            _cell.Guard = (Guard)_guardList.SelectedItem;

            var warden = LoggedUser.Instance as Warden;

            if ( warden == null ) return;
            if ( warden.CreateCell( _cell ) )
            {
                SlmAlert.ShowSuccess( "Cell was successfully saved" );
                Close();
            }
            else SlmAlert.ShowError( "There was an error while trying to save the cell" );
        }


        #region Fields

        private ICell _cell;
        private SlmComboBox _blockList;
        private SlmComboBox _guardList;

        #endregion
    }
}