﻿using System;
using System.Drawing;
using System.Linq;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Locations;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Management;

namespace TheSlammer.Views.ModifyForms.Locations
{
    [Authorization( Level.Warden )]
    // ReSharper disable once RedundantNameQualifier
    [System.ComponentModel.DesignerCategory( "" )]
    internal class ModifyWarehouseForm : SlmModal
    {
        internal ModifyWarehouseForm( IWarehouse warehouse )
        {
            _warehouse = warehouse;

            SlmModalType = ModalType.Purple;
            Text = "Modify Warehouse";

            SlmIcon icon = new SlmIcon
            {
                IconType = CustomFonts.Icon.Warehouse,
                Size = new Size( 100, 100 ),
                Location = new Point( 0, AccessHeight + 5 ),
                ActiveColor = Palette.Type.Purple,
                InActiveColor = Palette.Type.Purple,
                Parent = this
            };

            _building = new SlmComboBox
            {
                Description = "Choose a building",
                ValueMember = "BuidlingID",
                DisplayMember = "Name",
                DataSource = ContextInstance
                    .Data
                    .Buildings
                    .Where( b => !b.Rooms.Any( r => r is Warehouse && r.RoomID != _warehouse.RoomID ) )
                    .ToList(),
                SelectedItem = _warehouse.Building,
                Location = new Point( 0, icon.Bottom + 5 ),
                Parent = this
            };

            _manager = new SlmComboBox
            {
                Description = "Choose a manager",
                ValueMember = "PersonID",
                DisplayMember = "Name",
                DataSource = ContextInstance.Data.Maintenance.ToList(),
                //SelectedItem = _warehouse.Manager,
                Location = new Point( 0, _building.Bottom + 10 ),
                Parent = this
            };

            SlmButton submit = new SlmButton
            {
                Text = "Submit",
                SlmButtonSize = SlmButton.ButtonSize.Medium,
                BackgroundColor = Palette.Type.Purple,
                BackgroundHoverColor = Palette.Type.Purple,
                Parent = Footer
            };
            submit.Click += Submit;

            Utility.HorizontalCenterControl( icon, this );
            Utility.HorizontalCenterControl( _building, this );
            Utility.HorizontalCenterControl( _manager, this );
            Utility.CenterControl( submit, Footer );
        }

        private void Submit( object sender, EventArgs e )
        {
            _warehouse.Building = (Building)_building.SelectedItem;
            //_warehouse.Manager = (Maintenance)_manager.SelectedItem;

            var warden = LoggedUser.Instance as Warden;
            if ( warden == null ) return;

            if ( warden.CreateWarehouse( _warehouse ) )
            {
                SlmAlert.ShowSuccess( "Warehouse was successfully saved" );
                Close();
            }
            else SlmAlert.ShowError( "There was an error while trying to save your warehouse" );
        }


        #region Fields

        private readonly SlmComboBox _manager;
        private readonly SlmComboBox _building;
        private readonly IWarehouse _warehouse;

        #endregion
    }
}