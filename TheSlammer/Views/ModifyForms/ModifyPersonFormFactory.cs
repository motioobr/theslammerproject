﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Views.Forms;
using TheSlammer.Views.Pages;

namespace TheSlammer.Views.ModifyForms
{
    internal class ModifyPersonFormFactory : IFactory<Enums.Role, SlmModal, IPerson>
    {
        public SlmModal Generate( Enums.Role role, IPerson person )
        {
            switch ( role )
            {
                case Enums.Role.Warden :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyUser()
                    }, role );

                case Enums.Role.Secretary :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyUser()
                    }, role );

                case Enums.Role.Officer :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyUser()
                    }, role );

                case Enums.Role.Guard :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyUser()
                    }, role );

                case Enums.Role.Medical :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyUser(),
                        new ModifyMedical()
                    }, role );

                case Enums.Role.Administrator:
                    return new SlmMultiPage(person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyUser(),
                    }, role);

                case Enums.Role.Maintenance :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyUser(),
                        new ModifyMaintenance(  )
                    }, role );

                case Enums.Role.Inmate :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson(),
                        new ModifyInmate()
                    }, role );

                case Enums.Role.Visitor :
                    return new SlmMultiPage( person, new List<UserControl>
                    {
                        new ModifyPerson()
                    }, role );
            }

            throw new NotImplementedException();
        }
    }
}