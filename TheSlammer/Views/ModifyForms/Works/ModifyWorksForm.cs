﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Assets.Styles;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Maintenance;

namespace TheSlammer.Views.ModifyForms.Works
{
    class ModifyWorksForm : SlmModal , IWork
    {
        public ModifyWorksForm()
        {
            Intialize();
        }

        private void Intialize()
        {
            SlmModalType = ModalType.Turquoise;
            SlmModalSize = ModalSize.ExtraLarge;

            _job = new SlmComboBox
            {
                Description = "Choose the maintenance job type of this work",
                DataSource = Enum.GetValues( typeof ( Enums.MaintenanceType ) ),
                Parent = this
            };

            _workType = new SlmComboBox
            {
                Description = "Type of job done by a maintenance worker",
                DataSource = Enum.GetValues( typeof ( Enums.WorkType ) ),
                Parent = this
            };

            _workDate = new SlmDateBox
            {
                Description = "Date of the work",
                Parent = this,                
            };

            _isComplete = new CheckBox
            {
                Text = @"Check if the work is complete",
                Font = CustomFonts.GetFont( CustomFonts.Size.Large ),
                Parent = this,
                Size = new Size(500,_job.Size.Height),
            };

            _btnSave = new SlmButton
            {
                Dock = DockStyle.Right,
                IconType = CustomFonts.Icon.Success,
                Text = "Save",
                SlmButtonSize = SlmButton.ButtonSize.Large,
                BackgroundColor = Palette.Type.Green,
                BackgroundHoverColor = Palette.Type.Green,
                Parent = Footer
            };

            _btnCancel = new SlmButton
            {
                Dock = DockStyle.Right,
                IconType = CustomFonts.Icon.Danger,
                Text = "Cancel",
                SlmButtonSize = SlmButton.ButtonSize.Large,
                BackgroundColor = Palette.Type.Red,
                BackgroundHoverColor = Palette.Type.Red,
                Parent = Footer
            };

            _btnCancel.Click +=
                delegate
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                };

            _btnSave.Click += delegate { Save(); };

            IntializeLocation();
        }

        private void IntializeLocation()
        {
            _job.Location = new Point(50, Header.Bottom + 50);
            _workType.Location = new Point(_job.Right + 50, Header.Bottom + 50);
            _workDate.Location = new Point(50, _job.Bottom + 50);
            _isComplete.Location = new Point(_job.Right + 50, _workDate.Location.Y);
        }

        private void Save()
        {
            var administrator = LoggedUser.Instance as Administrator;

            Work _new = new Work
            {
                Job = Job,
                WorkDate = WorkDate,
                IsCompleted = IsCompleted,
                Worker = Worker,
                WorkerID = WorkerID,
                WorkID = WorkID,
                WorkType = WorkType
            };

            if (administrator != null && WorkID == 0 && administrator.CreateWork(_new)) SlmAlert.ShowSuccess("Work was saved");
            else if (administrator != null && WorkID != 0 && administrator.UpdateWork(_new)) SlmAlert.ShowSuccess("Work was updated");
            else SlmAlert.ShowError("There was an error while saving");

            Close();
        }
        

        private SlmComboBox _job;
        private SlmComboBox _workType;
        private SlmDateBox _workDate;
        private CheckBox _isComplete;

        private SlmButton _btnSave;
        private SlmButton _btnCancel;




        public int WorkID { get; set; }
        public Enums.MaintenanceType Job 
        { 
            get { return (Enums.MaintenanceType)_job.SelectedItem; }
            set { _job.SelectedItem = value; } 
        }

        public DateTime? WorkDate
        { 
            get
            {
                if ( _workDate.Checked ) return _workDate.Value;
                return null;
            }
            set
            {
                if ( value == null ) _workDate.Checked = false;
                else
                {
                    _workDate.Checked = true;
                    _workDate.Value = (DateTime)value;
                }
            }
        }
        public bool IsCompleted { get { return _isComplete.Checked; } set { _isComplete.Checked = value; } }
        public int? WorkerID { get; set; }
        public virtual Maintenance Worker { get; set; }
        public Enums.WorkType WorkType { get; set; }
    }
}
