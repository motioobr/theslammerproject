﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheSlammer
{
    interface IGridView
    {
        System.Windows.Forms.DataGridView GridTable();
    }
}
