﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TheSlammer.Model;
using TheSlammer.Model.Data;
using TheSlammer.Model.Interfaces;
using TheSlammer.Model.Management;
using TheSlammer.Model.People;

namespace TheSlammer.Views.GridViews
{
    internal class PersonGridViewFactory : IFactory<Enums.Role, DataGridView, IPerson>
    {
        public DataGridView Generate( Enums.Role enumChoice, IPerson interfaceType = null )
        {
            DataGridView dataGridView = new DataGridView();
            BindingSource binsource = new BindingSource();

            switch ( enumChoice )
            {
                case Enums.Role.Warden :
                    binsource.DataSource = ContextInstance.Data.Wardens.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;
                
                case Enums.Role.Secretary:
                    binsource.DataSource = ContextInstance.Data.Secretaries.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;

                case Enums.Role.Officer:
                    binsource.DataSource = ContextInstance.Data.Officers.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;

                case Enums.Role.Guard:
                    binsource.DataSource = ContextInstance.Data.Guards.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;

                case Enums.Role.Medical:
                    binsource.DataSource = ContextInstance.Data.Medical.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;
                
                case Enums.Role.Maintenance:
                    binsource.DataSource = ContextInstance.Data.Maintenance.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;
                
                case Enums.Role.Inmate:
                    binsource.DataSource = ContextInstance.Data.Inmates.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;

                case Enums.Role.Administrator:
                    binsource.DataSource = ContextInstance.Data.Administrators.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;

                case Enums.Role.Visitor:
                    binsource.DataSource = ContextInstance.Data.Visitors.ToList();
                    dataGridView.DataSource = binsource.DataSource;
                    return dataGridView;
                
                default :
                    return null;
            }
        }
    }
}