﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TheSlammer.Assets.Fonts;
using TheSlammer.Model;
using TheSlammer.Model.Data;
using TheSlammer.Model.Maintenance;

namespace TheSlammer.Views.GridViews
{
    class WorkGridView : UserControl
    {
        private DataGridView _dataGridView;
        
        public WorkGridView()
        {                           
            SetDataGrid();            
        }

        private void SetDataGrid()
        {

           _dataGridView = new DataGridView
           {
               BorderStyle = BorderStyle.None,
               Font = CustomFonts.GetFont(CustomFonts.Size.Medium),
               ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize,
               ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None,
               AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill,
               AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells,
               SelectionMode = DataGridViewSelectionMode.FullRowSelect,
               ScrollBars = ScrollBars.Both,
               Dock = DockStyle.Fill,
               ReadOnly = true,
               MultiSelect = false,
               DataSource = ContextInstance.Data.Works.ToList(),
               Parent = this
               
           };

           ClientSize = new Size(522, 323);
           AutoScaleDimensions = new SizeF(6F, 13F);
           AutoScaleMode = AutoScaleMode.Font;

           Text = @"Work View";
           
           _dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
           _dataGridView.MultiSelect = false;
 
        }

        public void FilterByDate(DateTime workDate)
        {
            _dataGridView.DataSource = ContextInstance.Data.Works.Where( w => w.WorkDate == workDate.Date ).ToList();
        }

        public void FilterByJob(Enums.MaintenanceType job)
        {
            _dataGridView.DataSource = ContextInstance.Data.Works.Where(w => w.Job == job).ToList();
        }

        public void FilterByName(Maintenance maintenance)
        {
            _dataGridView.DataSource = ContextInstance.Data.Works.Where( w  =>  w.Worker.PersonID == maintenance.PersonID ).ToList();
        }

        public Work GetSelectedRow()
        {
            int index = _dataGridView.SelectedRows[ 0 ].Index;
            return ( (Work)_dataGridView.Rows[ index ].DataBoundItem );
        }

        public void RefreshGrid()
        {
            _dataGridView.DataSource = ContextInstance.Data.Works.ToList();
        }
    }
}
