﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TheSlammer.Assets.UserControls;
using TheSlammer.Model;
using TheSlammer.Model.Inmate;
using TheSlammer.Model.Maintenance;
using TheSlammer.Model.Management;
using TheSlammer.Model.Medical;
using TheSlammer.Model.People;
using TheSlammer.Model.Security;
using TheSlammer.Model.Visit;
using TheSlammer.Views.ModifyForms;

namespace TheSlammer.Views.GridViews
{
    public class PersonGridView : SlmModal {
        
        private TableLayoutPanel _gridViewOptions;
        private TableLayoutPanel _leftOptions;
        private Button _edit;
        private Button _delete;
        private TableLayoutPanel _rightOptions;
        private Button _cancel;
        private readonly DataGridView _dataGridView;

        public PersonGridView()
        {
            SetGridView();
        }

        public PersonGridView( DataGridView dataGridView )
        {
            _dataGridView = dataGridView;
            SetGridView();
        }

        public void SetGridView()
        {
            SlmModalSize = ModalSize.Large;
           
            
            _dataGridView.Parent = this;
            _dataGridView.Dock = DockStyle.Fill;
            _dataGridView.AutoSize = true;



            _gridViewOptions    =     new TableLayoutPanel();
            _leftOptions        =     new TableLayoutPanel();
            _rightOptions       =     new TableLayoutPanel();
            _edit               =     new Button();
            _delete             =     new Button();
            _cancel             =     new Button();

            
            // 
            // GridViewOptions
            // 
            _gridViewOptions.ColumnCount = 2;
            _gridViewOptions.ColumnStyles.Add(
                                             new ColumnStyle(
                                                 SizeType.Percent, 50F ) );
            _gridViewOptions.ColumnStyles.Add(
                                             new ColumnStyle(
                                                 SizeType.Percent, 50F ) );
            _gridViewOptions.Controls.Add( _leftOptions, 0, 0 );
            _gridViewOptions.Controls.Add( _rightOptions, 1, 0 );
            _gridViewOptions.Dock = DockStyle.Bottom;
            _gridViewOptions.Location = new Point( 0, 262 );
            _gridViewOptions.Name = "GridViewOptions";
            _gridViewOptions.RowCount = 1;
            _gridViewOptions.RowStyles.Add(
                                          new RowStyle(
                                              SizeType.Percent, 50F ) );
            _gridViewOptions.Size = new Size( 522, 61 );
            _gridViewOptions.TabIndex = 1;
            // 
            // Edit
            // 
            _edit.Anchor =
                ( ( AnchorStyles.Top | AnchorStyles.Bottom )
                  | AnchorStyles.Left )
                | AnchorStyles.Right;
            _edit.Location = new Point( 3, 3 );
            _edit.Name = "Edit";
            _edit.Size = new Size( 121, 49 );
            _edit.TabIndex = 0;
            _edit.Text = @"EDIT";
            _edit.UseVisualStyleBackColor = true;
            _edit.Click += EditClick;
            // 
            // LeftOptions
            // 
            _leftOptions.ColumnCount = 2;
            _leftOptions.ColumnStyles.Add(
                                         new ColumnStyle(
                                             SizeType.Percent, 50F ) );
            _leftOptions.ColumnStyles.Add(
                                         new ColumnStyle(
                                             SizeType.Percent, 50F ) );
            _leftOptions.Controls.Add( _edit, 0, 0 );
            _leftOptions.Controls.Add( _delete, 1, 0 );
            _leftOptions.Dock = DockStyle.Fill;
            _leftOptions.Location = new Point( 3, 3 );
            _leftOptions.RowCount = 1;
            _leftOptions.RowStyles.Add(
                                      new RowStyle(
                                          SizeType.Percent, 50F ) );
            _leftOptions.Size = new Size( 255, 55 );
            _leftOptions.TabIndex = 0;
            // 
            // RightOptions
            // 
            _rightOptions.ColumnCount = 2;
            _rightOptions.ColumnStyles.Add(
                                          new ColumnStyle(
                                              SizeType.Percent, 50F ) );
            _rightOptions.ColumnStyles.Add(
                                          new ColumnStyle(
                                              SizeType.Percent, 50F ) );
            _rightOptions.Controls.Add( _cancel, 1, 0 );
            _rightOptions.Dock = DockStyle.Fill;
            _rightOptions.Location = new Point( 264, 3 );
            _rightOptions.RowCount = 1;
            _rightOptions.RowStyles.Add(
                                       new RowStyle(
                                           SizeType.Percent, 50F ) );
            _rightOptions.Size = new Size( 255, 55 );
            _rightOptions.TabIndex = 1;
            // 
            // Delete
            // 
            _delete.Anchor =
                ( ( AnchorStyles.Top | AnchorStyles.Bottom )
                  | AnchorStyles.Left )
                | AnchorStyles.Right;
            _delete.Location = new Point( 130, 3 );
            _delete.Name = "Delete";
            _delete.Size = new Size( 122, 49 );
            _delete.TabIndex = 1;
            _delete.Text = @"DELETE";
            _delete.UseVisualStyleBackColor = true;
            _delete.Click += DeleteClick;
            // 
            // Cancel
            // 
            _cancel.Anchor =
                ( ( AnchorStyles.Top | AnchorStyles.Bottom )
                  | AnchorStyles.Left )
                | AnchorStyles.Right;
            _cancel.Location = new Point( 130, 3 );
            _cancel.Size = new Size( 122, 49 );
            _cancel.TabIndex = 2;
            _cancel.Text = @"CANCEL";
            _cancel.UseVisualStyleBackColor = true;
            // 
            // PersonGridView
            // 
            AutoScaleDimensions = new SizeF( 6F, 13F );
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size( 522, 323 );
            Controls.Add( _gridViewOptions );
           // Controls.Add( PersonDVG );
            Text = "Person View";
            _dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            _dataGridView.MultiSelect = false;
        }

        private void EditClick( object sender, EventArgs e )
        {
            int i = _dataGridView.SelectedRows[ 0 ].Index;
            Person selectedRow = (Person)_dataGridView.Rows[ i ].DataBoundItem;

            Enums.Role role = selectedRow.PersonRole;

            var warden = LoggedUser.Instance as Warden;
            var management = LoggedUser.Instance as Management;
            var security = LoggedUser.Instance as Security;
            var medical = LoggedUser.Instance as Medical;

            switch ( role )
            {
                case Enums.Role.Warden :
                    if ( warden == null ) return;
                    new ModifyPersonFormFactory().Generate(role, (Warden)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Secretary :
                    if ( management == null ) return;
                    new ModifyPersonFormFactory().Generate(role, (Secretary)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Officer :
                    if ( management == null && security == null ) return;
                    new ModifyPersonFormFactory().Generate(role, (Warden)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Guard :
                    if (management == null && security == null) return;
                    new ModifyPersonFormFactory().Generate(role, (Guard)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Medical :
                    if (management == null && security == null && medical == null) return;
                    new ModifyPersonFormFactory().Generate(role, (Medical)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Administrator :
                    new ModifyPersonFormFactory().Generate(role, (Administrator)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Maintenance :
                    new ModifyPersonFormFactory().Generate(role, (Maintenance)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Inmate :
                    new ModifyPersonFormFactory().Generate(role, (Inmate)selectedRow).ShowDialog();
                    break;
                
                case Enums.Role.Visitor :
                    new ModifyPersonFormFactory().Generate(role, (Visitor)selectedRow).ShowDialog();
                    break;
                
                default :
                    throw new ArgumentOutOfRangeException();
            }

        }

        private void DeleteClick( object sender, EventArgs e )
        {
            int i = _dataGridView.SelectedRows[ 0 ].Index;
            Person selectedRow = (Person)_dataGridView.Rows[ i ].DataBoundItem;

            Enums.Role role = selectedRow.PersonRole;

            var warden = LoggedUser.Instance as Warden;
            var management = LoggedUser.Instance as Management;
            var security = LoggedUser.Instance as Security;

            switch ( role )
            {
                case Enums.Role.Warden :

                    if ( warden == null ) return;

                    if ( warden.DeleteWarden( (Warden)selectedRow ) ) {}

                    break;

                case Enums.Role.Secretary :

                    if ( management == null ) return;

                    if ( management.DeleteSecretaries( (Management)selectedRow ) ) {}

                    break;

                case Enums.Role.Officer :

                    if ( management == null ) return;

                    if ( management.DeleteOfficer( (Officer)selectedRow ) ) {}

                    break;

                case Enums.Role.Guard :

                    if ( management == null ) return;

                    if ( management.DeleteGuards( (Guard)selectedRow ) ) {}

                    break;

                case Enums.Role.Medical :

                    if ( management == null ) return;

                    if ( management.DeleteMedical( (Medical)selectedRow ) ) {}

                    break;
                    
                case Enums.Role.Maintenance :

                    if ( management == null ) return;

                    if ( management.DeleteMaintenance( (Maintenance)selectedRow ) ) {}

                    break;

                case Enums.Role.Administrator:

                    if (management == null) return;

                    if (management.DeleteAdministrator((Administrator)selectedRow)) { }

                    break;

                case Enums.Role.Inmate :

                    if ( management != null && management.DeleteInmate( (Inmate)selectedRow ) ) {}

                    else if ( security != null && security.DeleteInmate( (Inmate)selectedRow ) ) {}

                    break;

                case Enums.Role.Visitor :

                    if ( management == null ) return;

                    if ( management.DeleteVisitor( (Visitor)selectedRow ) ) {}

                    break;
            }
        }
    }
}