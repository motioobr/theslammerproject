﻿using System;
using System.Linq;
using System.Linq.Expressions;
using TheSlammer.Model.Data;
using TheSlammer.Model.People;

namespace TheSlammer
{
    public static class Validation
    {
        public static bool BuildingNameExists( string name, byte buildingID )
        {
            return ContextInstance.Data.Buildings.Count( 
                b => b.Name.ToLower().Equals( name.ToLower() ) 
                    && b.BuildingID != buildingID
            ) != 0;
        }
        
        public static bool UsernameExists( string username, int userID )
        {
            return ContextInstance.Data.Users.Count( 
                u => u.Username.ToLower().Equals( username.ToLower() )
                    && u.PersonID != userID 
            ) != 0;
        }

        public static bool ValidUsername( string username )
        {
            return HasLength( username, 6, 30 ) && !HasWhiteSpace( username );
        }

        public static bool ValidPassword( string password, string passwordConfirm )
        {
            return HasLength( password, 6, 30 ) 
                && !HasWhiteSpace( password ) 
                && password.Equals( passwordConfirm );
        }

        public static bool HasLength( string str, int min, int max = 100 )
        {
            str = str.Trim();

            return str.Length >= min && str.Length <= max;
        }

        public static bool HasWhiteSpace( string str )
        {
            return str.IndexOf( " ", StringComparison.Ordinal ) != 0;
        }
    }
}