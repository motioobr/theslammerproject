﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TheSlammer.Model.Data;
using TheSlammer.Views;

namespace TheSlammer
{
    internal static class Program
    {
        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );

            Thread load = new Thread( new ThreadStart( delegate { ContextInstance.Data.People.ToList(); } ) );
            load.Start();

            DEVELOPMENT.Playground();

            if ( LoggedUser.Instance == null ) return;

            Application.Run( new MainDashboard() );

            DEVELOPMENT.PlaygroundTwo();
        }
    }
}