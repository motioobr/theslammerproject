﻿using System;
using System.Linq;
using System.Windows.Forms;
using TheSlammer.Model.Data;
using TheSlammer.Model.People;
using TheSlammer.Views.Forms;

namespace TheSlammer
{
    public static class LoggedUser
    {
        public static bool Authenticate( string username, string password )
        {
            User user;

            try
            {
                user =
                    ContextInstance
                        .Data
                        .Users
                        .FirstOrDefault( u => u.Username == username && u.Password == password );
            }
            catch ( ArgumentNullException )
            {
                return false;
            }

            if ( user == null ) return false;

            _user = user;
            return true;
        }

        private static void CheckInstance()
        {
            if ( _user != null ) return;

            new LoginForm().ShowDialog();

            _loggedInTime = DateTime.Now;
        }


        #region Fields/Properties

        private static User _user;
        private static DateTime _loggedInTime;

        public static User Instance
        {
            get
            {
                CheckInstance();
                return _user;
            }
        }

        public static DateTime LoggedInTime
        {
            get
            {
                CheckInstance();
                return _loggedInTime;
            }
        }

        #endregion
    }
}